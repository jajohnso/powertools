#ifndef AMACv2_H_
#define AMACv2_H_

#include <map>

#include "AMACv2RegMap.h"
#include "EndeavourCom.h"
#include "EndeavourRaw.h"

class AMACv2 : public EndeavourCom, public AMACv2RegMap
{
public:
  enum class AM {
    VDCDC,
    VDDLR,
    DCDCIN,
    VDDREG,
    VDDBG,
    AM900BG,
    AM600BG,
    CAL,
    AMREF,
    CALX,
    CALY,
    SHUNTX,
    SHUNTY,
    DCDCADJ,
    CTAT,
    NTCX,
    NTCY,
    NTCPB,
    HREFX,
    HREFY,
    CUR10V,
    CUR10VTPL,
    CUR10VTPH,
    CUR1V,
    CUR1VTPL,
    CUR1VTPH,
    HVRET,
    PTAT,
    };

  //! Map to go from AM enum to string
  static const std::map<AMACv2::AM, std::string> Map_AMStr;

  AMACv2(unsigned short amacid, std::unique_ptr<EndeavourRaw> raw); // with hardware	
  ~AMACv2();

  //! \brief Initialize the communication with chip
  /**
   * Consists of two steps:
   *  1. Call setID (if needed, as determined by isCommIDSet).
   *  2. Load current chip register values into the local register map.
   */
  void init();

  //! \brief Load AMAC with local register map
  void initRegisters();

  /**
   * Load registers from the AMAC chip and store them in the
   * internal register map
   */
  void loadRegisters();

  bool isCommIDSet();

  virtual void write_reg(unsigned int address, unsigned int data);
  virtual unsigned int read_reg(unsigned int address);

  void wrField(AMACv2Field AMACv2RegMap::* ref, uint32_t data);
  void wrField(const std::string& fieldName, uint32_t data);

  uint32_t rdField(AMACv2Field AMACv2RegMap::* ref);
  uint32_t rdField(const std::string& fieldName);

  //
  // PADID and eFuse settings
  void setPADID(uint8_t padid);
  uint8_t getPADID();
  uint32_t readEFuse();

  /** \name Calibrations
   * @{
   */

  //! Set slope of AM
  /**
   * \param ADCslope slope of AM ramp in mV/count
   */
  void setADCslope (double ADCslope);

  //! Set the AM offset for a given channel
  /**
   * \param ch Channel
   * \param counts counts at zero voltage
   */
  void setADCoffset(uint8_t ch, uint32_t counts);

  //! Set the NTCx reference
  /**
   * \param ntccal reference in mV
   */
  void setNTCxCal(double ntccal);

  //! Set the NTCy reference
  /**
   * \param ntccal reference in mV
   */
  void setNTCyCal(double ntccal);

  //! Set the NTCpb reference
  /**
   * \param ntccal reference in mV
   */
  void setNTCpbCal(double ntccal);

  //! Set PTAT absolute scale
  /**
   * \param ptat0 PTAT reading (mV) at 0C
   */
  void setPTAT0(double ptat0);

  //! Set the CTAT absolute scale
  /**
   * \param offset CTAT offset
   * \param ctat0 CTAT reading (mV) at 0C
   */
  void setCTAT0(uint8_t offset, double ctat0);

  //! Set the Cur10V offset at "zero" current
  /**
   * \param offset new offset value
   */
  void setCur10VOffset(double offset);

  //! Get the Cur10V offset at "zero" current
  double getCur10VOffset();

  //! Set the Cur1V offset at "zero" current
  /**
   * \param offset new offset value
   */
  void setCur1VOffset(double offset);

  //! Get the Cur1V offset at "zero" current
  double getCur1VOffset();

  /** @} */

  /** \name Analogue Monitor Readings
   * @{
   */

  //! \brief AM reading of quantity [counts]
  /**
   * Measures a given quantity using the AM block. If
   * the `am` is in a multiplexed channel, then it is
   * also selected.
   *
   * Note that the average is rounded to the nearest
   * integer.
   *
   * \param am Quantity to measure
   * \param reads Number of reads to average
   *
   * \return Measured voltage [counts]
   */
  uint32_t readAM(AM am, uint32_t reads=1);

  //! \brief AM reading for channel `ch` [mV]
  /**
   * \param ch AM channel to read
   * \param reads Number of reads to average
   *
   * \return Measured voltage [mV]
   */
  double getADC(uint8_t ch, uint32_t reads=1);

  double getVDDREG();
  double getAM600();
  double getAM900();

  //! Get temperature reading from NTCx in volts
  double getNTCx ();

  //! Get temperature reading from NTCy in volts
  double getNTCy ();

  //! Get temperature reading from NTCpb in volts
  double getNTCpb();

  //! Get PTAT reading from bPOL in volts
  double getPTAT();

  //! Get CTAT reading from AMAC in volts
  double getCTAT();

  /** @} */

  /** \name Temperature Measurements
   * @{
   */

  //! Get Hybrid X temperature in C
  /**
   * Assuming random NTC for conversion
   */
  double temperatureX ();

  //! Get Hybrid Y temperature in C
  /**
   * Assuming random NTC for conversion
   */
  double temperatureY ();

  //! Get Powerboard temperature in C
  /**
   * Assuming [Panasonic ERT-J0EM103J](https://industrial.panasonic.com/cdbs/www-data/pdf/AUA0000/AUA0000C8.pdf)
   * as the NTC for converting volts to C.
   */
  double temperaturePB();

  //! Get bPOL temperature in C
  /**
   * Assumes 8.5mV/C and PTAT0 calibraton.
   * temp = (PTAT-PTAT0)/8.5
   */
  double temperaturePTAT();

  //! Get AMAC temperature in C
  /**
   * Assumes 1.5mV/C and CTAT0 calibraton.
   * temp = (CTAT-CTAT0)/1.5
   */
  double temperatureCTAT();

  /** @} */

  /** \name Current Measurements
   * @{
   */

  //! Calculate input current from AM reading
  /**
   * Assumes gain of 10.4 and resistance of 33mOhm.
   *
   * \param reading Cur10V AM reading [counts]
   *
   * \return Corresponding input current [mA]
   */
  double convertCur10V(uint32_t Cur10V);

  //! Calculate output current from AM reading
  /**
   * Assumes gain of 30 and resistance of 7.6mOhm.
   *
   * \param reading Cur1V AM reading [counts]
   *
   * \return Corresponding output current [mA]
   */
  double convertCur1V(uint32_t Cur1V);

  /** @} */

  /** \name Voltage measurements
   * @{
   */

  //! Calculate bPOL output voltage from AM reading
  /**
   * Assumes divisor by half
   *
   * \param reading VDCDC AM reading [counts]
   *
   * \return Corresponding voltage [V]
   */
  double convertVDCDC(uint32_t VDCDC);

  /** @} */

private:
  double calibrateCounts(uint8_t ch, uint32_t counts);

  void syncReg(AMACv2Field AMACv2RegMap::* ref);

  double   m_ADCslope      =1.;
  uint32_t m_ADCoffset[16] ={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  uint8_t  m_padid         =0;

  double   m_NTCxCal       =250.;
  double   m_NTCyCal       =250.;
  double   m_NTCpbCal      =250.;

  double   m_PTAT0         =250.; // empirically derived
  uint32_t m_CTAT0[16]     ={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  double m_Cur10VOffset     =200.;
  double m_Cur1VOffset      =200.;
};

#endif // AMACv2_H_
