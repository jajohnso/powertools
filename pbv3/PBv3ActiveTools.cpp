#include "PBv3ActiveTools.h"

#include "ComIOException.h"
#include "NotSupportedException.h"
#include "PCA9548ACom.h"
#include "PowerSupplyChannel.h"

#include "PBv3Utils.h"

#include "PBv3TBMassive20190718.h"
#include "PBv3TBMassive20210504.h"

#include <memory>
#include <thread>

using namespace nlohmann;

namespace PBv3ActiveTools 
{
  json testLvEnable(std::shared_ptr<PBv3TBMassive> tb) 
  {
    logger(logINFO) << "## Test LV_ENABLE ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "LV_ENABLE";
    testSum["results"]["TIMESTART"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    tb->powerLVOn();

    // Wait to stabilize
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // read interesting values
    double Iin  = tb->getLVPS()->measureCurrent();
    double Vin  = tb->getLVPS()->measureVoltage();

    testSum["passed"] = (Iin<5e-3);

    if(testSum["passed"])
      logger(logINFO) << " --> Reading " << Iin << "A with " << Vin << "V supplied";
    else
      logger(logERROR) << " --> Reading " << Iin << "A with " << Vin << "V supplied";

    testSum["results"]["VIN"] = Vin;
    testSum["results"]["IIN"] = Iin;

    tb->powerLVOff();

    logger(logINFO) << "## End test LV_ENABLE ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
  }

  json testI2C(std::shared_ptr<PBv3TBMassive> tb) 
  {
    logger(logINFO) << "## Test I2CDEV ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "I2CDEV";
    testSum["results"]["TIMESTART"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<I2CCom> i2c = tb->getI2CRoot();

    // common chips
    std::map<std::string, std::shared_ptr<I2CCom>> devices= {
      {"0_0x14", std::make_shared<PCA9548ACom>(0x14, 0, i2c)},
      {"1_0x4C", std::make_shared<PCA9548ACom>(0x4C, 1, i2c)},
      {"5_0x4C", std::make_shared<PCA9548ACom>(0x4C, 5, i2c)},
      {"5_0x4D", std::make_shared<PCA9548ACom>(0x4D, 5, i2c)},
      {"5_0x4E", std::make_shared<PCA9548ACom>(0x4E, 5, i2c)},
      {"5_0x21", std::make_shared<PCA9548ACom>(0x21, 5, i2c)},
      {"5_0x22", std::make_shared<PCA9548ACom>(0x22, 5, i2c)},
      {"5_0x68", std::make_shared<PCA9548ACom>(0x68, 5, i2c)},
      {"5_0x6C", std::make_shared<PCA9548ACom>(0x6C, 5, i2c)},
      {"5_0x6A", std::make_shared<PCA9548ACom>(0x6A, 5, i2c)}
    };

    // determine revision to determine what chips to test
    if(std::dynamic_pointer_cast<PBv3TBMassive20190718>(tb)!=nullptr) {
      devices["1_0x68"]=std::make_shared<PCA9548ACom>(0x68, 1, i2c);

      devices["3_0x20"]=std::make_shared<PCA9548ACom>(0x20, 3, i2c);

      devices["4_0x40"]=std::make_shared<PCA9548ACom>(0x40, 4, i2c);

      devices["7_0x21"]=std::make_shared<PCA9548ACom>(0x6A, 5, i2c);
    } else if(std::dynamic_pointer_cast<PBv3TBMassive20210504>(tb)!=nullptr) {
      devices["1_0x6A"]=std::make_shared<PCA9548ACom>(0x6A, 1, i2c);

      devices["3_0x38"]=std::make_shared<PCA9548ACom>(0x38, 3, i2c);
      devices["3_0x3C"]=std::make_shared<PCA9548ACom>(0x3C, 3, i2c);

      devices["4_0x44"]=std::make_shared<PCA9548ACom>(0x44, 4, i2c);

      devices["7_0x23"]=std::make_shared<PCA9548ACom>(0x6A, 5, i2c);
    } else {
      throw NotSupportedException("Unknown active board revision");
    }

    for(const std::pair<std::string, std::shared_ptr<I2CCom>>& device : devices)
      {
	try
	  {
	    device.second->write_block({});
	    logger(logINFO) << device.first << " OK";
	    testSum["results"][device.first]=true;
	  }
	catch(const ComIOException& e)
	  {
	    logger(logERROR) << device.first << " ERROR";
	    testSum["results"][device.first]=false;
	    testSum["passed"] = false;
	  }
      }

    logger(logINFO) << "## End test I2CDEV ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
  }
}
