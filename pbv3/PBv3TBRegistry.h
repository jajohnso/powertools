#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <ClassRegistry.h>

#include "PBv3TB.h"

// Registry for specific PBv3 test bench implementations
namespace PBv3TBRegistry
{
  //! \brief Register new PBv3TB implementation
  /**
   * \param type Type of the implementation
   * \param f functional wrapping the constructor
   */
  bool registerPBv3TB(const std::string& type,
		      std::function<std::shared_ptr<PBv3TB>()> f);

  //! Get new instance of given PBv3TB implementation
  /**
   * \param type Type of the implementation
   */
  std::shared_ptr<PBv3TB> createPBv3TB(const std::string& type);

  /** \brief List registered implementations
   */
  std::vector<std::string> listPBv3TB();  
}

#define REGISTER_PBV3TB(type)						\
  static bool _registered_##type =					\
    PBv3TBRegistry::registerPBv3TB(#type,				\
				   std::function<std::shared_ptr<PBv3TB>()>([]() { return std::make_shared<type>(); }));
