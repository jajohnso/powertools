#include "PBv3ConfigTools.h"

#include "EndeavourComException.h"
#ifdef FTDI
#include "EndeavourRawFTDI.h"
#endif // FTDI

#include <algorithm>
#include <unistd.h>

namespace PBv3ConfigTools
{
  void configAMAC(std::shared_ptr<AMACv2> amac, const json& config, bool write)
  {
    // Check if "config" key is present, in which case we are using a test result output
    const json& cfgtop=(config.count("config"))?config.at("config"):config;

    //
    // Configure registers
    if(cfgtop.count("properties"))
      {
	for(auto field : cfgtop["properties"].items())
	  {
	    AMACv2Field *fref=amac->findField(field.key());
	    if(fref==nullptr)
	      {
		logger(logWARNING) << "Skipping non-existent field " << field.key();
		continue;
	      }

	    fref->setDefaultVal(field.value().get<uint32_t>());
	    if(write)
	      amac->wrField  (field.key(), field.value().get<uint32_t>());
	  }
      }

    //
    //Set PADID
    if(cfgtop.count("results") && cfgtop["results"].count("PADID"))
      {
	amac->setPADID(cfgtop["results"]["PADID"].get<uint8_t>());
      }

    //
    // Configure ADC calibration
    if(cfgtop.count("results") && cfgtop["results"].count("AMSLOPE"))
      {
	amac->setADCslope(cfgtop["results"]["AMSLOPE"].get<double>());
      }

    //
    // Configure ADC offset
    if(cfgtop.count("results") && cfgtop["results"].count("AMOFFSET"))
      {
	for(uint i=0; i<cfgtop["results"]["AMOFFSET"].size(); i++)
	  if(!cfgtop["results"]["AMOFFSET"][i].is_null())
	    amac->setADCoffset(i, cfgtop["results"]["AMOFFSET"][i].get<uint32_t>());
      }

    //
    // Configure temperature calibrations
    if(cfgtop.count("results"))
      {
	if(cfgtop["results"].count("NTCX" ))
	  amac->setNTCxCal (cfgtop["results"]["NTCX" ].get<double>());

	if(cfgtop["results"].count("NTCY" ))
	  amac->setNTCyCal (cfgtop["results"]["NTCY" ].get<double>());

	if(cfgtop["results"].count("NTCPB"))
	  amac->setNTCpbCal(cfgtop["results"]["NTCPB"].get<double>());

	if(cfgtop["results"].count("PTAT0"))
	  amac->setPTAT0(cfgtop["results"]["PTAT0"].get<double>());

	if(cfgtop["results"].count("CTAT0"))
	  {
	    for(uint i=0; i<cfgtop["results"]["CTAT0"].size(); i++)
	      if(!cfgtop["results"]["CTAT0"][i].is_null())
		amac->setCTAT0(i, cfgtop["results"]["CTAT0"][i].get<double>());
	  }
      }

    //
    // Configure current measuring offsets
    if(cfgtop.count("results")) {
      if(cfgtop["results"].count("CUR10VOFFSET")) {
        amac->setCur10VOffset(cfgtop["results"]["CUR10VOFFSET"].get<double>());
      }

      if(cfgtop["results"].count("CUR1VOFFSET" )) {
        amac->setCur1VOffset (cfgtop["results"]["CUR1VOFFSET" ].get<double>());
      }
    }
  }

  void saveConfigAMAC(std::shared_ptr<AMACv2> amac, json& config)
  {
    //
    // Get register values
    for(const AMACv2Field* field : amac->getFields())
      {
	if(field->isReadWrite())
	  config["properties"][field->getName()]=field->read();
      }
  }

  void decorateConfig(json& config)
  {
    config["testType"]="CONFIG";
    config["institution"]="LBNL_STRIP_POWERBOARDS";

    std::string runNumber="0-0";
    if(config.count("runNumber")>0)
      {
	// Parse the existing version string
	std::istringstream ss(config["runNumber"].get<std::string>());
	std::vector<int32_t> tokens;
	std::string token;
	while(std::getline(ss, token, '-'))
	  tokens.push_back(std::stoi(token)); // TODO: try-catch bad format

	// Update version
	if(tokens.size()>0)
	  {
	    runNumber="";
	    for(uint32_t i=0;i<tokens.size();i++)
	      {
		if(i!=0) runNumber+="-";
	 	runNumber+=(i!=tokens.size()-1)?(std::to_string(tokens[i])):(std::to_string(tokens[i]+1));
	      }
	  }
      }
    config["runNumber"]=runNumber;
  }

json tuneVDDBG(std::shared_ptr<AMACv2> amac, std::shared_ptr<DACDevice> CALdac)
  {
    logger(logINFO) << "## Tuning VDDREG ##";
    json config;

    amac->wrField(&AMACv2RegMap::VDDbgen, 1);

    // Run the test
    double bestVDDREG=0;
    uint32_t bestVDDbg=0;
    json bestSlope, bestOffset;

    for(uint VDDbg=0;VDDbg<pow(2,4);VDDbg++)
      {
	amac->wrField(&AMACv2RegMap::VDDbg, VDDbg);
	usleep(5e3);
	json slope =calibrateSlope (amac, CALdac);
	json offset=calibrateOffset(amac);

	double VDDREG=amac->getVDDREG();

	logger(logDEBUG) << "VDDbg = " << VDDbg << ", VDDREG = "  << VDDREG;
	
 	if(fabs(VDDREG-1200)<fabs(bestVDDREG-1200))
	  {
            bestVDDREG=VDDREG;
	    bestVDDbg =VDDbg;
            bestSlope =slope;
	    bestOffset=offset;
	  }
      }

    logger(logINFO) << "\tVDDbg = " << bestVDDbg;
    logger(logINFO) << "\tVDDREG = " << bestVDDREG/1e3 << " V";

    config["properties"]["VDDbgen"]=1;
    config["properties"]["VDDbg"]  =bestVDDbg;
    config["results"]["VDDREG"]    =bestVDDREG;
    config.merge_patch(bestSlope );
    config.merge_patch(bestOffset);

    configAMAC(amac, config, true);

    return config;
  }

  json tuneAMBG(std::shared_ptr<AMACv2> amac, std::shared_ptr<DACDevice> CALdac)
  {
    logger(logINFO) << "## Tuning AMBG ##";
    json config;

    // Run the test
    double bestAM=0;
    uint32_t bestAMbg=0;
    json bestSlope, bestOffset;

    amac->wrField(&AMACv2RegMap::AMbgen, 1);

    for(uint AMbg=0;AMbg<pow(2,4);AMbg++)
      {
	amac->wrField(&AMACv2RegMap::AMbg, AMbg);
	usleep(5e3);
	json slope =calibrateSlope (amac, CALdac);
	json offset=calibrateOffset(amac);

	double AM=amac->getAM600();

	logger(logDEBUG) << "AMbg = " << AMbg << ", AM = "  << AM;
	
 	if(fabs(AM-600)<fabs(bestAM-600))
	  {
	    bestAM    =AM;
	    bestAMbg  =AMbg;
	    bestSlope =slope;
	    bestOffset=offset;
	  }
      }

    logger(logINFO) << "\tAMbg = " << bestAMbg;
    logger(logINFO) << "\tAM = " << bestAM << " mV";

    config["properties"]["AMbgen"]=1;
    config["properties"]["AMbg"  ]=bestAMbg;
    config["results"   ]["AM"    ]=bestAM;
    config.merge_patch(bestSlope );
    config.merge_patch(bestOffset);

    configAMAC(amac, config, true);

    return config;
  }

  json tuneRampGain(std::shared_ptr<AMACv2> amac, std::shared_ptr<DACDevice> CALdac)
  {
    logger(logINFO) << "## Tuning Ramp Gain ##";

    json config;

    // Run the test
    uint32_t bestAMintCalib=0;
    json bestSlope;

    amac->wrField(&AMACv2RegMap::Ch4Mux , 1); // Set Ch4 mux to CAL input
    for(uint32_t AMintCalib=0;
	AMintCalib<16;
	AMintCalib++)
      {
	amac->wrField(&AMACv2RegMap::AMintCalib, AMintCalib);
	logger(logDEBUG) << "Set AMintCalib = " << AMintCalib;

	json slope=calibrateSlope(amac, CALdac);

 	if(bestSlope.is_null() || fabs(slope["results"]["AMSLOPE"].get<double>()-1)<fabs(bestSlope["results"]["AMSLOPE"].get<double>()-1))
	  {
	    bestSlope=slope;
	    bestAMintCalib=AMintCalib;
	  }
      }

    amac->wrField(&AMACv2RegMap::AMintCalib, bestAMintCalib);
    json bestOffset=calibrateOffset(amac);

    logger(logINFO) << "\tAMintCalib = " << bestAMintCalib;
    logger(logINFO) << "\tslope = " << bestSlope["results"]["AMSLOPE"].get<double>()*1e3 << " mV/count";

    config["properties"]["AMintCalib"]=bestAMintCalib;
    config.merge_patch(bestSlope );
    config.merge_patch(bestOffset);

    configAMAC(amac, config, true);

    return config;
  }

  json tuneCur10V(std::shared_ptr<AMACv2> amac)
  {
    static const uint32_t trails=100; // Number of trails per point

    logger(logINFO) << "## Tuning input current monitor block ##";

    json config;

    if(amac->rdField(&AMACv2RegMap::DCDCen)==1)
      {
	logger(logWARNING) << "tuneCur10V run with DCDCen=1";
      }

    uint32_t DCDCiZeroReading_curr=amac->rdField(&AMACv2RegMap::DCDCiZeroReading);
    uint32_t Ch12Mux_curr         =amac->rdField(&AMACv2RegMap::Ch12Mux         );

    //
    // Buffer offsets
    amac->wrField(&AMACv2RegMap::DCDCiZeroReading , 1); // Short the DCDCi inputs

    logger(logDEBUG) << "Measuring relative buffer offset";

    amac->wrField(&AMACv2RegMap::Ch12Mux , 1); // Read the low point
    double VtestLZero=amac->getADC(12, trails);

    amac->wrField(&AMACv2RegMap::Ch12Mux , 2); // Read the high point
    double VtestHZero=amac->getADC(12, trails);

    double VtestHLZero=VtestHZero-VtestLZero;

    logger(logINFO) << "\tBuffer offset = " << VtestHLZero << " mV";

    //
    // Tune the current mirrors (does not do anything other than write values)

    amac->wrField(&AMACv2RegMap::DCDCiZeroReading , 0); // Separate the DCDCi inputs

    logger(logDEBUG) << "Tuning input CM mirror";
    double bestCMDV=-666; // Best attainable match
    uint32_t bestidcdcP=0, bestidcdcN=0;

    for(uint32_t idcdcP=0;idcdcP<pow(2,3);idcdcP++)
      {
	amac->wrField(&AMACv2RegMap::DCDCiP, idcdcP);
	for(uint32_t idcdcN=0;idcdcN<pow(2,3);idcdcN++)
	  {
	    amac->wrField(&AMACv2RegMap::DCDCiN, idcdcN);

	    amac->wrField(&AMACv2RegMap::Ch12Mux , 0); // Read the mid point
	    double Vout  =amac->getADC(12, trails);

	    amac->wrField(&AMACv2RegMap::Ch12Mux , 1); // Read the low point
	    double VtestL=amac->getADC(12, trails);

	    amac->wrField(&AMACv2RegMap::Ch12Mux , 2); // Read the high point
	    double VtestH=amac->getADC(12, trails);

	    logger(logDEBUG) << "idcdcP,idcdcN,Vout,VtestL,VtestH = " << idcdcP << "\t" << idcdcN << "\t" << Vout << "\t" << VtestL << "\t" << VtestH;

	    double CMDV=VtestH-VtestL-VtestHLZero;
	    if(bestCMDV==-666 || fabs(CMDV-0)<fabs(bestCMDV-0))
	      {
		bestCMDV=CMDV;
		bestidcdcP=idcdcP;
		bestidcdcN=idcdcN;
	      }
	  }
      }

    logger(logINFO) << "\tDCDCiP/NidcdcP/N = " << bestidcdcP<<","<<bestidcdcN;
    logger(logINFO) << "\tCM offset = " << bestCMDV << " mV";

    //
    // Amplifier offset tunning
    //
    amac->wrField(&AMACv2RegMap::DCDCiP , bestidcdcP);
    amac->wrField(&AMACv2RegMap::DCDCiN , bestidcdcN);
    amac->wrField(&AMACv2RegMap::Ch12Mux, 0);

    //
    // Full Chain
    logger(logDEBUG) << "Tuning amplifier offset, \"Full Chain\"";

    amac->wrField(&AMACv2RegMap::DCDCiZeroReading , 0); // Separate the DCDCi inputs

    double bestDVFullChain=-1;
    uint32_t bestOffset=0;
    for(uint i=0;i<pow(2,4);i++)
      {
    	amac->wrField(&AMACv2RegMap::DCDCiOffset, i);
    	double DV=amac->getADC(12, trails);
    	logger(logDEBUG) << " DCDCiOffset = " << i << ", DV = " << DV << " mV";
    	if(bestDVFullChain==-1 || fabs(DV-200)<fabs(bestDVFullChain-200))
    	  {
    	    bestDVFullChain=DV;
    	    bestOffset=i;
    	  }
      }

    amac->wrField(&AMACv2RegMap::DCDCiOffset, bestOffset);
    bestDVFullChain=amac->getADC(12, 1000);

    logger(logINFO) << "\tDCDCiOffset = " << bestOffset;
    logger(logINFO) << "\toffset = " << bestDVFullChain << " mV";

    //
    // Zero Mode
    logger(logDEBUG) << "Tuning amplifier offset, \"Zero Mode\"";
    amac->wrField(&AMACv2RegMap::DCDCiZeroReading , 1); // Short the DCDCi inputs

    double bestDVZeroMode=-1;
    /** Code to tune in "Zero Mode"
    uint32_t bestOffset=0;
    for(uint i=0;i<pow(2,4);i++)
      {
    	amac->wrField(&AMACv2RegMap::DCDCiOffset, i);
    	double DV=amac->getADC(12, trails);
    	logger(logDEBUG) << " DCDCiOffset = " << i << ", DV = " << DV << " mV";
    	if(bestDVZeroMode==-1 || fabs(DV-250)<fabs(bestDVZeroMode-250))
    	  {
    	    bestDVZeroMode=DV;
    	    bestOffset=i;
    	  }
      }
      amac->wrField(&AMACv2RegMap::DCDCiOffset, bestOffset);
    */
    bestDVZeroMode=amac->getADC(12, 1000);

    logger(logINFO) << "\toffset zero = " << bestDVZeroMode << " mV";

    // Save values
    config["properties"]["DCDCiOffset"] = bestOffset;
    config["properties"]["DCDCiP"     ] = bestidcdcP;
    config["properties"]["DCDCiN"     ] = bestidcdcN;
    config["results"]["CUR10VOFFSETZERO"] = bestDVZeroMode;
    config["results"]["CUR10VCM"        ] = bestCMDV;
    config["results"]["CUR10VOFFSET"    ] = bestDVFullChain;
    configAMAC(amac, config, true);

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCiZeroReading, DCDCiZeroReading_curr);
    amac->wrField(&AMACv2RegMap::Ch12Mux         , Ch12Mux_curr         );

    return config;
  }

  json tuneCur1V(std::shared_ptr<AMACv2> amac)
  {
    static const uint32_t trails=100; // Number of trails per point

    logger(logINFO) << "## Tuning output current monitor block ##";

    json config;

    uint32_t DCDCen_curr          =amac->rdField(&AMACv2RegMap::DCDCen          );
    uint32_t DCDCoZeroReading_curr=amac->rdField(&AMACv2RegMap::DCDCoZeroReading);
    uint32_t Ch13Mux_curr         =amac->rdField(&AMACv2RegMap::Ch13Mux         );

    // Power on the current mirrors
    amac->wrField(&AMACv2RegMap::DCDCen , 1);
    amac->wrField(&AMACv2RegMap::DCDCenC, 1);

    //
    // Buffer offsets
    amac->wrField(&AMACv2RegMap::DCDCoZeroReading , 1); // Short the DCDCo inputs

    logger(logDEBUG) << "Measuring relative buffer offset";

    amac->wrField(&AMACv2RegMap::Ch13Mux , 1); // Read the low point
    double VtestLZero=amac->getADC(13, trails);

    amac->wrField(&AMACv2RegMap::Ch13Mux , 2); // Read the high point
    double VtestHZero=amac->getADC(13, trails);

    double VtestHLZero=VtestHZero-VtestLZero;

    logger(logINFO) << "\tBuffer offset = " << VtestHLZero << " mV";

    //
    // Tune the current mirrors (does not do anything other than write values)

    amac->wrField(&AMACv2RegMap::DCDCoZeroReading , 0); // Separate the DCDCi inputs
    amac->wrField(&AMACv2RegMap::Ch13Mux , 0); // Read the high point

    logger(logDEBUG) << "Tuning output CM mirror";
    double bestCMDV=-666; // Best attanable match
    uint32_t bestodcdcP=0, bestodcdcN=0;

    for(uint32_t odcdcP=0;odcdcP<pow(2,1);odcdcP++)
      {
	amac->wrField(&AMACv2RegMap::DCDCoP, odcdcP);
	for(uint32_t odcdcN=0;odcdcN<pow(2,1);odcdcN++)
	  {
	    amac->wrField(&AMACv2RegMap::DCDCoN, odcdcN);


	    amac->wrField(&AMACv2RegMap::Ch13Mux , 0); // Read the mid point
	    double Vout  =amac->getADC(13, trails);

	    amac->wrField(&AMACv2RegMap::Ch13Mux , 1); // Read the low point
	    double VtestL=amac->getADC(13, trails);

	    amac->wrField(&AMACv2RegMap::Ch13Mux , 2); // Read the high point
	    double VtestH=amac->getADC(13, trails);

            logger(logDEBUG) << "odcdcP,odcdcN,Vout,VtestL,VtestH = " << odcdcP << "\t" << odcdcN << "\t" << Vout << "\t" << VtestL << "\t" << VtestH;

            // When matched, the difference should be zero
            double CMDV=VtestH-VtestL-VtestHLZero;
	    if(bestCMDV==-666 || fabs(CMDV-0)<fabs(bestCMDV-0))
	      {
		bestCMDV=CMDV;
		bestodcdcP=odcdcP;
		bestodcdcN=odcdcN;
	      }
	  }
      }

    logger(logINFO) << "\tDCDCoP/NodcdcP/N = " << bestodcdcP << "," << bestodcdcN;
    logger(logINFO) << "\tCM offset = " << bestCMDV << " mV";

    //
    // Amplifier offset tunning
    amac->wrField(&AMACv2RegMap::DCDCoP , bestodcdcP);
    amac->wrField(&AMACv2RegMap::DCDCoN , bestodcdcN);
    amac->wrField(&AMACv2RegMap::Ch13Mux, 0);

    //
    // Full Chain
    logger(logDEBUG) << "Tuning amplifier offset, \"Full Chain\"";

    amac->wrField(&AMACv2RegMap::DCDCoZeroReading , 0); // Separate the DCDCo inputs

    double bestDVFullChain=-1;
    uint32_t bestOffset=0;
    for(uint i=0;i<pow(2,4);i++)
    {
      amac->wrField(&AMACv2RegMap::DCDCoOffset, i);
      double DV=amac->getADC(13, trails);
      logger(logDEBUG) << " DCDCoOffset = " << i << ", DV = " << DV << " mV";
      if(bestDVFullChain==-1 || fabs(DV-100)<fabs(bestDVFullChain-100))
      {
        bestDVFullChain=DV;
        bestOffset=i;
      }
    }

    amac->wrField(&AMACv2RegMap::DCDCoOffset, bestOffset);
    bestDVFullChain=amac->getADC(13, 1000);

    logger(logINFO) << "\tDCDCoOffset = " << bestOffset;
    logger(logINFO) << "\toffset = " << bestDVFullChain << " mV";

    //
    // Zero Mode
    logger(logDEBUG) << "Tuning amplifier offset, \"Zero Mode\"";
    amac->wrField(&AMACv2RegMap::DCDCoZeroReading , 1); // Short the DCDCo inputs

    double bestDVZeroMode=-1;
    /** Code to tune in "Zero Mode"
    uint32_t bestOffset=0;
    for(uint i=0;i<pow(2,4);i++)
      {
    	amac->wrField(&AMACv2RegMap::DCDCoOffset, i);
    	double DV=amac->getADC(13, trails);
    	logger(logDEBUG) << " DCDCoOffset = " << i << ", DV = " << DV << " mV";
    	if(bestDVZeroMode==-1 || fabs(DV-200)<fabs(bestDVZeroMode-200))
    	  {
    	    bestDVZeroMode=DV;
    	    bestOffset=i;
    	  }
      }
    amac->wrField(&AMACv2RegMap::DCDCoOffset, bestOffset);
    */

    bestDVZeroMode=amac->getADC(13, 1000);

    logger(logINFO) << "\toffset zero = " << bestDVZeroMode << " mV";

    // Save values
    config["properties"]["DCDCoOffset"] = bestOffset;
    config["properties"]["DCDCoP"     ] = bestodcdcP;
    config["properties"]["DCDCoN"     ] = bestodcdcN;
    config["results"]["CUR1VOFFSETZERO"] = bestDVZeroMode;
    config["results"]["CUR1VCM"        ] = bestCMDV;
    config["results"]["CUR1VOFFSET"    ] = bestDVFullChain;
    configAMAC(amac, config, true);

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCoZeroReading, DCDCoZeroReading_curr);
    amac->wrField(&AMACv2RegMap::Ch13Mux         , Ch13Mux_curr         );
    amac->wrField(&AMACv2RegMap::DCDCen          , DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCenC         , DCDCen_curr);

    return config;
  }

  json tuneNTC(std::shared_ptr<AMACv2> amac)
  {
    logger(logINFO) << "## Tuning NTC range ##";

    json config;

    // NTCx
    uint32_t bestNTCx      =0;
    uint32_t bestNTCxValue =1024;
    uint32_t bestNTCy      =0;
    uint32_t bestNTCyValue =1024;
    uint32_t bestNTCpb     =0;
    uint32_t bestNTCpbValue=1024;
    for(uint32_t i=0; i<8; i++)
      {
	amac->wrField(&AMACv2::NTCx0SenseRange, i);
	amac->wrField(&AMACv2::NTCy0SenseRange, i);
	amac->wrField(&AMACv2::NTCpbSenseRange, i);
	usleep(10e3);

	uint32_t NTCx  = amac->rdField(&AMACv2::Ch7Value);
	uint32_t NTCy  = amac->rdField(&AMACv2::Ch8Value);
	uint32_t NTCpb = amac->rdField(&AMACv2::Ch9Value);

	logger(logDEBUG) << "Testing Range Setting " << i;
	logger(logDEBUG) << "NTCx,y,pb = " << NTCx << ", " << NTCy << ", " << NTCpb ;

	uint32_t NTCxValue =(NTCx >500)?abs(NTCx -500):abs(500-NTCx );
	uint32_t NTCyValue =(NTCy >500)?abs(NTCy -500):abs(500-NTCy );
	uint32_t NTCpbValue=(NTCpb>500)?abs(NTCpb-500):abs(500-NTCpb);

	if(NTCxValue <bestNTCxValue  || (i==7 && NTCx ==1023))
	  {
	    bestNTCxValue =NTCxValue;
	    bestNTCx =i;
	  }

	if(NTCyValue <bestNTCyValue  || (i==7 && NTCy ==1023))
	  {
	    bestNTCyValue =NTCyValue;
	    bestNTCy =i;
	  }

	if(NTCpbValue<bestNTCpbValue || (i==7 && NTCpb==1023))
	  {
	    bestNTCpbValue=NTCpbValue;
	    bestNTCpb=i;
	  }
      }

    config["properties"]["NTCx0SenseRange"] = bestNTCx ;
    config["properties"]["NTCy0SenseRange"] = bestNTCy ;
    config["properties"]["NTCpbSenseRange"] = bestNTCpb;

    logger(logDEBUG) << "Choosing NTCx,y,pb = " << bestNTCx << ", " << bestNTCy << ", " << bestNTCpb ;

    configAMAC(amac, config, true);

    return config;
  }

  json calibrateSlope(std::shared_ptr<AMACv2> amac, std::shared_ptr<DACDevice> CALdac)
  {
    logger(logINFO) << "## Calibrating AMAC slope ##";

    json config;
    
    // Run the test
    double CALact=0;
    uint32_t CALamac=0;

    amac->wrField(&AMACv2RegMap::Ch4Mux , 1); // Set Ch4 mux to CAL input

    // Run ramp
    std::vector<double> x,y;
    uint n=0;

    // Longer wait for the first value due to RC constant
    CALdac->set(0);
    usleep(10e3);
    for(double CALin=0; CALin<1.01; CALin+=0.05)
      {
	// actual voltage to be compared against digitized value
	CALact=CALdac->set(CALin);

	// digital about
	CALamac = amac->readAM(AMACv2::AM::CAL);
	if(CALamac==1023) break; //saturated, don't include
	if(750<CALamac && CALamac<760) continue; //Switch point

	x.push_back(CALamac);
	y.push_back(CALact );
	n++;

	logger(logDEBUG) << "\t" << x.back() << "\t" << y.back();
      }

    double xavg=std::accumulate(x.begin(), x.end(), 0.)/n;
    double yavg=std::accumulate(y.begin(), y.end(), 0.)/n;

    double num=0;
    double den=0;
    for(uint i=0;i<x.size();i++)
      {
	num+=(x[i]-xavg)*(y[i]-yavg);
	den+=(x[i]-xavg)*(x[i]-xavg);
      }
    double m=num/den;
    double b=yavg-m*xavg;

    logger(logDEBUG) << "\tm = " << m << ", b = " << b;

    config["results"]["AMSLOPE"]=m*1e3;

    configAMAC(amac, config, true);

    return config;
  }

  json calibrateOffset(std::shared_ptr<AMACv2> amac)
  {
    logger(logINFO) << "## Calibrating AMAC offset ##";

    json config;

    // Enable offset calibration
    amac->wrField(&AMACv2RegMap::AMzeroCalib , 1);
    amac->wrField(&AMACv2RegMap::AMzeroCalibC, 1);
    usleep(5e3);

    // Calculate the calibration
    uint32_t counts;

    counts=amac->rdField(&AMACv2RegMap::Ch0Value );
    config["results"]["AMOFFSET"][ 0] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch1Value );
    config["results"]["AMOFFSET"][ 1] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch2Value );
    config["results"]["AMOFFSET"][ 2] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch3Value );
    config["results"]["AMOFFSET"][ 3] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch4Value );
    config["results"]["AMOFFSET"][ 4] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch5Value );
    config["results"]["AMOFFSET"][ 5] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch6Value );
    config["results"]["AMOFFSET"][ 6] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch7Value );
    config["results"]["AMOFFSET"][ 7] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch8Value );
    config["results"]["AMOFFSET"][ 8] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch9Value );
    config["results"]["AMOFFSET"][ 9] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch10Value);
    config["results"]["AMOFFSET"][10] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch11Value);
    config["results"]["AMOFFSET"][11] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch12Value);
    config["results"]["AMOFFSET"][12] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch13Value);
    config["results"]["AMOFFSET"][13] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch14Value);
    config["results"]["AMOFFSET"][14] = counts;

    counts=amac->rdField(&AMACv2RegMap::Ch15Value);
    config["results"]["AMOFFSET"][15] = counts;

    logger(logDEBUG) << config["results"]["AMOFFSET"];


    // Disable offset calibration
    amac->wrField(&AMACv2RegMap::AMzeroCalib , 0);
    amac->wrField(&AMACv2RegMap::AMzeroCalibC, 0);

    configAMAC(amac, config, true);

    return config;
  }

  json calibrateNTC(std::shared_ptr<AMACv2> amac)
  {
    logger(logINFO) << "## Calibrating NTCs ##";

    json config;

    // Enable NTC calibration
    amac->wrField(&AMACv2RegMap::NTCpbCal, 0);
    amac->wrField(&AMACv2RegMap::NTCx0Cal, 0);
    amac->wrField(&AMACv2RegMap::NTCy0Cal, 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    double NTCx =amac->getNTCx ();
    double NTCy =amac->getNTCy ();
    double NTCpb=amac->getNTCpb();

    logger(logDEBUG) << "NTCx = " << NTCx << " mV";
    logger(logDEBUG) << "NTCy = " << NTCy << " mV";
    logger(logDEBUG) << "NTCpb= " << NTCpb<< " mV";

    config["results"]["NTCX"] = NTCx ;
    config["results"]["NTCY"] = NTCy ;
    config["results"]["NTCPB"]= NTCpb;

    // Disable NTC calibration
    amac->wrField(&AMACv2RegMap::NTCpbCal, 1);
    amac->wrField(&AMACv2RegMap::NTCx0Cal, 1);
    amac->wrField(&AMACv2RegMap::NTCy0Cal, 1);

    configAMAC(amac, config, true);

    return config;
  }

  json calibrateCur10V(std::shared_ptr<AMACv2> amac)
  {
    logger(logINFO) << "## Calibrating input current monitor block ##";

    json config;

    amac->wrField(&AMACv2RegMap::DCDCiZeroReading , 1); // Short the DCDCi inputs

    amac->wrField(&AMACv2RegMap::Ch12Mux , 0);

    double DV=0;
    for(uint i=0; i<100; i++)
      {
	std::this_thread::sleep_for(std::chrono::milliseconds(1));
    	DV+=amac->getADC(12);
      }
    DV/=100;

    config["results"]["CUR10VOFFSETZERO"] = DV;
    configAMAC(amac, config, true);

    amac->wrField(&AMACv2RegMap::DCDCiZeroReading , 0); // Separate the DCDCi inputs

    return config;
  }

  json calibrateCur1V(std::shared_ptr<AMACv2> amac)
  {
    logger(logINFO) << "## Calibrating output current monitor block ##";

    json config;

    amac->wrField(&AMACv2RegMap::DCDCoZeroReading , 1); // Short the DCDCo inputs

    amac->wrField(&AMACv2RegMap::Ch13Mux , 0);

    double DV=0;
    for(uint i=0; i<100; i++)
      {
    	usleep(5e3);
    	DV+=amac->getADC(13);
      }
    DV/=100;

    config["results"]["CUR1VOFFSET"] = DV;
    configAMAC(amac, config, true);

    amac->wrField(&AMACv2RegMap::DCDCoZeroReading , 0); // Separate the DCDCo inputs

    return config;
  }

  json calibrateTemperature (std::shared_ptr<AMACv2> amac)
  {
    logger(logINFO) << "## Calibrating the temperature scale ##";

    json config;

    uint32_t DCDCen_curr    =amac->rdField(&AMACv2::DCDCen);
    uint32_t CTAToffset_curr=amac->rdField(&AMACv2RegMap::CTAToffset);

    //
    // Check if bPOL is enabled, heating everything up
    if(DCDCen_curr>0)
      {
	amac->wrField(&AMACv2::DCDCen , 0);
	amac->wrField(&AMACv2::DCDCenC, 0);
      }

    //
    // Wait until PTAT stabilizes
    uint32_t cPTAT  = amac->rdField(&AMACv2::Ch15Value);
    uint32_t cPTATp = 0;
    while(abs(cPTAT-cPTATp)>1)
      {
	logger(logDEBUG) << "Cooling... PTAT = " << cPTAT << ", NTCpb = " << amac->temperaturePB() << "C";

	// This will take a while...
	std::this_thread::sleep_for(std::chrono::seconds(1));

	// Update tempreature
	cPTATp=cPTAT;
	cPTAT = amac->rdField(&AMACv2::Ch15Value);
      }

    //
    // Measure temperatures
    double PTAT  = amac->getPTAT();
    double NTCpb = amac->temperaturePB();

    // Assume
    //  m = 8.5mV/C (FEAST datasheet)
    //  Tnow = from NTCpb, in C
    //  PTAT(Tnow) = m * Tnow + PTAT0
    double PTAT0 = PTAT - 8.5 * NTCpb;
    logger(logDEBUG) << "PTAT0 = " << PTAT0;

    config["results"]["PTAT0"]=PTAT0;

    //
    // Calibrate CTAT's
    for(uint32_t CTAToffset_set=0; CTAToffset_set<16; CTAToffset_set++)
      {
	amac->wrField(&AMACv2RegMap::CTAToffset, CTAToffset_set);
	std::this_thread::sleep_for(std::chrono::milliseconds(5));

	double CTAT  = amac->getCTAT();
	double CTAT0 = CTAT + 1.5 * NTCpb;

	config["results"]["CTAT0"][CTAToffset_set]=CTAT0;
	logger(logDEBUG) << "CTAT0[" << CTAToffset_set << "] = " << CTAT0;
      }

    //
    // Re-enable DC/DC
    if(DCDCen_curr>0)
      {
	amac->wrField(&AMACv2::DCDCen , 1);
	amac->wrField(&AMACv2::DCDCenC, 1);
      }
    amac->wrField(&AMACv2RegMap::CTAToffset, CTAToffset_curr );

    configAMAC(amac, config, true);

    return config;
  }

  json scanPADID(uint32_t pbNum, std::shared_ptr<PBv3TB> tb)
  {
    logger(logINFO) << "## Determining AMAC PADID ##";

    json config;

    std::shared_ptr<AMACv2> amac=tb->getPB(pbNum);

    //Power cycle AMAC if the ID is already set
    if(amac->isCommIDSet()) {
      tb->setOFin(pbNum, true);
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
      tb->setOFin(pbNum, false);
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }

    for(uint8_t i=0; i<=16; i++) {
      try {
        amac->EndeavourCom::setid(EndeavourCom::REFMODE::IDPads, i);
        config["results"]["PADID"]=i;
      } catch(EndeavourComException &e) {
        if(i==16) {
          logger(logERROR) << "Could not determine PADID";
        }
      }
    }
        
    //Power cycle AMAC to allow for resetting of the ID
    tb->setOFin(pbNum, true);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    tb->setOFin(pbNum, false);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    return config;
  }

}
