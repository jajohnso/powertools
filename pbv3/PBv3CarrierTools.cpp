#include "PBv3CarrierTools.h"

#include "ComException.h"
#include "PowerSupplyChannel.h"

#include "PBv3Utils.h"

#include <memory>
#include <thread>

using namespace nlohmann;

namespace PBv3CarrierTools 
{
  json testLvEnable(std::shared_ptr<PBv3TBMassive> tb) 
  {
    logger(logINFO) << "## LV Enable test ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "LV_ENABLE";
    testSum["results"]["TIMESTART"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    tb->powerLVOn();

    // Wait to stabilize
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // read interesting values
    double Iin  = tb->getLVPS()->measureCurrent();
    double Vin  = tb->getLVPS()->measureVoltage();

    testSum["passed"] = (Iin<5e-3);

    if(testSum["passed"])
      logger(logINFO) << " --> Reading " << Iin << "A with " << Vin << "V supplied";
    else
      logger(logERROR) << " --> Reading " << Iin << "A with " << Vin << "V supplied";

    testSum["results"]["VIN"] = Vin;
    testSum["results"]["IIN"] = Iin;

    tb->powerLVOff();

    return testSum;
  }

  json testMuxes(std::shared_ptr<PBv3TBMassive> tb)
  {
    logger(logINFO) << "## Multiplexer test test ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "MUXCOM";
    testSum["results"]["TIMESTART"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    std::cout << "PBNUM" << "\t" << "OUTPUT" << "\t" << "VALUE" << std::endl;
    for(uint32_t i=0; i<10; i++)
      {
	float out=0;
	bool commok=true;
	try
	  {
	    out=tb->readCarrierOutput(i, PBv3TB::LINPOL1V4);
	  }
	catch(const ComException& e)
	  {
	    logger(logERROR) << "Error switching multiplexers!";
	    commok=false;
	  }
	std::cout << i << "\t" << "LINPOL1V4" << "\t" << out << std::endl;
	testSum["results"]["PBNUM" ][i] = i;
	testSum["results"]["OUTPUT"][i] = "LINPOL1V4";
	testSum["results"]["VALUE" ][i] = out;
	testSum["results"]["COMMOK"][i] = commok;
	testSum["passed"] = ((out<50e-3) || testSum["passed"]);
      }

    return testSum;
  }
}
