#include "AMACv2.h"

#include <math.h>
#include <unistd.h>

#include <chrono>
#include <thread>

#include "EndeavourComException.h"

const std::map<AMACv2::AM, std::string> AMACv2::Map_AMStr = {
    {AM::VDCDC    ,"VDCDC"    },
    {AM::VDDLR    ,"VDDLR"    },
    {AM::DCDCIN   ,"DCDCIN"   },
    {AM::VDDREG   ,"VDDREG"   },
    {AM::VDDBG    ,"VDDBG"    },
    {AM::AM900BG  ,"AM900BG"  },
    {AM::AM600BG  ,"AM600BG"  },
    {AM::CAL      ,"CAL"      },
    {AM::AMREF    ,"AMREF"    },
    {AM::CALX     ,"CALX"     },
    {AM::CALY     ,"CALY"     },
    {AM::SHUNTX   ,"SHUNTX"   },
    {AM::SHUNTY   ,"SHUNTY"   },
    {AM::DCDCADJ  ,"DCDCADJ"  },
    {AM::CTAT     ,"CTAT"     },
    {AM::NTCX     ,"NTCX"     },
    {AM::NTCY     ,"NTCY"     },
    {AM::NTCPB    ,"NTCPB"    },
    {AM::HREFX    ,"HREFX"    },
    {AM::HREFY    ,"HREFY"    },
    {AM::CUR10V   ,"CUR10V"   },
    {AM::CUR10VTPL,"CUR10VTPL"},
    {AM::CUR10VTPH,"CUR10VTPH"},
    {AM::CUR1V    ,"CUR1V"    },
    {AM::CUR1VTPL ,"CUR1VTPL" },
    {AM::CUR1VTPH ,"CUR1VTPH" },
    {AM::HVRET    ,"HVRET"    },
    {AM::PTAT     ,"PTAT"     },
};

AMACv2::AMACv2(unsigned short amacid, std::unique_ptr<EndeavourRaw> raw)
  : EndeavourCom(amacid, std::move(raw)), AMACv2RegMap()
{ }

AMACv2::~AMACv2(){}

void AMACv2::init()
{
  // Set AMAC ID
  if(!isCommIDSet())
    {
      EndeavourCom::setid(EndeavourCom::REFMODE::IDPads, m_padid);
      usleep(10);
    }

  // Load current register values
  loadRegisters();
}

void AMACv2::initRegisters()
{ 
  // Initialize registers with default values
  for(AMACv2Field AMACv2RegMap::* field : m_fields)
    {
      if((this->*field).canBeWrittenField())
	(this->*field).writeDefaultVal();
    }

  // Write AMAC registers into the chip
  for(const AMACv2Register *reg : getRegisters())
    {
      if(reg->isRW()!=RW)
	continue;

      EndeavourCom::write_reg(reg->getAddress(), getReg(reg->getAddress()));
    }
}

void AMACv2::loadRegisters()
{ 
  // Load register map with current values
  for(const AMACv2Register *reg : getRegisters())
    {
      if(reg->isRW()!=RW && reg->isRW()!=RO)
	continue;

      uint32_t value=EndeavourCom::read_reg(reg->getAddress());
      setReg(reg->getAddress(), value);
    }
}

bool AMACv2::isCommIDSet()
{
  try
    { read_reg(0); }
  catch(const EndeavourComException& e)
    { return false; }
  return true;
}

void AMACv2::syncReg(AMACv2Field AMACv2RegMap::* ref)
{
  AMACv2Register *reg=(this->*ref).getRegister();
  EndeavourCom::write_reg(reg->getAddress(), reg->getValue());
}

void AMACv2::wrField(AMACv2Field AMACv2RegMap::* ref, uint32_t data)
{
  setField(ref, data);
  AMACv2Register *reg=(this->*ref).getRegister();
  EndeavourCom::write_reg(reg->getAddress(), reg->getValue());
  usleep(1e4);
}

void AMACv2::wrField(const std::string& fieldName, uint32_t data)
{
  wrField(m_fieldMap[fieldName], data);
}

uint32_t AMACv2::rdField(AMACv2Field AMACv2RegMap::* ref)
{
  uint32_t ret = EndeavourCom::read_reg(getAddr(ref));  
  setReg(getAddr(ref), ret);
  return getField(ref);
}

uint32_t AMACv2::rdField(const std::string& fieldName)
{
  return rdField(m_fieldMap[fieldName]);
}

void AMACv2::write_reg(unsigned int address, unsigned int data)
{
  setReg(address, data);
  EndeavourCom::write_reg(address, data);
  usleep(1e4);
}

unsigned int AMACv2::read_reg(unsigned int address)
{
  uint32_t value=EndeavourCom::read_reg(address);
  setReg(address, value);
  return value;
}

void AMACv2::setPADID(uint8_t padid)
{
  m_padid=padid;
}

void AMACv2::setADCslope (double ADCslope)
{ 
  m_ADCslope=ADCslope;
}

void AMACv2::setADCoffset(uint8_t ch, uint32_t counts)
{
  if(ch>=16) return;
  m_ADCoffset[ch]=counts;
}

void AMACv2::setNTCxCal(double ntccal)
{ m_NTCxCal=ntccal; }

void AMACv2::setNTCyCal(double ntccal)
{ m_NTCyCal=ntccal; }

void AMACv2::setNTCpbCal(double ntccal)
{ m_NTCpbCal=ntccal; }

void AMACv2::setPTAT0(double ptat0)
{ m_PTAT0=ptat0; }

void AMACv2::setCTAT0(uint8_t offset, double ctat0)
{
  if(offset>=16) return;
  m_CTAT0[offset]=ctat0;
}

void AMACv2::setCur10VOffset(double offset)
{
  m_Cur10VOffset=offset;
}

double AMACv2::getCur10VOffset()
{
  return m_Cur10VOffset;
}

void AMACv2::setCur1VOffset(double offset)
{
  m_Cur1VOffset=offset;
}

double AMACv2::getCur1VOffset()
{
  return m_Cur1VOffset;
}

double AMACv2::calibrateCounts(uint8_t ch, uint32_t counts)
{
  if(ch>=16) return 0.;

  return m_ADCslope*(counts-m_ADCoffset[ch]);
}

uint8_t AMACv2::getPADID()
{
  return m_padid;
}

uint32_t AMACv2::readAM(AM am, uint32_t reads)
{
  AMACv2Field AMACv2RegMap::* ch;
  switch(am) {
    case AM::VDCDC:
      ch=&AMACv2::Ch0Value;
      break;

    case AM::VDDLR:
      ch=&AMACv2::Ch1Value;
      break;

    case AM::DCDCIN:
      ch=&AMACv2::Ch2Value;
      break;

    case AM::VDDREG:
      wrField(&AMACv2::Ch3Mux,0);
      ch=&AMACv2::Ch3Value;
      break;

    case AM::VDDBG:
      wrField(&AMACv2::Ch3Mux,1);
      ch=&AMACv2::Ch3Value;
      break;

    case AM::AM900BG:
      wrField(&AMACv2::Ch3Mux,2);
      ch=&AMACv2::Ch3Value;
      break;

    case AM::AM600BG:
      wrField(&AMACv2::Ch4Mux,0);
      ch=&AMACv2::Ch4Value;
      break;

    case AM::CAL:
      wrField(&AMACv2::Ch4Mux,1);
      ch=&AMACv2::Ch4Value;
      break;

    case AM::AMREF:
      wrField(&AMACv2::Ch4Mux,2);
      ch=&AMACv2::Ch4Value;
      break;

    case AM::CALX:
      wrField(&AMACv2::Ch5Mux,0);
      ch=&AMACv2::Ch5Value;
      break;

    case AM::CALY:
      wrField(&AMACv2::Ch5Mux,1);
      ch=&AMACv2::Ch5Value;
      break;

    case AM::SHUNTX:
      wrField(&AMACv2::Ch5Mux,2);
      ch=&AMACv2::Ch5Value;
      break;

    case AM::SHUNTY:
      wrField(&AMACv2::Ch5Mux,3);
      ch=&AMACv2::Ch5Value;
      break;

    case AM::DCDCADJ:
      wrField(&AMACv2::Ch5Mux,4);
      ch=&AMACv2::Ch5Value;
      break;

    case AM::CTAT:
      ch=&AMACv2::Ch6Value;
      break;

    case AM::NTCX:
      ch=&AMACv2::Ch7Value;
      break;

    case AM::NTCY:
      ch=&AMACv2::Ch8Value;
      break;

    case AM::NTCPB:
      ch=&AMACv2::Ch9Value;
      break;

    case AM::HREFX:
      ch=&AMACv2::Ch10Value;
      break;

    case AM::HREFY:
      ch=&AMACv2::Ch11Value;
      break;

    case AM::CUR10V:
      wrField(&AMACv2::Ch12Mux,0);
      ch=&AMACv2::Ch12Value;
      break;

    case AM::CUR10VTPL:
      wrField(&AMACv2::Ch12Mux,1);
      ch=&AMACv2::Ch12Value;
      break;

    case AM::CUR10VTPH:
      wrField(&AMACv2::Ch12Mux,2);
      ch=&AMACv2::Ch12Value;
      break;

    case AM::CUR1V:
      wrField(&AMACv2::Ch13Mux,0);
      ch=&AMACv2::Ch13Value;
      break;

    case AM::CUR1VTPL:
      wrField(&AMACv2::Ch13Mux,1);
      ch=&AMACv2::Ch13Value;
      break;

    case AM::CUR1VTPH:
      wrField(&AMACv2::Ch13Mux,2);
      ch=&AMACv2::Ch13Value;
      break;

    case AM::HVRET:
      ch=&AMACv2::Ch14Value;
      break;

    case AM::PTAT:
      ch=&AMACv2::Ch15Value;
      break;

    default:
      return 0;
    }

  uint32_t val=0;
  for(uint32_t i=0;i<reads;i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(2));
    val+=rdField(ch);
  }

  return val/reads;
}

double AMACv2::getADC(uint8_t ch, uint32_t reads)
{
  if(ch>=16) return 0.;
  double value=0;
  uint32_t counts;
  for(uint32_t i=0; i<reads; i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(2));
    counts=rdField("Ch"+std::to_string(ch)+"Value");
    value+=calibrateCounts(ch, counts);
  }

  return value/reads;
}

double AMACv2::getVDDREG()
{
  uint32_t counts=readAM(AM::VDDREG);
  return calibrateCounts(3, counts)*3/2;
}

double AMACv2::getAM600()
{
  uint32_t counts=readAM(AM::AM600BG);
  return calibrateCounts(4, counts);
}

double AMACv2::getAM900()
{
  uint32_t counts=readAM(AM::AM900BG);
  return calibrateCounts(3, counts);
}

double AMACv2::getNTCx()
{
  uint32_t counts=rdField(&AMACv2RegMap::Ch7Value);
  return calibrateCounts(7, counts);
}

double AMACv2::getNTCy()
{
  uint32_t counts=rdField(&AMACv2RegMap::Ch8Value);
  return calibrateCounts(8, counts);
}

double AMACv2::getNTCpb()
{
  uint32_t counts=rdField(&AMACv2RegMap::Ch9Value);
  return calibrateCounts(9, counts);
}

double AMACv2::getPTAT()
{
  uint32_t counts=rdField(&AMACv2RegMap::Ch15Value);
  return calibrateCounts(15, counts);
}

double AMACv2::getCTAT()
{
  uint32_t counts=rdField(&AMACv2RegMap::Ch6Value);
  return calibrateCounts(6, counts);
}

double AMACv2::temperatureX()
{ return 0; }

double AMACv2::temperatureY()
{ return 0; }

double AMACv2::temperaturePB()
{
  // Determine the settings
  uint32_t SR=rdField(&AMACv2RegMap::NTCpbSenseRange);
  bool NTCS0=(SR>>0)&1;
  bool NTCS1=(SR>>1)&1;
  bool NTCS2=(SR>>2)&1;

  // Readings
  double NTCvolt=getNTCpb();

  // AMACv2 gain resistors
  static const double R=200e3;
  static const double R0=133e3;
  static const double R1=50e3;
  static const double R2=22.22e3;

  double Rgain=1/(1/R+NTCS0/R0+NTCS1/R1+NTCS2/R2);

  // NTC resistance
  double Rntc=Rgain*m_NTCpbCal/(NTCvolt-m_NTCpbCal)-280-250;

  // And finally calculate temperature
  static const double B=3900;
  static const double T0=275.15;
  static const double R25=10e3;
  static const double T25=T0+25;
  double temp=1./(log(Rntc/R25)/B+1/T25)-T0;

  return temp;
}

double AMACv2::temperaturePTAT()
{
  return (getPTAT()-m_PTAT0)/8.5;
}

double AMACv2::temperatureCTAT()
{
  uint32_t CTAToffset=rdField(&AMACv2RegMap::CTAToffset);
  return (getCTAT()-m_CTAT0[CTAToffset])/(-1.5);
}

double AMACv2::convertCur10V(uint32_t Cur10V)
{
  return (calibrateCounts(12, Cur10V)-m_Cur10VOffset)/10.4/33e-3;
}

double AMACv2::convertCur1V(uint32_t Cur1V)
{
  return (calibrateCounts(13, Cur1V )-m_Cur1VOffset )/30  /7.6e-3;
}

double AMACv2::convertVDCDC(uint32_t VDCDC)
{
  return 2*calibrateCounts(0, VDCDC)/1e3;
}

uint32_t AMACv2::readEFuse()
{
  return rdField(&AMACv2RegMap::SerNum);
}
