#include "EndeavourRawUIOMux.h"

#include <chrono>
#include <thread>

EndeavourRawUIOMux::EndeavourRawUIOMux(uint8_t code, std::shared_ptr<IOExpander> sel, std::shared_ptr<DeviceCom> fpgaCom)
  : EndeavourRawUIOMux(code, false, false, sel, fpgaCom)
{ }

EndeavourRawUIOMux::EndeavourRawUIOMux(uint8_t code, bool invertCMDin, bool invertCMDout, std::shared_ptr<IOExpander> sel, std::shared_ptr<DeviceCom> fpgaCom)
  : EndeavourRawUIO(fpgaCom),
    m_code(code), m_invertCMDin(invertCMDin), m_invertCMDout(invertCMDout), m_sel(sel)
{ }

void EndeavourRawUIOMux::sendData(unsigned long long int data, unsigned int size)
{
  uint32_t curr_val=m_sel->read();

  if((curr_val&0xF) != (m_code&0xF))
  {
      m_sel->write((curr_val&0xC0)|(0x10)|(m_code&0x0F));
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
      m_sel->write((curr_val&0xC0)|(0x00)|(m_code&0x0F));
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
  invertCMDin (m_invertCMDin );
  invertCMDout(m_invertCMDout);
  EndeavourRawUIO::sendData(data, size);
}
