#ifndef AMACREGMAP_H
#define AMACREGMAP_H

#include <memory>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <stdint.h>
#include <string>

#include "AMACv2Field.h"

//! \brief AMACv2 register map
/**
 * Contains definition of all fields and registers inside AMAC
 */
class AMACv2RegMap
{
private:
  void init();

public:
  AMACv2RegMap();

  std::vector<const AMACv2Register *> getRegisters() const;
  std::vector<const AMACv2Field    *> getFields() const;

  AMACv2Field* findField(AMACv2Field AMACv2RegMap::* ref);
  AMACv2Field* findField(const std::string& fieldName);

  uint32_t getField(AMACv2Field AMACv2RegMap::* ref);
  uint32_t getField(const std::string& fieldName);

  uint32_t getReg(uint32_t reg);
  uint32_t getReg(AMACv2Register AMACv2RegMap::* ref);

  void setField(AMACv2Field AMACv2RegMap::* ref, uint32_t value);
  void setField(const std::string& fieldName, uint32_t value);

  void setReg(uint32_t reg, uint32_t value);
  void setReg(AMACv2Register AMACv2RegMap::* ref, uint32_t value);

  uint8_t getAddr(AMACv2Field AMACv2RegMap::* ref);
  uint8_t getAddr(const std::string& fieldName);

  bool canBeWritten(AMACv2Field AMACv2RegMap::* ref);

  /** \name Registers
   * @{ */
  AMACv2Register RegisterStatus    ={"Status"    ,  0, RO};
  AMACv2Register RegisterHxFlags   ={"HxFlags"   ,  1, RO};
  AMACv2Register RegisterHyFlags   ={"HyFlags"   ,  2, RO};
  AMACv2Register RegisterHV0Flags  ={"HV0Flags"  ,  3, RO};
  AMACv2Register RegisterHV2Flags  ={"HV2Flags"  ,  4, RO};
  AMACv2Register RegisterDCDCFlags ={"DCDCFlags" ,  5, RO};
  AMACv2Register RegisterWRNHxFlags={"WRNHxFlags",  6, RO};
  AMACv2Register RegisterSynFlags  ={"SynFlags"  ,  7, RO};

  AMACv2Register RegisterValue0={"Value0", 10, RO};
  AMACv2Register RegisterValue1={"Value1", 11, RO};
  AMACv2Register RegisterValue2={"Value2", 12, RO};
  AMACv2Register RegisterValue3={"Value3", 13, RO};
  AMACv2Register RegisterValue4={"Value4", 14, RO};
  AMACv2Register RegisterValue5={"Value5", 15, RO};

  AMACv2Register RegisterSerNum={"SerNum", 31, RO};

  AMACv2Register RegisterFlagResets={"FlagResets", 32, WO};
  AMACv2Register RegisterLogicReset={"LogicReset", 33, WO};
  AMACv2Register RegisterHardReset ={"HardReset" , 34, WO};

  AMACv2Register RegisterCntSet  ={"CntSet"  , 40, RW};
  AMACv2Register RegisterCntSetC ={"CntSetC" , 41, RW};
  AMACv2Register RegisterDCDCen  ={"DCDCen"  , 42, RW};
  AMACv2Register RegisterDCDCenC ={"DCDCenC" , 43, RW};
  AMACv2Register RegisterIlock   ={"Ilock"   , 44, RW};
  AMACv2Register RegisterIlockC  ={"IlockC"  , 45, RW};
  AMACv2Register RegisterRstCnt  ={"RstCnt"  , 46, RW};
  AMACv2Register RegisterRstCntC ={"RstCntC" , 47, RW};
  AMACv2Register RegisterAMen    ={"AMen"    , 48, RW};
  AMACv2Register RegisterAMenC   ={"AMenC"   , 49, RW};
  AMACv2Register RegisterAMpwr   ={"AMpwr"   , 50, RW};
  AMACv2Register RegisterAMpwrC  ={"AMpwrC"  , 51, RW};
  AMACv2Register RegisterBgCnt   ={"BgCnt"   , 52, RW};
  AMACv2Register RegisterAMCnt   ={"AMCnt"   , 53, RW};
  AMACv2Register RegisterDACs0   ={"DACs0"   , 54, RW};
  AMACv2Register RegisterDACbias ={"DACbias" , 55, RW};
  AMACv2Register RegisterAMACCnt ={"AMACCnt" , 56, RW};
  AMACv2Register RegisterNTCRange={"NTCRange", 57, RW};
  AMACv2Register RegisterLVCurCal={"LVCurCal", 58, RW};

  AMACv2Register RegisterHxICfg  ={"HxICfg"  , 60, RW};
  AMACv2Register RegisterHyICfg  ={"HyICfg"  , 61, RW};
  AMACv2Register RegisterHV0ICfg ={"HV0ICfg" , 62, RW};
  AMACv2Register RegisterHV2ICfg ={"HV2ICfg" , 63, RW};
  AMACv2Register RegisterDCDCICfg={"DCDCICfg", 64, RW};
  AMACv2Register RegisterWRNICfg ={"WRNICfg" , 65, RW};

  AMACv2Register RegisterHxTLUT     ={"HxTLUT"     , 70, RW};
  AMACv2Register RegisterHxModLUT1  ={"HxModLUT1"  , 71, RW};
  AMACv2Register RegisterHxModLUT2  ={"HxModLUT2"  , 72, RW};
  AMACv2Register RegisterHyTLUT     ={"HyTLUT"     , 73, RW};
  AMACv2Register RegisterHyModLUT1  ={"HyModLUT1"  , 74, RW};
  AMACv2Register RegisterHyModLUT2  ={"HyModLUT2"  , 75, RW};
  AMACv2Register RegisterHV0TLUT    ={"HV0TLUT"    , 76, RW};
  AMACv2Register RegisterHV0ModLUT1 ={"HV0ModLUT1" , 77, RW};
  AMACv2Register RegisterHV0ModLUT2 ={"HV0ModLUT2" , 78, RW};
  AMACv2Register RegisterHV2TLUT    ={"HV2TLUT"    , 79, RW};
  AMACv2Register RegisterHV2ModLUT1 ={"HV2ModLUT1" , 80, RW};
  AMACv2Register RegisterHV2ModLUT2 ={"HV2ModLUT2" , 81, RW};
  AMACv2Register RegisterDCDCTLUT   ={"DCDCTLUT"   , 82, RW};
  AMACv2Register RegisterDCDCModLUT1={"DCDCModLUT1", 83, RW};
  AMACv2Register RegisterDCDCModLUT2={"DCDCModLUT2", 84, RW};
  AMACv2Register RegisterWRNTLUT    ={"WRNTLUT"    , 85, RW};
  AMACv2Register RegisterWRNModLUT1 ={"WRNModLUT1" , 86, RW};
  AMACv2Register RegisterWRNModLUT2 ={"WRNModLUT2" , 87, RW};

  AMACv2Register RegisterHxFlagEn  ={"HxFlagEn"  , 90, RW};
  AMACv2Register RegisterHyFlagEn  ={"HyFlagEn"  , 91, RW};
  AMACv2Register RegisterHV0FlagEn ={"HV0FlagEn" , 92, RW};
  AMACv2Register RegisterHV2FlagEn ={"HV2FlagEn" , 93, RW};
  AMACv2Register RegisterDCDCFlagEn={"DCDCFlagEn", 94, RW};
  AMACv2Register RegisterSynFlagEn ={"SynFlagEn" , 95, RW};
  //AMACv2Register RegisterWRNFlagEn={"", 95, RW};

  AMACv2Register RegisterHxLoTh0  ={"HxLoTh0"  ,100, RW};
  AMACv2Register RegisterHxLoTh1  ={"HxLoTh1"  ,101, RW};
  AMACv2Register RegisterHxLoTh2  ={"HxLoTh2"  ,102, RW};
  AMACv2Register RegisterHxLoTh3  ={"HxLoTh3"  ,103, RW};
  AMACv2Register RegisterHxLoTh4  ={"HxLoTh4"  ,104, RW};
  AMACv2Register RegisterHxLoTh5  ={"HxLoTh5"  ,105, RW};
  AMACv2Register RegisterHxHiTh0  ={"HxHiTh0"  ,106, RW};
  AMACv2Register RegisterHxHiTh1  ={"HxHiTh1"  ,107, RW};
  AMACv2Register RegisterHxHiTh2  ={"HxHiTh2"  ,108, RW};
  AMACv2Register RegisterHxHiTh3  ={"HxHiTh3"  ,109, RW};
  AMACv2Register RegisterHxHiTh4  ={"HxHiTh4"  ,110, RW};
  AMACv2Register RegisterHxHiTh5  ={"HxHiTh5"  ,111, RW};
  AMACv2Register RegisterHyLoTh0  ={"HyLoTh0"  ,112, RW};
  AMACv2Register RegisterHyLoTh1  ={"HyLoTh1"  ,113, RW};
  AMACv2Register RegisterHyLoTh2  ={"HyLoTh2"  ,114, RW};
  AMACv2Register RegisterHyLoTh3  ={"HyLoTh3"  ,115, RW};
  AMACv2Register RegisterHyLoTh4  ={"HyLoTh4"  ,116, RW};
  AMACv2Register RegisterHyLoTh5  ={"HyLoTh5"  ,117, RW};
  AMACv2Register RegisterHyHiTh0  ={"HyHiTh0"  ,118, RW};
  AMACv2Register RegisterHyHiTh1  ={"HyHiTh1"  ,119, RW};
  AMACv2Register RegisterHyHiTh2  ={"HyHiTh2"  ,120, RW};
  AMACv2Register RegisterHyHiTh3  ={"HyHiTh3"  ,121, RW};
  AMACv2Register RegisterHyHiTh4  ={"HyHiTh4"  ,122, RW};
  AMACv2Register RegisterHyHiTh5  ={"HyHiTh5"  ,123, RW};
  AMACv2Register RegisterHV0LoTh0 ={"HV0LoTh0" ,124, RW};
  AMACv2Register RegisterHV0LoTh1 ={"HV0LoTh1" ,125, RW};
  AMACv2Register RegisterHV0LoTh2 ={"HV0LoTh2" ,126, RW};
  AMACv2Register RegisterHV0LoTh3 ={"HV0LoTh3" ,127, RW};
  AMACv2Register RegisterHV0LoTh4 ={"HV0LoTh4" ,128, RW};
  AMACv2Register RegisterHV0LoTh5 ={"HV0LoTh5" ,129, RW};
  AMACv2Register RegisterHV0HiTh0 ={"HV0HiTh0" ,130, RW};
  AMACv2Register RegisterHV0HiTh1 ={"HV0HiTh1" ,131, RW};
  AMACv2Register RegisterHV0HiTh2 ={"HV0HiTh2" ,132, RW};
  AMACv2Register RegisterHV0HiTh3 ={"HV0HiTh3" ,133, RW};
  AMACv2Register RegisterHV0HiTh4 ={"HV0HiTh4" ,134, RW};
  AMACv2Register RegisterHV0HiTh5 ={"HV0HiTh5" ,135, RW};
  AMACv2Register RegisterHV2LoTh0 ={"HV2LoTh0" ,136, RW};
  AMACv2Register RegisterHV2LoTh1 ={"HV2LoTh1" ,137, RW};
  AMACv2Register RegisterHV2LoTh2 ={"HV2LoTh2" ,138, RW};
  AMACv2Register RegisterHV2LoTh3 ={"HV2LoTh3" ,139, RW};
  AMACv2Register RegisterHV2LoTh4 ={"HV2LoTh4" ,140, RW};
  AMACv2Register RegisterHV2LoTh5 ={"HV2LoTh5" ,141, RW};
  AMACv2Register RegisterHV2HiTh0 ={"HV2HiTh0" ,142, RW};
  AMACv2Register RegisterHV2HiTh1 ={"HV2HiTh1" ,143, RW};
  AMACv2Register RegisterHV2HiTh2 ={"HV2HiTh2" ,144, RW};
  AMACv2Register RegisterHV2HiTh3 ={"HV2HiTh3" ,145, RW};
  AMACv2Register RegisterHV2HiTh4 ={"HV2HiTh4" ,146, RW};
  AMACv2Register RegisterHV2HiTh5 ={"HV2HiTh5" ,147, RW};
  AMACv2Register RegisterDCDCLoTh0={"DCDCLoTh0",148, RW};
  AMACv2Register RegisterDCDCLoTh1={"DCDCLoTh1",149, RW};
  AMACv2Register RegisterDCDCLoTh2={"DCDCLoTh2",150, RW};
  AMACv2Register RegisterDCDCLoTh3={"DCDCLoTh3",151, RW};
  AMACv2Register RegisterDCDCLoTh4={"DCDCLoTh4",152, RW};
  AMACv2Register RegisterDCDCLoTh5={"DCDCLoTh5",153, RW};
  AMACv2Register RegisterDCDCHiTh0={"DCDCHiTh0",154, RW};
  AMACv2Register RegisterDCDCHiTh1={"DCDCHiTh1",155, RW};
  AMACv2Register RegisterDCDCHiTh2={"DCDCHiTh2",156, RW};
  AMACv2Register RegisterDCDCHiTh3={"DCDCHiTh3",157, RW};
  AMACv2Register RegisterDCDCHiTh4={"DCDCHiTh4",158, RW};
  AMACv2Register RegisterDCDCHiTh5={"DCDCHiTh5",159, RW};
  AMACv2Register RegisterWRNLoTh0 ={"WRNLoTh0" ,160, RW};
  AMACv2Register RegisterWRNLoTh1 ={"WRNLoTh1" ,161, RW};
  AMACv2Register RegisterWRNLoTh2 ={"WRNLoTh2" ,162, RW};
  AMACv2Register RegisterWRNLoTh3 ={"WRNLoTh3" ,163, RW};
  AMACv2Register RegisterWRNLoTh4 ={"WRNLoTh4" ,164, RW};
  AMACv2Register RegisterWRNLoTh5 ={"WRNLoTh5" ,165, RW};
  AMACv2Register RegisterWRNHiTh0 ={"WRNHiTh0" ,166, RW};
  AMACv2Register RegisterWRNHiTh1 ={"WRNHiTh1" ,167, RW};
  AMACv2Register RegisterWRNHiTh2 ={"WRNHiTh2" ,168, RW};
  AMACv2Register RegisterWRNHiTh3 ={"WRNHiTh3" ,169, RW};
  AMACv2Register RegisterWRNHiTh4 ={"WRNHiTh4" ,170, RW};
  AMACv2Register RegisterWRNHiTh5 ={"WRNHiTh5" ,171, RW};

  /** @} */
  
  /** \name Fields
   * @{ */
  // 0 - Status register
  AMACv2Field StatusAM       ={"StatusAM"       , &RegisterStatus, 31, 1, 0};
  AMACv2Field StatusWARN     ={"StatusWARN"     , &RegisterStatus, 29, 1, 0};
  AMACv2Field StatusDCDC     ={"StatusDCDC"     , &RegisterStatus, 28, 1, 0};
  AMACv2Field StatusHV3      ={"StatusHV3"      , &RegisterStatus, 27, 1, 0};
  AMACv2Field StatusHV2      ={"StatusHV2"      , &RegisterStatus, 26, 1, 0};
  AMACv2Field StatusHV1      ={"StatusHV1"      , &RegisterStatus, 25, 1, 0};
  AMACv2Field StatusHV0      ={"StatusHV0"      , &RegisterStatus, 24, 1, 0};
  AMACv2Field StatusY2LDO    ={"StatusY2LDO"    , &RegisterStatus, 22, 1, 0};
  AMACv2Field StatusY1LDO    ={"StatusY1LDO"    , &RegisterStatus, 21, 1, 0};
  AMACv2Field StatusY0LDO    ={"StatusY0LDO"    , &RegisterStatus, 20, 1, 0};
  AMACv2Field StatusX2LDO    ={"StatusX2LDO"    , &RegisterStatus, 18, 1, 0};
  AMACv2Field StatusX1LDO    ={"StatusX1LDO"    , &RegisterStatus, 17, 1, 0};
  AMACv2Field StatusX0LDO    ={"StatusX0LDO"    , &RegisterStatus, 16, 1, 0};
  AMACv2Field StatusGPI      ={"StatusGPI"      , &RegisterStatus, 12, 1, 0};
  AMACv2Field StatusPGOOD    ={"StatusPGOOD"    , &RegisterStatus,  8, 1, 0};
  AMACv2Field StatusILockWARN={"StatusILockWARN", &RegisterStatus,  5, 1, 0};
  AMACv2Field StatusILockDCDC={"StatusILockDCDC", &RegisterStatus,  4, 1, 0};
  AMACv2Field StatusILockHV2 ={"StatusILockHV2" , &RegisterStatus,  3, 1, 0};
  AMACv2Field StatusILockHV0 ={"StatusILockHV2" , &RegisterStatus,  2, 1, 0};
  AMACv2Field StatusILockYLDO={"StatusILockYLDO", &RegisterStatus,  1, 1, 0};
  AMACv2Field StatusILockxLDO={"StatusILockxLDO", &RegisterStatus,  0, 1, 0};
  // 1 - HxFlags
  AMACv2Field HxFlagsHi ={"HxFlagsHi", &RegisterHxFlags, 16, 16, 0x0};
  AMACv2Field HxFlagsLo ={"HxFlagsLo", &RegisterHxFlags,  0, 16, 0x0};
  // 2 - HyFlags
  AMACv2Field HyFlagsHi ={"HyFlagsHi", &RegisterHyFlags, 16, 16, 0x0};
  AMACv2Field HyFlagsLo ={"HyFlagsLo", &RegisterHyFlags, 0, 16, 0x0};
  // 3 - HV0Flags
  AMACv2Field HV0FlagsHi ={"HV0FlagsHi", &RegisterHV0Flags, 16, 16, 0x0};
  AMACv2Field HV0FlagsLo ={"HV0FlagsLo", &RegisterHV0Flags, 0, 16, 0x0};
  // 4 - HV2Flags
  AMACv2Field HV2FlagsHi ={"HV2FlagsHi", &RegisterHV2Flags, 16, 16, 0x0};
  AMACv2Field HV2FlagsLo ={"HV2FlagsLo", &RegisterHV2Flags, 0, 16, 0x0};
  // 5 - DCDCFlags
  AMACv2Field DCDCflagsHi ={"DCDCflagsHi", &RegisterDCDCFlags, 16, 16, 0x0};
  AMACv2Field DCDCflagsLo ={"DCDCflagsLo", &RegisterDCDCFlags, 0, 16, 0x0};
  // 6 - WRNHxFlags
  AMACv2Field WRNflagsHi ={"WRNflagsHi", &RegisterWRNHxFlags, 16, 16, 0x0};
  AMACv2Field WRNflagsLo ={"WRNflagsLo", &RegisterWRNHxFlags, 0, 16, 0x0};
  // 7 - SynFlags
  AMACv2Field SynFlagsHx  ={"SynFlagsHx"  , &RegisterSynFlags,  0, 2, 0x0};
  AMACv2Field SynFlagsHy  ={"SynFlagsHy"  , &RegisterSynFlags,  4, 2, 0x0};
  AMACv2Field SynFlagsHV0 ={"SynFlagsHV0" , &RegisterSynFlags,  8, 2, 0x0};
  AMACv2Field SynFlagsHV2 ={"SynFlagsHV2" , &RegisterSynFlags, 12, 2, 0x0};
  AMACv2Field SynFlagsDCDC={"SynFlagsDCDC", &RegisterSynFlags, 16, 2, 0x0};
  AMACv2Field SynFlagsWRN ={"SynFlagsWRN" , &RegisterSynFlags, 20, 2, 0x0};
  // 10 - Value0
  AMACv2Field Value0AMen ={"Value0AMen", &RegisterValue0, 31, 1, 0x0};
  AMACv2Field Ch0Value ={"Ch0Value", &RegisterValue0, 0, 10, 0x0};
  AMACv2Field Ch1Value ={"Ch1Value", &RegisterValue0, 10, 10, 0x0};
  AMACv2Field Ch2Value ={"Ch2Value", &RegisterValue0, 20, 10, 0x0};
  // 11 - Value1
  AMACv2Field Value1AMen ={"Value1AMen", &RegisterValue1, 31, 1, 0x0};
  AMACv2Field Ch3Value ={"Ch3Value", &RegisterValue1, 0, 10, 0x0};
  AMACv2Field Ch4Value ={"Ch4Value", &RegisterValue1, 10, 10, 0x0};
  AMACv2Field Ch5Value ={"Ch5Value", &RegisterValue1, 20, 10, 0x0};
  // 12 - Value2
  AMACv2Field Value2AMen ={"Value2AMen", &RegisterValue2, 31, 1, 0x0};
  AMACv2Field Ch6Value ={"Ch6Value", &RegisterValue2, 0, 10, 0x0};
  AMACv2Field Ch7Value ={"Ch7Value", &RegisterValue2, 10, 10, 0x0};
  AMACv2Field Ch8Value ={"Ch8Value", &RegisterValue2, 20, 10, 0x0};
  // 13 - Value3
  AMACv2Field Value3AMen ={"Value3AMen", &RegisterValue3, 31, 1, 0x0};
  AMACv2Field Ch9Value ={"Ch9Value", &RegisterValue3, 0, 10, 0x0};
  AMACv2Field Ch10Value ={"Ch10Value", &RegisterValue3, 10, 10, 0x0};
  AMACv2Field Ch11Value ={"Ch11Value", &RegisterValue3, 20, 10, 0x0};
  // 14 - Value4
  AMACv2Field Value4AMen ={"Value4AMen", &RegisterValue4, 31, 1, 0x0};
  AMACv2Field Ch12Value ={"Ch12Value", &RegisterValue4, 0, 10, 0x0};
  AMACv2Field Ch13Value ={"Ch13Value", &RegisterValue4, 10, 10, 0x0};
  AMACv2Field Ch14Value ={"Ch14Value", &RegisterValue4, 20, 10, 0x0};
  // 15 - Value5
  AMACv2Field Value5AMen ={"Value5AMen", &RegisterValue5, 31, 1, 0x0};
  AMACv2Field Ch15Value ={"Ch15Value", &RegisterValue5, 0, 10, 0x0};
  // 31 - SerNum
  AMACv2Field PadID ={"PadID", &RegisterSerNum, 24, 5, 0x0};
  AMACv2Field SerNum ={"SerNum", &RegisterSerNum, 0, 16, 0x0};
  // 32 - FlagResets
  AMACv2Field FlagResetWRN ={"FlagResetWRN", &RegisterFlagResets, 5, 1, 0x0};
  AMACv2Field FlagResetDCDC ={"FlagResetDCDC", &RegisterFlagResets, 4, 1, 0x0};
  AMACv2Field FlagResetHV2 ={"FlagResetHV2", &RegisterFlagResets, 3, 1, 0x0};
  AMACv2Field FlagResetHV0 ={"FlagResetHV0", &RegisterFlagResets, 2, 1, 0x0};
  AMACv2Field FlagResetXLDO ={"FlagResetXLDO", &RegisterFlagResets, 1, 1, 0x0};
  AMACv2Field FlagResetYLDO ={"FlagResetYLDO", &RegisterFlagResets, 0, 1, 0x0};
  // 33 - LogicReset
  AMACv2Field LogicReset ={"LogicReset", &RegisterLogicReset, 0, 32, 0x0};
  // 34 - HardReset
  AMACv2Field HardReset ={"HardReset", &RegisterHardReset, 0, 32, 0x0};
  // 40 - CntSet
  AMACv2Field CntSetHV3frq ={"CntSetHV3frq", &RegisterCntSet, 29, 2, 0x3};
  AMACv2Field CntSetHV3en ={"CntSetHV3en", &RegisterCntSet, 28, 1, 0x0};
  AMACv2Field CntSetHV2frq ={"CntSetHV2frq", &RegisterCntSet, 25, 2, 0x3};
  AMACv2Field CntSetHV2en ={"CntSetHV2en", &RegisterCntSet, 24, 1, 0x0};
  AMACv2Field CntSetHV1frq ={"CntSetHV1frq", &RegisterCntSet, 21, 2, 0x3};
  AMACv2Field CntSetHV1en ={"CntSetHV1en", &RegisterCntSet, 20, 1, 0x0};
  AMACv2Field CntSetHV0frq ={"CntSetHV0frq", &RegisterCntSet, 17, 2, 0x3};
  AMACv2Field CntSetHV0en ={"CntSetHV0en", &RegisterCntSet, 16, 1, 0x0};
  AMACv2Field CntSetHyLDO2en ={"CntSetHyLDO2en", &RegisterCntSet, 14, 1, 0x0};
  AMACv2Field CntSetHyLDO1en ={"CntSetHyLDO1en", &RegisterCntSet, 13, 1, 0x0};
  AMACv2Field CntSetHyLDO0en ={"CntSetHyLDO0en", &RegisterCntSet, 12, 1, 0x0};
  AMACv2Field CntSetHxLDO2en ={"CntSetHxLDO2en", &RegisterCntSet, 10, 1, 0x0};
  AMACv2Field CntSetHxLDO1en ={"CntSetHxLDO1en", &RegisterCntSet, 9, 1, 0x0};
  AMACv2Field CntSetHxLDO0en ={"CntSetHxLDO0en", &RegisterCntSet, 8, 1, 0x0};
  AMACv2Field CntSetWARN ={"CntSetWARN", &RegisterCntSet, 4, 1, 0x0};
  // 41 - CntSetC
  AMACv2Field CntSetCHV3frq ={"CntSetCHV3frq", &RegisterCntSetC, 29, 2, 0x3};
  AMACv2Field CntSetCHV3en ={"CntSetCHV3en", &RegisterCntSetC, 28, 1, 0x0};
  AMACv2Field CntSetCHV2frq ={"CntSetCHV2frq", &RegisterCntSetC, 25, 2, 0x3};
  AMACv2Field CntSetCHV2en ={"CntSetCHV2en", &RegisterCntSetC, 24, 1, 0x0};
  AMACv2Field CntSetCHV1frq ={"CntSetCHV1frq", &RegisterCntSetC, 21, 2, 0x3};
  AMACv2Field CntSetCHV1en ={"CntSetCHV1en", &RegisterCntSetC, 20, 1, 0x0};
  AMACv2Field CntSetCHV0frq ={"CntSetCHV0frq", &RegisterCntSetC, 17, 2, 0x3};
  AMACv2Field CntSetCHV0en ={"CntSetCHV0en", &RegisterCntSetC, 16, 1, 0x0};
  AMACv2Field CntSetCHyLDO2en ={"CntSetCHyLDO2en", &RegisterCntSetC, 14, 1, 0x0};
  AMACv2Field CntSetCHyLDO1en ={"CntSetCHyLDO1en", &RegisterCntSetC, 13, 1, 0x0};
  AMACv2Field CntSetCHyLDO0en ={"CntSetCHyLDO0en", &RegisterCntSetC, 12, 1, 0x0};
  AMACv2Field CntSetCHxLDO2en ={"CntSetCHxLDO2en", &RegisterCntSetC, 10, 1, 0x0};
  AMACv2Field CntSetCHxLDO1en ={"CntSetCHxLDO1en", &RegisterCntSetC, 9, 1, 0x0};
  AMACv2Field CntSetCHxLDO0en ={"CntSetCHxLDO0en", &RegisterCntSetC, 8, 1, 0x0};
  AMACv2Field CntSetCWARN ={"CntSetCWARN", &RegisterCntSetC, 4, 1, 0x0};
  // 42 - DCDCen
  AMACv2Field DCDCAdj ={"DCDCAdj", &RegisterDCDCen, 4, 2, 0x0};
  AMACv2Field DCDCen ={"DCDCen", &RegisterDCDCen, 0, 1, 0x0};
  // 43 - DCDCenC
  AMACv2Field DCDCAdjC ={"DCDCAdjC", &RegisterDCDCenC, 4, 2, 0x0};
  AMACv2Field DCDCenC ={"DCDCenC", &RegisterDCDCenC, 0, 1, 0x0};
  // 44 - Ilock
  AMACv2Field IlockHx  ={"IlockHx"  , &RegisterIlock, 0, 1, 0x0};
  AMACv2Field IlockHy  ={"IlockHy"  , &RegisterIlock, 1, 1, 0x0};
  AMACv2Field IlockHV0 ={"IlockHV0" , &RegisterIlock, 2, 1, 0x0};
  AMACv2Field IlockHV2 ={"IlockHV2" , &RegisterIlock, 3, 1, 0x0};
  AMACv2Field IlockDCDC={"IlockDCDC", &RegisterIlock, 4, 1, 0x0};
  AMACv2Field IlockWRN ={"IlockWRN" , &RegisterIlock, 5, 1, 0x0};
  // 45 - IlockC
  AMACv2Field IlockCHx  ={"IlockCHx"  , &RegisterIlockC, 0, 1, 0x0};
  AMACv2Field IlockCHy  ={"IlockCHy"  , &RegisterIlockC, 1, 1, 0x0};
  AMACv2Field IlockCHV0 ={"IlockCHV0" , &RegisterIlockC, 2, 1, 0x0};
  AMACv2Field IlockCHV2 ={"IlockCHV2" , &RegisterIlockC, 3, 1, 0x0};
  AMACv2Field IlockCDCDC={"IlockCDCDC", &RegisterIlockC, 4, 1, 0x0};
  AMACv2Field IlockCWRN ={"IlockCWRN" , &RegisterIlockC, 5, 1, 0x0};
  // 46 - RstCnt
  AMACv2Field RstCntHyHCCresetB ={"RstCntHyHCCresetB", &RegisterRstCnt, 16, 1, 0x0};
  AMACv2Field RstCntHxHCCresetB ={"RstCntHxHCCresetB", &RegisterRstCnt, 8, 1, 0x0};
  AMACv2Field RstCntOF ={"RstCntOF", &RegisterRstCnt, 0, 1, 0x0};
  // 47 - RstCntC
  AMACv2Field RstCntCHyHCCresetB ={"RstCntCHyHCCresetB", &RegisterRstCntC, 16, 1, 0x0};
  AMACv2Field RstCntCHxHCCresetB ={"RstCntCHxHCCresetB", &RegisterRstCntC, 8, 1, 0x0};
  AMACv2Field RstCntCOF ={"RstCntCOF", &RegisterRstCntC, 0, 1, 0x0};
  // 48 - AMen
  AMACv2Field AMzeroCalib ={"AMzeroCalib", &RegisterAMen, 8, 1, 0x0};
  AMACv2Field AMen ={"AMen", &RegisterAMen, 0, 1, 0x1};
  // 49 - AMenC
  AMACv2Field AMzeroCalibC ={"AMzeroCalibC", &RegisterAMenC, 8, 1, 0x0};
  AMACv2Field AMenC ={"AMenC", &RegisterAMenC, 0, 1, 0x1};
  // 50 - AMpwr
  AMACv2Field ReqDCDCPGOOD ={"ReqDCDCPGOOD", &RegisterAMpwr, 8, 1, 0x1};
  AMACv2Field DCDCenToPwrAMAC ={"DCDCenToPwrAMAC", &RegisterAMpwr, 0, 1, 0x0};
  // 51 - AMpwrC
  AMACv2Field ReqDCDCPGOODC ={"ReqDCDCPGOODC", &RegisterAMpwrC, 8, 1, 0x1};
  AMACv2Field DCDCenToPwrAMACC ={"DCDCenToPwrAMACC", &RegisterAMpwrC, 0, 1, 0x0};
  // 52 - BgCnt
  AMACv2Field AMbgen  ={"AMbgen" , &RegisterBgCnt, 15, 1, 0x0};
  AMACv2Field AMbg    ={"AMbg"   , &RegisterBgCnt,  8, 5, 0x0};
  AMACv2Field VDDbgen ={"VDDbgen", &RegisterBgCnt,  7, 1, 0x0};
  AMACv2Field VDDbg   ={"VDDbg"  , &RegisterBgCnt,  0, 5, 0x0};
  // 53 - AMCnt
  AMACv2Field AMintCalib ={"AMintCalib", &RegisterAMCnt, 24, 4, 0x0};
  AMACv2Field Ch13Mux    ={"Ch13Mux"   , &RegisterAMCnt, 20, 2, 0x0};
  AMACv2Field Ch12Mux    ={"Ch12Mux"   , &RegisterAMCnt, 16, 2, 0x0};
  AMACv2Field Ch5Mux     ={"Ch5Mux"    , &RegisterAMCnt, 12, 2, 0x0};
  AMACv2Field Ch4Mux     ={"Ch4Mux"    , &RegisterAMCnt,  8, 2, 0x0};
  AMACv2Field Ch3Mux     ={"Ch3Mux"    , &RegisterAMCnt,  4, 2, 0x0};
  // 54 - DACs0
  AMACv2Field DACShunty ={"DACShunty", &RegisterDACs0, 24, 8, 0x0};
  AMACv2Field DACShuntx ={"DACShuntx", &RegisterDACs0, 16, 8, 0x0};
  AMACv2Field DACCaly   ={"DACCaly"  , &RegisterDACs0,  8, 8, 0x0};
  AMACv2Field DACCalx   ={"DACCalx"  , &RegisterDACs0,  0, 8, 0x0};
  // 55 - DACbias
  AMACv2Field DACbias ={"DACbias", &RegisterDACbias, 0, 5, 0xD};
  // 56 - AMACCnt
  AMACv2Field HVcurGain ={"HVcurGain", &RegisterAMACCnt, 16, 4, 0x0};
  AMACv2Field DRcomMode ={"DRcomMode", &RegisterAMACCnt, 12, 2, 0x0};
  AMACv2Field DRcurr ={"DRcurr", &RegisterAMACCnt, 8, 3, 0x4};
  AMACv2Field RingOscFrq ={"RingOscFrq", &RegisterAMACCnt, 0, 3, 0x4};
  // 57 - NTCRange
  AMACv2Field CTAToffset ={"CTAToffset", &RegisterNTCRange, 20, 4, 0x0};
  AMACv2Field NTCpbCal ={"NTCpbCal", &RegisterNTCRange, 19, 1, 0x1};
  AMACv2Field NTCpbSenseRange ={"NTCpbSenseRange", &RegisterNTCRange, 16, 3, 0x4};
  AMACv2Field NTCy0Cal ={"NTCy0Cal", &RegisterNTCRange, 11, 1, 0x1};
  AMACv2Field NTCy0SenseRange ={"NTCy0SenseRange", &RegisterNTCRange, 8, 3, 0x4};
  AMACv2Field NTCx0Cal ={"NTCx0Cal", &RegisterNTCRange, 3, 1, 0x1};
  AMACv2Field NTCx0SenseRange ={"NTCx0SenseRange", &RegisterNTCRange, 0, 3, 0x4};
  // 58 - LVCurCal
  AMACv2Field DCDCoOffset ={"DCDCoOffset", &RegisterLVCurCal, 20, 4, 0x4};
  AMACv2Field DCDCoZeroReading ={"DCDCoZeroReading", &RegisterLVCurCal, 19, 1, 0x0};
  AMACv2Field DCDCoN ={"DCDCoN", &RegisterLVCurCal, 17, 1, 0x0};
  AMACv2Field DCDCoP ={"DCDCoP", &RegisterLVCurCal, 16, 1, 0x0};
  AMACv2Field DCDCiZeroReading ={"DCDCiZeroReading", &RegisterLVCurCal, 15, 1, 0x0};
  AMACv2Field DCDCiRangeSW ={"DCDCiRangeSW", &RegisterLVCurCal, 12, 1, 0x1};
  AMACv2Field DCDCiOffset ={"DCDCiOffset", &RegisterLVCurCal, 8, 4, 0x8};
  AMACv2Field DCDCiP ={"DCDCiP", &RegisterLVCurCal, 4, 3, 0x0};
  AMACv2Field DCDCiN ={"DCDCiN", &RegisterLVCurCal, 0, 3, 0x0};
  // 60 - HxICfg
  AMACv2Field HxFlagValid  ={"HxFlagValid"  , &RegisterHxICfg,  0, 2, 0x0};
  AMACv2Field HxFlagValidEn={"HxFlagValidEn", &RegisterHxICfg,  4, 1, 0x0};
  AMACv2Field HxFlagsLogic ={"HxFlagsLogic" , &RegisterHxICfg,  8, 1, 0x0};
  AMACv2Field HxFlagsLatch ={"HxFlagsLatch" , &RegisterHxICfg, 12, 1, 0x1};
  AMACv2Field HxLAM        ={"HxLAM"        , &RegisterHxICfg, 16, 1, 0x0};
  // 61 - HyICfg
  AMACv2Field HyFlagValid  ={"HyFlagValid"  , &RegisterHyICfg,  0, 2, 0x0};
  AMACv2Field HyFlagValidEn={"HyFlagValidEn", &RegisterHyICfg,  4, 1, 0x0};
  AMACv2Field HyFlagsLogic ={"HyFlagsLogic" , &RegisterHyICfg,  8, 1, 0x0};
  AMACv2Field HyFlagsLatch ={"HyFlagsLatch" , &RegisterHyICfg, 12, 1, 0x1};
  AMACv2Field HyLAM        ={"HyLAM"        , &RegisterHyICfg, 16, 1, 0x0};
  // 62 - HV0ICfg
  AMACv2Field HV0FlagValid  ={"HV0FlagValid"  , &RegisterHV0ICfg,  0, 2, 0x0};
  AMACv2Field HV0FlagValidEn={"HV0FlagValidEn", &RegisterHV0ICfg,  4, 1, 0x0};
  AMACv2Field HV0FlagsLogic ={"HV0FlagsLogic" , &RegisterHV0ICfg,  8, 1, 0x0};
  AMACv2Field HV0FlagsLatch ={"HV0FlagsLatch" , &RegisterHV0ICfg, 12, 1, 0x1};
  AMACv2Field HV0LAM        ={"HV0LAM"        , &RegisterHV0ICfg, 16, 1, 0x0};
  // 63 - HV2ICfg
  AMACv2Field HV2FlagValid  ={"HV2FlagValid"  , &RegisterHV2ICfg,  0, 2, 0x0};
  AMACv2Field HV2FlagValidEn={"HV2FlagValidEn", &RegisterHV2ICfg,  4, 1, 0x0};
  AMACv2Field HV2FlagsLogic ={"HV2FlagsLogic" , &RegisterHV2ICfg,  8, 1, 0x0};
  AMACv2Field HV2FlagsLatch ={"HV2FlagsLatch" , &RegisterHV2ICfg, 12, 1, 0x1};
  AMACv2Field HV2LAM        ={"HV2LAM"        , &RegisterHV2ICfg, 16, 1, 0x0};
  // 64 - DCDCICfg
  AMACv2Field DCDCFlagValid  ={"DCDCFlagValid"  , &RegisterDCDCICfg,  0, 2, 0x0};
  AMACv2Field DCDCFlagValidEn={"DCDCFlagValidEn", &RegisterDCDCICfg,  4, 1, 0x0};
  AMACv2Field DCDCFlagsLogic ={"DCDCFlagsLogic" , &RegisterDCDCICfg,  8, 1, 0x0};
  AMACv2Field DCDCFlagsLatch ={"DCDCFlagsLatch" , &RegisterDCDCICfg, 12, 1, 0x1};
  AMACv2Field DCDCLAM        ={"DCDCLAM"        , &RegisterDCDCICfg, 16, 1, 0x0};
  // 65 - WRNICfg
  AMACv2Field WRNFlagValid  ={"WRNFlagValid"  , &RegisterWRNICfg,  0, 2, 0x0};
  AMACv2Field WRNFlagValidEn={"WRNFlagValidEn", &RegisterWRNICfg,  4, 1, 0x0};
  AMACv2Field WRNFlagsLogic ={"WRNFlagsLogic" , &RegisterWRNICfg,  8, 1, 0x0};
  AMACv2Field WRNFlagsLatch ={"WRNFlagsLatch" , &RegisterWRNICfg, 12, 1, 0x1};
  AMACv2Field WRNLAM        ={"WRNLAM"        , &RegisterWRNICfg, 16, 1, 0x0};
  // 70 - HxTLUT
  AMACv2Field HxTlut ={"HxTlut", &RegisterHxTLUT, 0, 8, 0x0};
  // 71 - HxModLUT1
  AMACv2Field HxModlut1 ={"HxModlut1", &RegisterHxModLUT1, 0, 32, 0x0};
  // 72 - HxModLUT2
  AMACv2Field HxModlut2 ={"HxModlut2", &RegisterHxModLUT2, 0, 32, 0x0};
  // 73 - HyTLUT
  AMACv2Field HyTlut ={"HyTlut", &RegisterHyTLUT, 0, 8, 0x0};
  // 74 - HyModLUT1
  AMACv2Field HyModlut1 ={"HyModlut1", &RegisterHyModLUT1, 0, 32, 0x0};
  // 75 - HyModLUT2
  AMACv2Field HyModlut2 ={"HyModlut2", &RegisterHyModLUT2, 0, 32, 0x0};
  // 76 - HV0TLUT
  AMACv2Field HV0Tlut ={"HV0Tlut", &RegisterHV0TLUT, 0, 8, 0x0};
  // 77 - HV0ModLUT1
  AMACv2Field HV0Modlut1 ={"HV0Modlut1", &RegisterHV0ModLUT1, 0, 32, 0x0};
  // 78 - HV0ModLUT2
  AMACv2Field HV0Modlut2 ={"HV0Modlut2", &RegisterHV0ModLUT2, 0, 32, 0x0};
  // 79 - HV2TLUT
  AMACv2Field HV2Tlut ={"HV2Tlut", &RegisterHV2TLUT, 0, 8, 0x0};
  // 80 - HV2ModLUT1
  AMACv2Field HV2Modlut1 ={"HV2Modlut1", &RegisterHV2ModLUT1, 0, 32, 0x0};
  // 81 - HV2ModLUT2
  AMACv2Field HV2Modlut2 ={"HV2Modlut2", &RegisterHV2ModLUT2, 0, 32, 0x0};
  // 82 - DCDCTLUT
  AMACv2Field DCDCTlut ={"DCDCTlut", &RegisterDCDCTLUT, 0, 8, 0x0};
  // 83 - DCDCModLUT1
  AMACv2Field DCDCModlut1 ={"DCDCModlut1", &RegisterDCDCModLUT1, 0, 32, 0x0};
  // 84 - DCDCModLUT2
  AMACv2Field DCDCModlut2 ={"DCDCModlut2", &RegisterDCDCModLUT2, 0, 32, 0x0};
  // 85 - WRNTLUT
  AMACv2Field WRNTlut ={"WRNTlut", &RegisterWRNTLUT, 0, 8, 0x0};
  // 86 - WRNModLUT1
  AMACv2Field WRNModlut1 ={"WRNModlut1", &RegisterWRNModLUT1, 0, 32, 0x0};
  // 87 - WRNModLUT2
  AMACv2Field WRNModlut2 ={"WRNModlut2", &RegisterWRNModLUT2, 0, 32, 0x0};
  // 90 - HxFlagEn
  AMACv2Field HxFlagsEnHi ={"HxFlagsEnHi", &RegisterHxFlagEn, 16, 16, 0x0};
  AMACv2Field HxFlagsEnLo ={"HxFlagsEnLo", &RegisterHxFlagEn, 16, 16, 0x0};
  // 91 - HyFlagEn
  AMACv2Field HyFlagsEnHi ={"HyFlagsEnHi", &RegisterHyFlagEn, 16, 16, 0x0};
  AMACv2Field HyFlagsEnLo ={"HyFlagsEnLo", &RegisterHyFlagEn, 16, 16, 0x0};
  // 92 - HV0FlagEn
  AMACv2Field HV0FlagsEnHi ={"HV0FlagsEnHi", &RegisterHV0FlagEn, 16, 16, 0x0};
  AMACv2Field HV0FlagsEnLo ={"HV0FlagsEnLo", &RegisterHV0FlagEn, 16, 16, 0x0};
  // 93 - HV2FlagEn
  AMACv2Field HV2FlagsEnHi ={"HV2FlagsEnHi", &RegisterHV2FlagEn, 16, 16, 0x0};
  AMACv2Field HV2FlagsEnLo ={"HV2FlagsEnLo", &RegisterHV2FlagEn, 16, 16, 0x0};
  // 94 - DCDCFlagEn
  AMACv2Field DCDCFlagsEnHi ={"DCDCFlagsEnHi", &RegisterDCDCFlagEn, 16, 16, 0x0};
  AMACv2Field DCDCFlagsEnLo ={"DCDCFlagsEnLo", &RegisterDCDCFlagEn, 16, 16, 0x0};
  // 95 - SynFlagEn
  AMACv2Field WRNFlagsEnHi ={"WRNFlagsEnHi", &RegisterSynFlagEn, 16, 16, 0x0};
  AMACv2Field WRNFlagsEnLo ={"WRNFlagsEnLo", &RegisterSynFlagEn, 16, 16, 0x0};
  AMACv2Field WRNsynFlagEnHi ={"WRNsynFlagEnHi", &RegisterSynFlagEn, 21, 1, 0x0};
  AMACv2Field WRNsynFlagEnLo ={"WRNsynFlagEnLo", &RegisterSynFlagEn, 20, 1, 0x0};
  AMACv2Field DCDCsynFlagEnHi ={"DCDCsynFlagEnHi", &RegisterSynFlagEn, 17, 1, 0x0};
  AMACv2Field DCDCsynFlagEnLo ={"DCDCsynFlagEnLo", &RegisterSynFlagEn, 16, 1, 0x0};
  AMACv2Field HV2synFlagEnHi ={"HV2synFlagEnHi", &RegisterSynFlagEn, 13, 1, 0x0};
  AMACv2Field HV2synFlagEnLo ={"HV2synFlagEnLo", &RegisterSynFlagEn, 12, 1, 0x0};
  AMACv2Field HV0synFlagEnHi ={"HV0synFlagEnHi", &RegisterSynFlagEn, 9, 1, 0x0};
  AMACv2Field HV0synFlagEnLo ={"HV0synFlagEnLo", &RegisterSynFlagEn, 8, 1, 0x0};
  AMACv2Field HysynFlagEnHi ={"HysynFlagEnHi", &RegisterSynFlagEn, 5, 1, 0x0};
  AMACv2Field HysynFlagEnLo ={"HysynFlagEnLo", &RegisterSynFlagEn, 4, 1, 0x0};
  AMACv2Field HxsynFlagEnHi ={"HxsynFlagEnHi", &RegisterSynFlagEn, 1, 1, 0x0};
  AMACv2Field HxsynFlagEnLo ={"HxsynFlagEnLo", &RegisterSynFlagEn, 0, 1, 0x0};
  // 100 - HxLoTh0
  AMACv2Field HxLoThCh0  ={"HxLoThCh0", &RegisterHxLoTh0,  0, 10, 0x0};
  AMACv2Field HxLoThCh1  ={"HxLoThCh1", &RegisterHxLoTh0, 10, 10, 0x0};
  AMACv2Field HxLoThCh2  ={"HxLoThCh2", &RegisterHxLoTh0, 20, 10, 0x0};
  // 101 - HxLoTh1
  AMACv2Field HxLoThCh3  ={"HxLoThCh3", &RegisterHxLoTh1,  0, 10, 0x0};
  AMACv2Field HxLoThCh4  ={"HxLoThCh4", &RegisterHxLoTh1, 10, 10, 0x0};
  AMACv2Field HxLoThCh5  ={"HxLoThCh5", &RegisterHxLoTh1, 20, 10, 0x0};
  // 102 - HxLoTh2
  AMACv2Field HxLoThCh6  ={"HxLoThCh6", &RegisterHxLoTh2,  0, 10, 0x0};
  AMACv2Field HxLoThCh7  ={"HxLoThCh7", &RegisterHxLoTh2, 10, 10, 0x0};
  AMACv2Field HxLoThCh8  ={"HxLoThCh8", &RegisterHxLoTh2, 20, 10, 0x0};
  // 103 - HxLoTh3
  AMACv2Field HxLoThCh9  ={"HxLoThCh9" , &RegisterHxLoTh3,  0, 10, 0x0};
  AMACv2Field HxLoThCh10 ={"HxLoThCh10", &RegisterHxLoTh3, 10, 10, 0x0};
  AMACv2Field HxLoThCh11 ={"HxLoThCh11", &RegisterHxLoTh3, 20, 10, 0x0};
  // 104 - HxLoTh4
  AMACv2Field HxLoThCh12 ={"HxLoThCh12", &RegisterHxLoTh4,  0, 10, 0x0};
  AMACv2Field HxLoThCh13 ={"HxLoThCh13", &RegisterHxLoTh4, 10, 10, 0x0};
  AMACv2Field HxLoThCh14 ={"HxLoThCh14", &RegisterHxLoTh4, 20, 10, 0x0};
  // 105 - HxLoTh5
  AMACv2Field HxLoThCh15 ={"HxLoThCh15", &RegisterHxLoTh5,  0, 10, 0x0};
  // 106 - HxHiTh0
  AMACv2Field HxHiThCh0  ={"HxHiThCh0" , &RegisterHxHiTh0,  0, 10, 0x3FF};
  AMACv2Field HxHiThCh1  ={"HxHiThCh1" , &RegisterHxHiTh0, 10, 10, 0x3FF};
  AMACv2Field HxHiThCh2  ={"HxHiThCh2" , &RegisterHxHiTh0, 20, 10, 0x3FF};
  // 107 - HxHiTh1
  AMACv2Field HxHiThCh3  ={"HxHiThCh3" , &RegisterHxHiTh1,  0, 10, 0x3FF};
  AMACv2Field HxHiThCh4  ={"HxHiThCh4" , &RegisterHxHiTh1, 10, 10, 0x3FF};
  AMACv2Field HxHiThCh5  ={"HxHiThCh5" , &RegisterHxHiTh1, 20, 10, 0x3FF};
  // 108 - HxHiTh2
  AMACv2Field HxHiThCh6  ={"HxHiThCh6" , &RegisterHxHiTh2,  0, 10, 0x3FF};
  AMACv2Field HxHiThCh7  ={"HxHiThCh7" , &RegisterHxHiTh2, 10, 10, 0x3FF};
  AMACv2Field HxHiThCh8  ={"HxHiThCh8" , &RegisterHxHiTh2, 20, 10, 0x3FF};
  // 109 - HxHiTh3
  AMACv2Field HxHiThCh9  ={"HxHiThCh9" , &RegisterHxHiTh3,  0, 10, 0x3FF};
  AMACv2Field HxHiThCh10 ={"HxHiThCh10", &RegisterHxHiTh3, 10, 10, 0x3FF};
  AMACv2Field HxHiThCh11 ={"HxHiThCh11", &RegisterHxHiTh3, 20, 10, 0x3FF};
  // 110 - HxHiTh4
  AMACv2Field HxHiThCh12 ={"HxHiThCh12", &RegisterHxHiTh4,  0, 10, 0x3FF};
  AMACv2Field HxHiThCh13 ={"HxHiThCh13", &RegisterHxHiTh4, 10, 10, 0x3FF};
  AMACv2Field HxHiThCh14 ={"HxHiThCh14", &RegisterHxHiTh4, 20, 10, 0x3FF};
  // 111 - HxHiTh5
  AMACv2Field HxHiThCh15 ={"HxHiThCh15", &RegisterHxHiTh5,  0, 10, 0x3FF};
  // 112 - HyLoTh0
  AMACv2Field HyLoThCh0 ={"HyLoThCh0", &RegisterHyLoTh0, 0, 10, 0x0};
  AMACv2Field HyLoThCh1 ={"HyLoThCh1", &RegisterHyLoTh0, 10, 10, 0x0};
  AMACv2Field HyLoThCh2 ={"HyLoThCh2", &RegisterHyLoTh0, 20, 10, 0x0};
  // 113 - HyLoTh1
  AMACv2Field HyLoThCh3 ={"HyLoThCh3", &RegisterHyLoTh1, 0, 10, 0x0};
  AMACv2Field HyLoThCh4 ={"HyLoThCh4", &RegisterHyLoTh1, 10, 10, 0x0};
  AMACv2Field HyLoThCh5 ={"HyLoThCh5", &RegisterHyLoTh1, 20, 10, 0x0};
  // 114 - HyLoTh2
  AMACv2Field HyLoThCh6 ={"HyLoThCh6", &RegisterHyLoTh2, 0, 10, 0x0};
  AMACv2Field HyLoThCh7 ={"HyLoThCh7", &RegisterHyLoTh2, 10, 10, 0x0};
  AMACv2Field HyLoThCh8 ={"HyLoThCh8", &RegisterHyLoTh2, 20, 10, 0x0};
  // 115 - HyLoTh3
  AMACv2Field HyLoThCh9 ={"HyLoThCh9", &RegisterHyLoTh3, 0, 10, 0x0};
  AMACv2Field HyLoThCh10 ={"HyLoThCh10", &RegisterHyLoTh3, 10, 10, 0x0};
  AMACv2Field HyLoThCh11 ={"HyLoThCh11", &RegisterHyLoTh3, 20, 10, 0x0};
  // 116 - HyLoTh4
  AMACv2Field HyLoThCh12 ={"HyLoThCh12", &RegisterHyLoTh4, 0, 10, 0x0};
  AMACv2Field HyLoThCh13 ={"HyLoThCh13", &RegisterHyLoTh4, 10, 10, 0x0};
  AMACv2Field HyLoThCh14 ={"HyLoThCh14", &RegisterHyLoTh4, 20, 10, 0x0};
  // 117 - HyLoTh5
  AMACv2Field HyLoThCh15 ={"HyLoThCh15", &RegisterHyLoTh5, 0, 10, 0x0};
  // 118 - HyHiTh0
  AMACv2Field HyHiThCh0 ={"HyHiThCh0", &RegisterHyHiTh0, 0, 10, 0x3FF};
  AMACv2Field HyHiThCh1 ={"HyHiThCh1", &RegisterHyHiTh0, 10, 10, 0x3FF};
  AMACv2Field HyHiThCh2 ={"HyHiThCh2", &RegisterHyHiTh0, 20, 10, 0x3FF};
  // 119 - HyHiTh1
  AMACv2Field HyHiThCh3 ={"HyHiThCh3", &RegisterHyHiTh1, 0, 10, 0x3FF};
  AMACv2Field HyHiThCh4 ={"HyHiThCh4", &RegisterHyHiTh1, 10, 10, 0x3FF};
  AMACv2Field HyHiThCh5 ={"HyHiThCh5", &RegisterHyHiTh1, 20, 10, 0x3FF};
  // 120 - HyHiTh2
  AMACv2Field HyHiThCh6 ={"HyHiThCh6", &RegisterHyHiTh2, 0, 10, 0x3FF};
  AMACv2Field HyHiThCh7 ={"HyHiThCh7", &RegisterHyHiTh2, 10, 10, 0x3FF};
  AMACv2Field HyHiThCh8 ={"HyHiThCh8", &RegisterHyHiTh2, 20, 10, 0x3FF};
  // 121 - HyHiTh3
  AMACv2Field HyHiThCh9 ={"HyHiThCh9", &RegisterHyHiTh3, 0, 10, 0x3FF};
  AMACv2Field HyHiThCh10 ={"HyHiThCh10", &RegisterHyHiTh3, 10, 10, 0x3FF};
  AMACv2Field HyHiThCh11 ={"HyHiThCh11", &RegisterHyHiTh3, 20, 10, 0x3FF};
  // 122 - HyHiTh4
  AMACv2Field HyHiThCh12 ={"HyHiThCh12", &RegisterHyHiTh4, 0, 10, 0x3FF};
  AMACv2Field HyHiThCh13 ={"HyHiThCh13", &RegisterHyHiTh4, 10, 10, 0x3FF};
  AMACv2Field HyHiThCh14 ={"HyHiThCh14", &RegisterHyHiTh4, 20, 10, 0x3FF};
  // 123 - HyHiTh5
  AMACv2Field HyHiThCh15 ={"HyHiThCh15", &RegisterHyHiTh5, 0, 10, 0x3FF};
  // 124 - HV0LoTh0
  AMACv2Field HV0LoThCh0 ={"HV0LoThCh0", &RegisterHV0LoTh0, 0, 10, 0x0};
  AMACv2Field HV0LoThCh1 ={"HV0LoThCh1", &RegisterHV0LoTh0, 10, 10, 0x0};
  AMACv2Field HV0LoThCh2 ={"HV0LoThCh2", &RegisterHV0LoTh0, 20, 10, 0x0};
  // 125 - HV0LoTh1
  AMACv2Field HV0LoThCh3 ={"HV0LoThCh3", &RegisterHV0LoTh1, 0, 10, 0x0};
  AMACv2Field HV0LoThCh4 ={"HV0LoThCh4", &RegisterHV0LoTh1, 10, 10, 0x0};
  AMACv2Field HV0LoThCh5 ={"HV0LoThCh5", &RegisterHV0LoTh1, 20, 10, 0x0};
  // 126 - HV0LoTh2
  AMACv2Field HV0LoThCh6 ={"HV0LoThCh6", &RegisterHV0LoTh2, 0, 10, 0x0};
  AMACv2Field HV0LoThCh7 ={"HV0LoThCh7", &RegisterHV0LoTh2, 10, 10, 0x0};
  AMACv2Field HV0LoThCh8 ={"HV0LoThCh8", &RegisterHV0LoTh2, 20, 10, 0x0};
  // 127 - HV0LoTh3
  AMACv2Field HV0LoThCh9 ={"HV0LoThCh9", &RegisterHV0LoTh3, 0, 10, 0x0};
  AMACv2Field HV0LoThCh10 ={"HV0LoThCh10", &RegisterHV0LoTh3, 10, 10, 0x0};
  AMACv2Field HV0LoThCh11 ={"HV0LoThCh11", &RegisterHV0LoTh3, 20, 10, 0x0};
  // 128 - HV0LoTh4
  AMACv2Field HV0LoThCh12 ={"HV0LoThCh12", &RegisterHV0LoTh4, 0, 10, 0x0};
  AMACv2Field HV0LoThCh13 ={"HV0LoThCh13", &RegisterHV0LoTh4, 10, 10, 0x0};
  AMACv2Field HV0LoThCh14 ={"HV0LoThCh14", &RegisterHV0LoTh4, 20, 10, 0x0};
  // 129 - HV0LoTh5
  AMACv2Field HV0LoThCh15 ={"HV0LoThCh15", &RegisterHV0LoTh5, 0, 10, 0x0};
  // 130 - HV0HiTh0
  AMACv2Field HV0HiThCh0 ={"HV0HiThCh0", &RegisterHV0HiTh0, 0, 10, 0x3FF};
  AMACv2Field HV0HiThCh1 ={"HV0HiThCh1", &RegisterHV0HiTh0, 10, 10, 0x3FF};
  AMACv2Field HV0HiThCh2 ={"HV0HiThCh2", &RegisterHV0HiTh0, 20, 10, 0x3FF};
  // 131 - HV0HiTh1
  AMACv2Field HV0HiThCh3 ={"HV0HiThCh3", &RegisterHV0HiTh1, 0, 10, 0x3FF};
  AMACv2Field HV0HiThCh4 ={"HV0HiThCh4", &RegisterHV0HiTh1, 10, 10, 0x3FF};
  AMACv2Field HV0HiThCh5 ={"HV0HiThCh5", &RegisterHV0HiTh1, 20, 10, 0x3FF};
  // 132 - HV0HiTh2
  AMACv2Field HV0HiThCh6 ={"HV0HiThCh6", &RegisterHV0HiTh2, 0, 10, 0x3FF};
  AMACv2Field HV0HiThCh7 ={"HV0HiThCh7", &RegisterHV0HiTh2, 10, 10, 0x3FF};
  AMACv2Field HV0HiThCh8 ={"HV0HiThCh8", &RegisterHV0HiTh2, 20, 10, 0x3FF};
  // 133 - HV0HiTh3
  AMACv2Field HV0HiThCh9 ={"HV0HiThCh9", &RegisterHV0HiTh3, 0, 10, 0x3FF};
  AMACv2Field HV0HiThCh10 ={"HV0HiThCh10", &RegisterHV0HiTh3, 10, 10, 0x3FF};
  AMACv2Field HV0HiThCh11 ={"HV0HiThCh11", &RegisterHV0HiTh3, 20, 10, 0x3FF};
  // 134 - HV0HiTh4
  AMACv2Field HV0HiThCh12 ={"HV0HiThCh12", &RegisterHV0HiTh4, 0, 10, 0x3FF};
  AMACv2Field HV0HiThCh13 ={"HV0HiThCh13", &RegisterHV0HiTh4, 10, 10, 0x3FF};
  AMACv2Field HV0HiThCh14 ={"HV0HiThCh14", &RegisterHV0HiTh4, 20, 10, 0x3FF};
  // 135 - HV0HiTh5
  AMACv2Field HV0HiThCh15 ={"HV0HiThCh15", &RegisterHV0HiTh5, 0, 10, 0x3FF};
  // 136 - HV2HiTh0
  AMACv2Field HV2LoThCh0 ={"HV2LoThCh0", &RegisterHV2LoTh0, 0, 10, 0x0};
  AMACv2Field HV2LoThCh1 ={"HV2LoThCh1", &RegisterHV2LoTh0, 10, 10, 0x0};
  AMACv2Field HV2LoThCh2 ={"HV2LoThCh2", &RegisterHV2LoTh0, 20, 10, 0x0};
  // 137 - HV2LoTh1
  AMACv2Field HV2LoThCh3 ={"HV2LoThCh3", &RegisterHV2LoTh1, 0, 10, 0x0};
  AMACv2Field HV2LoThCh4 ={"HV2LoThCh4", &RegisterHV2LoTh1, 10, 10, 0x0};
  AMACv2Field HV2LoThCh5 ={"HV2LoThCh5", &RegisterHV2LoTh1, 20, 10, 0x0};
  // 138 - HV2LoTh2
  AMACv2Field HV2LoThCh6 ={"HV2LoThCh6", &RegisterHV2LoTh2, 0, 10, 0x0};
  AMACv2Field HV2LoThCh7 ={"HV2LoThCh7", &RegisterHV2LoTh2, 10, 10, 0x0};
  AMACv2Field HV2LoThCh8 ={"HV2LoThCh8", &RegisterHV2LoTh2, 20, 10, 0x0};
  // 139 - HV2LoTh3
  AMACv2Field HV2LoThCh9 ={"HV2LoThCh9", &RegisterHV2LoTh3, 0, 10, 0x0};
  AMACv2Field HV2LoThCh10 ={"HV2LoThCh10", &RegisterHV2LoTh3, 10, 10, 0x0};
  AMACv2Field HV2LoThCh11 ={"HV2LoThCh11", &RegisterHV2LoTh3, 20, 10, 0x0};
  // 140 - HV2LoTh4
  AMACv2Field HV2LoThCh12 ={"HV2LoThCh12", &RegisterHV2LoTh4, 0, 10, 0x0};
  AMACv2Field HV2LoThCh13 ={"HV2LoThCh13", &RegisterHV2LoTh4, 10, 10, 0x0};
  AMACv2Field HV2LoThCh14 ={"HV2LoThCh14", &RegisterHV2LoTh4, 20, 10, 0x0};
  // 141 - HV2LoTh5
  AMACv2Field HV2LoThCh15 ={"HV2LoThCh15", &RegisterHV2LoTh5, 0, 10, 0x0};
  // 142 - HV2HiTh0
  AMACv2Field HV2HiThCh0 ={"HV2HiThCh0", &RegisterHV2HiTh0, 0, 10, 0x3FF};
  AMACv2Field HV2HiThCh1 ={"HV2HiThCh1", &RegisterHV2HiTh0, 10, 10, 0x3FF};
  AMACv2Field HV2HiThCh2 ={"HV2HiThCh2", &RegisterHV2HiTh0, 20, 10, 0x3FF};
  // 143 - HV2HiTh1
  AMACv2Field HV2HiThCh3 ={"HV2HiThCh3", &RegisterHV2HiTh1, 0, 10, 0x3FF};
  AMACv2Field HV2HiThCh4 ={"HV2HiThCh4", &RegisterHV2HiTh1, 10, 10, 0x3FF};
  AMACv2Field HV2HiThCh5 ={"HV2HiThCh5", &RegisterHV2HiTh1, 20, 10, 0x3FF};
  // 144 - HV2HiTh2
  AMACv2Field HV2HiThCh6 ={"HV2HiThCh6", &RegisterHV2HiTh2, 0, 10, 0x3FF};
  AMACv2Field HV2HiThCh7 ={"HV2HiThCh7", &RegisterHV2HiTh2, 10, 10, 0x3FF};
  AMACv2Field HV2HiThCh8 ={"HV2HiThCh8", &RegisterHV2HiTh2, 20, 10, 0x3FF};
  // 145 - HV2HiTh3
  AMACv2Field HV2HiThCh9 ={"HV2HiThCh9", &RegisterHV2HiTh3, 0, 10, 0x3FF};
  AMACv2Field HV2HiThCh10 ={"HV2HiThCh10", &RegisterHV2HiTh3, 10, 10, 0x3FF};
  AMACv2Field HV2HiThCh11 ={"HV2HiThCh11", &RegisterHV2HiTh3, 20, 10, 0x3FF};
  // 146 - HV2HiTh4
  AMACv2Field HV2HiThCh12 ={"HV2HiThCh12", &RegisterHV2HiTh4, 0, 10, 0x3FF};
  AMACv2Field HV2HiThCh13 ={"HV2HiThCh13", &RegisterHV2HiTh4, 10, 10, 0x3FF};
  AMACv2Field HV2HiThCh14 ={"HV2HiThCh14", &RegisterHV2HiTh4, 20, 10, 0x3FF};
  // 147 - HV2HiTh5
  AMACv2Field HV2HiThCh15 ={"HV2HiThCh15", &RegisterHV2HiTh5, 0, 10, 0x3FF};
  // 148 - DCDCLoTh0
  AMACv2Field DCDCLoThCh0 ={"DCDCLoThCh0", &RegisterDCDCLoTh0, 0, 10, 0x0};
  AMACv2Field DCDCLoThCh1 ={"DCDCLoThCh1", &RegisterDCDCLoTh0, 10, 10, 0x0};
  AMACv2Field DCDCLoThCh2 ={"DCDCLoThCh2", &RegisterDCDCLoTh0, 20, 10, 0x0};
  // 149 - DCDCLoTh1
  AMACv2Field DCDCLoThCh3 ={"DCDCLoThCh3", &RegisterDCDCLoTh1, 0, 10, 0x0};
  AMACv2Field DCDCLoThCh4 ={"DCDCLoThCh4", &RegisterDCDCLoTh1, 10, 10, 0x0};
  AMACv2Field DCDCLoThCh5 ={"DCDCLoThCh5", &RegisterDCDCLoTh1, 20, 10, 0x0};
  // 150 - DCDCLoTh2
  AMACv2Field DCDCLoThCh6 ={"DCDCLoThCh6", &RegisterDCDCLoTh2, 0, 10, 0x0};
  AMACv2Field DCDCLoThCh7 ={"DCDCLoThCh7", &RegisterDCDCLoTh2, 10, 10, 0x0};
  AMACv2Field DCDCLoThCh8 ={"DCDCLoThCh8", &RegisterDCDCLoTh2, 20, 10, 0x0};
  // 151 - DCDCLoTh3
  AMACv2Field DCDCLoThCh9 ={"DCDCLoThCh9", &RegisterDCDCLoTh3, 0, 10, 0x0};
  AMACv2Field DCDCLoThCh10 ={"DCDCLoThCh10", &RegisterDCDCLoTh3, 10, 10, 0x0};
  AMACv2Field DCDCLoThCh11 ={"DCDCLoThCh11", &RegisterDCDCLoTh3, 20, 10, 0x0};
  // 152 - DCDCLoTh4
  AMACv2Field DCDCLoThCh12 ={"DCDCLoThCh12", &RegisterDCDCLoTh4, 0, 10, 0x0};
  AMACv2Field DCDCLoThCh13 ={"DCDCLoThCh13", &RegisterDCDCLoTh4, 10, 10, 0x0};
  AMACv2Field DCDCLoThCh14 ={"DCDCLoThCh14", &RegisterDCDCLoTh4, 20, 10, 0x0};
  // 153 - DCDCLoTh5
  AMACv2Field DCDCLoThCh15 ={"DCDCLoThCh15", &RegisterDCDCLoTh5, 0, 10, 0x0};
  // 154 - DCDCHiTh0
  AMACv2Field DCDCHiThCh0  ={"DCDCHiThCh0" , &RegisterDCDCHiTh0,  0, 10, 0x3FF};
  AMACv2Field DCDCHiThCh1  ={"DCDCHiThCh1" , &RegisterDCDCHiTh0, 10, 10, 0x3FF};
  AMACv2Field DCDCHiThCh2  ={"DCDCHiThCh2" , &RegisterDCDCHiTh0, 20, 10, 0x3FF};
  // 155 - DCDCHiTh1
  AMACv2Field DCDCHiThCh3  ={"DCDCHiThCh3" , &RegisterDCDCHiTh1,  0, 10, 0x3FF};
  AMACv2Field DCDCHiThCh4  ={"DCDCHiThCh4" , &RegisterDCDCHiTh1, 10, 10, 0x3FF};
  AMACv2Field DCDCHiThCh5  ={"DCDCHiThCh5" , &RegisterDCDCHiTh1, 20, 10, 0x3FF};
  // 156 - DCDCHiTh2
  AMACv2Field DCDCHiThCh6  ={"DCDCHiThCh6" , &RegisterDCDCHiTh2,  0, 10, 0x3FF};
  AMACv2Field DCDCHiThCh7  ={"DCDCHiThCh7" , &RegisterDCDCHiTh2, 10, 10, 0x3FF};
  AMACv2Field DCDCHiThCh8  ={"DCDCHiThCh8" , &RegisterDCDCHiTh2, 20, 10, 0x3FF};
  // 157 - DCDCHiTh3
  AMACv2Field DCDCHiThCh9  ={"DCDCHiThCh9" , &RegisterDCDCHiTh3,  0, 10, 0x3FF};
  AMACv2Field DCDCHiThCh10 ={"DCDCHiThCh10", &RegisterDCDCHiTh3, 10, 10, 0x3FF};
  AMACv2Field DCDCHiThCh11 ={"DCDCHiThCh11", &RegisterDCDCHiTh3, 20, 10, 0x3FF};
  // 158 - DCDCHiTh4
  AMACv2Field DCDCHiThCh12 ={"DCDCHiThCh12", &RegisterDCDCHiTh4,  0, 10, 0x3FF};
  AMACv2Field DCDCHiThCh13 ={"DCDCHiThCh13", &RegisterDCDCHiTh4, 10, 10, 0x3FF};
  AMACv2Field DCDCHiThCh14 ={"DCDCHiThCh14", &RegisterDCDCHiTh4, 20, 10, 0x3FF};
  // 159 - DCDCHiTh5
  AMACv2Field DCDCHiThCh15 ={"DCDCHiThCh15", &RegisterDCDCHiTh5,  0, 10, 0x3FF};
  // 160 - WRNLoTh0
  AMACv2Field WRNLoThCh0  ={"WRNLoThCh0" , &RegisterWRNLoTh0,  0, 10, 0x0};
  AMACv2Field WRNLoThCh1  ={"WRNLoThCh1" , &RegisterWRNLoTh0, 10, 10, 0x0};
  AMACv2Field WRNLoThCh2  ={"WRNLoThCh2" , &RegisterWRNLoTh0, 20, 10, 0x0};
  // 161 - WRNLoTh1
  AMACv2Field WRNLoThCh3  ={"WRNLoThCh3" , &RegisterWRNLoTh1,  0, 10, 0x0};
  AMACv2Field WRNLoThCh4  ={"WRNLoThCh4" , &RegisterWRNLoTh1, 10, 10, 0x0};
  AMACv2Field WRNLoThCh5  ={"WRNLoThCh5" , &RegisterWRNLoTh1, 20, 10, 0x0};
  // 162 - WRNLoTh2
  AMACv2Field WRNLoThCh6  ={"WRNLoThCh6" , &RegisterWRNLoTh2,  0, 10, 0x0};
  AMACv2Field WRNLoThCh7  ={"WRNLoThCh7" , &RegisterWRNLoTh2, 10, 10, 0x0};
  AMACv2Field WRNLoThCh8  ={"WRNLoThCh8" , &RegisterWRNLoTh2, 20, 10, 0x0};
  // 163 - WRNLoTh3
  AMACv2Field WRNLoThCh9  ={"WRNLoThCh9" , &RegisterWRNLoTh3,  0, 10, 0x0};
  AMACv2Field WRNLoThCh10 ={"WRNLoThCh10", &RegisterWRNLoTh3, 10, 10, 0x0};
  AMACv2Field WRNLoThCh11 ={"WRNLoThCh11", &RegisterWRNLoTh3, 20, 10, 0x0};
  // 164 - WRNLoTh4
  AMACv2Field WRNLoThCh12 ={"WRNLoThCh12", &RegisterWRNLoTh4,  0, 10, 0x0};
  AMACv2Field WRNLoThCh13 ={"WRNLoThCh13", &RegisterWRNLoTh4, 10, 10, 0x0};
  AMACv2Field WRNLoThCh14 ={"WRNLoThCh14", &RegisterWRNLoTh4, 20, 10, 0x0};
  // 165 - WRNLoTh5
  AMACv2Field WRNLoThCh15 ={"WRNLoThCh15", &RegisterWRNLoTh5,  0, 10, 0x0};
  // 166 - WRNHiTh0
  AMACv2Field WRNHiThCh0  ={"WRNHiThCh0" , &RegisterWRNHiTh0,  0, 10, 0x3FF};
  AMACv2Field WRNHiThCh1  ={"WRNHiThCh1" , &RegisterWRNHiTh0, 10, 10, 0x3FF};
  AMACv2Field WRNHiThCh2  ={"WRNHiThCh2" , &RegisterWRNHiTh0, 20, 10, 0x3FF};
  // 167 - WRNHiTh1
  AMACv2Field WRNHiThCh3  ={"WRNHiThCh3" , &RegisterWRNHiTh1,  0, 10, 0x3FF};
  AMACv2Field WRNHiThCh4  ={"WRNHiThCh4" , &RegisterWRNHiTh1, 10, 10, 0x3FF};
  AMACv2Field WRNHiThCh5  ={"WRNHiThCh5" , &RegisterWRNHiTh1, 20, 10, 0x3FF};
  // 168 - WRNHiTh2
  AMACv2Field WRNHiThCh6  ={"WRNHiThCh6" , &RegisterWRNHiTh2,  0, 10, 0x3FF};
  AMACv2Field WRNHiThCh7  ={"WRNHiThCh7" , &RegisterWRNHiTh2, 10, 10, 0x3FF};
  AMACv2Field WRNHiThCh8  ={"WRNHiThCh8" , &RegisterWRNHiTh2, 20, 10, 0x3FF};
  // 169 - WRNHiTh3
  AMACv2Field WRNHiThCh9  ={"WRNHiThCh9" , &RegisterWRNHiTh3,  0, 10, 0x3FF};
  AMACv2Field WRNHiThCh10 ={"WRNHiThCh10", &RegisterWRNHiTh3, 10, 10, 0x3FF};
  AMACv2Field WRNHiThCh11 ={"WRNHiThCh11", &RegisterWRNHiTh3, 20, 10, 0x3FF};
  // 170 - WRNHiTh4
  AMACv2Field WRNHiThCh12 ={"WRNHiThCh12", &RegisterWRNHiTh4,  0, 10, 0x3FF};
  AMACv2Field WRNHiThCh13 ={"WRNHiThCh13", &RegisterWRNHiTh4, 10, 10, 0x3FF};
  AMACv2Field WRNHiThCh14 ={"WRNHiThCh14", &RegisterWRNHiTh4, 20, 10, 0x3FF};
  // 171 - WRNHiTh5
  AMACv2Field WRNHiThCh15 ={"WRNHiThCh15", &RegisterWRNHiTh5,  0, 10, 0x3FF};


  /** @} */  
protected:
  //! List of all registers
  std::vector<AMACv2Register AMACv2RegMap::*> m_registers;
  //! List of all fields
  std::vector<AMACv2Field AMACv2RegMap::*> m_fields;
  
  //
  // Name to attribute maps
  std::unordered_map<uint8_t    , AMACv2Register AMACv2RegMap::*> m_regAddrMap;
  std::unordered_map<std::string, AMACv2Register AMACv2RegMap::*> m_regMap;
  std::unordered_map<std::string, AMACv2Field AMACv2RegMap::*> m_fieldMap;
};
#endif //AMACREGMAP_H
