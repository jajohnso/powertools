#include "AMACv2RegMap.h"

#include <algorithm>

AMACv2RegMap::AMACv2RegMap()
{
  init();
}

void AMACv2RegMap::init()
{
  //
  // Create register maps
  m_registers = {
    &AMACv2RegMap::RegisterStatus, &AMACv2RegMap::RegisterHxFlags, &AMACv2RegMap::RegisterHyFlags, &AMACv2RegMap::RegisterHV0Flags, &AMACv2RegMap::RegisterHV2Flags, &AMACv2RegMap::RegisterDCDCFlags, &AMACv2RegMap::RegisterWRNHxFlags, &AMACv2RegMap::RegisterSynFlags,
    &AMACv2RegMap::RegisterValue0,&AMACv2RegMap::RegisterValue1,&AMACv2RegMap::RegisterValue2,&AMACv2RegMap::RegisterValue3,&AMACv2RegMap::RegisterValue4,&AMACv2RegMap::RegisterValue5,
    &AMACv2RegMap::RegisterSerNum,
    &AMACv2RegMap::RegisterFlagResets,&AMACv2RegMap::RegisterLogicReset,&AMACv2RegMap::RegisterHardReset,
    &AMACv2RegMap::RegisterCntSet,&AMACv2RegMap::RegisterCntSetC,&AMACv2RegMap::RegisterDCDCen,&AMACv2RegMap::RegisterDCDCenC,&AMACv2RegMap::RegisterIlock,&AMACv2RegMap::RegisterIlockC,&AMACv2RegMap::RegisterRstCnt,&AMACv2RegMap::RegisterRstCntC,&AMACv2RegMap::RegisterAMen,&AMACv2RegMap::RegisterAMenC,&AMACv2RegMap::RegisterAMpwr,&AMACv2RegMap::RegisterAMpwrC,&AMACv2RegMap::RegisterBgCnt,&AMACv2RegMap::RegisterAMCnt,&AMACv2RegMap::RegisterDACs0,&AMACv2RegMap::RegisterDACbias,&AMACv2RegMap::RegisterAMACCnt,&AMACv2RegMap::RegisterNTCRange,&AMACv2RegMap::RegisterLVCurCal,
    &AMACv2RegMap::RegisterHxICfg,&AMACv2RegMap::RegisterHyICfg,&AMACv2RegMap::RegisterHV0ICfg,&AMACv2RegMap::RegisterHV2ICfg,&AMACv2RegMap::RegisterDCDCICfg,&AMACv2RegMap::RegisterWRNICfg,
    &AMACv2RegMap::RegisterHxTLUT,&AMACv2RegMap::RegisterHxModLUT1,&AMACv2RegMap::RegisterHxModLUT2,
    &AMACv2RegMap::RegisterHyTLUT,&AMACv2RegMap::RegisterHyModLUT1,&AMACv2RegMap::RegisterHyModLUT2,
    &AMACv2RegMap::RegisterHV0TLUT,&AMACv2RegMap::RegisterHV0ModLUT1,&AMACv2RegMap::RegisterHV0ModLUT2,
    &AMACv2RegMap::RegisterHV2TLUT,&AMACv2RegMap::RegisterHV2ModLUT1,&AMACv2RegMap::RegisterHV2ModLUT2,
    &AMACv2RegMap::RegisterDCDCTLUT,&AMACv2RegMap::RegisterDCDCModLUT1,&AMACv2RegMap::RegisterDCDCModLUT2,
    &AMACv2RegMap::RegisterWRNTLUT,&AMACv2RegMap::RegisterWRNModLUT1,&AMACv2RegMap::RegisterWRNModLUT2,
    &AMACv2RegMap::RegisterHxFlagEn,&AMACv2RegMap::RegisterHyFlagEn,&AMACv2RegMap::RegisterHV0FlagEn,&AMACv2RegMap::RegisterHV2FlagEn,&AMACv2RegMap::RegisterDCDCFlagEn,&AMACv2RegMap::RegisterSynFlagEn,
    //AMACv2&AMACv2RegMap::Register&AMACv2RegMap::RegisterWRNFlagEn,
    &AMACv2RegMap::RegisterHxLoTh0,&AMACv2RegMap::RegisterHxLoTh1,&AMACv2RegMap::RegisterHxLoTh2,&AMACv2RegMap::RegisterHxLoTh3,&AMACv2RegMap::RegisterHxLoTh4,&AMACv2RegMap::RegisterHxLoTh5,
    &AMACv2RegMap::RegisterHxHiTh0,&AMACv2RegMap::RegisterHxHiTh1,&AMACv2RegMap::RegisterHxHiTh2,&AMACv2RegMap::RegisterHxHiTh3,&AMACv2RegMap::RegisterHxHiTh4,&AMACv2RegMap::RegisterHxHiTh5,
    &AMACv2RegMap::RegisterHyLoTh0,&AMACv2RegMap::RegisterHyLoTh1,&AMACv2RegMap::RegisterHyLoTh2,&AMACv2RegMap::RegisterHyLoTh3,&AMACv2RegMap::RegisterHyLoTh4,&AMACv2RegMap::RegisterHyLoTh5,
    &AMACv2RegMap::RegisterHyHiTh0,&AMACv2RegMap::RegisterHyHiTh1,&AMACv2RegMap::RegisterHyHiTh2,&AMACv2RegMap::RegisterHyHiTh3,&AMACv2RegMap::RegisterHyHiTh4,&AMACv2RegMap::RegisterHyHiTh5,
    &AMACv2RegMap::RegisterHV0LoTh0,&AMACv2RegMap::RegisterHV0LoTh1,&AMACv2RegMap::RegisterHV0LoTh2,&AMACv2RegMap::RegisterHV0LoTh3,&AMACv2RegMap::RegisterHV0LoTh4,&AMACv2RegMap::RegisterHV0LoTh5,
    &AMACv2RegMap::RegisterHV0HiTh0,&AMACv2RegMap::RegisterHV0HiTh1,&AMACv2RegMap::RegisterHV0HiTh2,&AMACv2RegMap::RegisterHV0HiTh3,&AMACv2RegMap::RegisterHV0HiTh4,&AMACv2RegMap::RegisterHV0HiTh5,
    &AMACv2RegMap::RegisterHV2LoTh0,&AMACv2RegMap::RegisterHV2LoTh1,&AMACv2RegMap::RegisterHV2LoTh2,&AMACv2RegMap::RegisterHV2LoTh3,&AMACv2RegMap::RegisterHV2LoTh4,&AMACv2RegMap::RegisterHV2LoTh5,
    &AMACv2RegMap::RegisterHV2HiTh0,&AMACv2RegMap::RegisterHV2HiTh1,&AMACv2RegMap::RegisterHV2HiTh2,&AMACv2RegMap::RegisterHV2HiTh3,&AMACv2RegMap::RegisterHV2HiTh4,&AMACv2RegMap::RegisterHV2HiTh5,
    &AMACv2RegMap::RegisterDCDCLoTh0,&AMACv2RegMap::RegisterDCDCLoTh1,&AMACv2RegMap::RegisterDCDCLoTh2,&AMACv2RegMap::RegisterDCDCLoTh3,&AMACv2RegMap::RegisterDCDCLoTh4,&AMACv2RegMap::RegisterDCDCLoTh5,
    &AMACv2RegMap::RegisterDCDCHiTh0,&AMACv2RegMap::RegisterDCDCHiTh1,&AMACv2RegMap::RegisterDCDCHiTh2,&AMACv2RegMap::RegisterDCDCHiTh3,&AMACv2RegMap::RegisterDCDCHiTh4,&AMACv2RegMap::RegisterDCDCHiTh5,
    &AMACv2RegMap::RegisterWRNLoTh0,&AMACv2RegMap::RegisterWRNLoTh1,&AMACv2RegMap::RegisterWRNLoTh2,&AMACv2RegMap::RegisterWRNLoTh3,&AMACv2RegMap::RegisterWRNLoTh4,&AMACv2RegMap::RegisterWRNLoTh5,
    &AMACv2RegMap::RegisterWRNHiTh0,&AMACv2RegMap::RegisterWRNHiTh1,&AMACv2RegMap::RegisterWRNHiTh2,&AMACv2RegMap::RegisterWRNHiTh3,&AMACv2RegMap::RegisterWRNHiTh4,&AMACv2RegMap::RegisterWRNHiTh5,
  };

  for(AMACv2Register AMACv2RegMap::* reg : m_registers)
    {
      m_regAddrMap[(this->*reg).getAddress()]=reg;
      m_regMap    [(this->*reg).getName   ()]=reg;
    }
  

  //
  //
  m_fields =
    {
      // 0 - Status
      &AMACv2RegMap::StatusAM, &AMACv2RegMap::StatusWARN, &AMACv2RegMap::StatusDCDC, &AMACv2RegMap::StatusHV3, &AMACv2RegMap::StatusHV2, &AMACv2RegMap::StatusHV1, &AMACv2RegMap::StatusHV0, &AMACv2RegMap::StatusY2LDO, &AMACv2RegMap::StatusY1LDO, &AMACv2RegMap::StatusY0LDO, &AMACv2RegMap::StatusX2LDO, &AMACv2RegMap::StatusX1LDO, &AMACv2RegMap::StatusX0LDO, &AMACv2RegMap::StatusGPI, &AMACv2RegMap::StatusPGOOD, &AMACv2RegMap::StatusILockWARN, &AMACv2RegMap::StatusILockDCDC, &AMACv2RegMap::StatusILockHV2, &AMACv2RegMap::StatusILockHV2, &AMACv2RegMap::StatusILockYLDO, &AMACv2RegMap::StatusILockxLDO,
      // 1 - HxFlags
      &AMACv2RegMap::HxFlagsHi, &AMACv2RegMap::HxFlagsLo,
      // 2 - HyFlags
      &AMACv2RegMap::HyFlagsHi, &AMACv2RegMap::HyFlagsLo,
      // 3 - HV0Flags
      &AMACv2RegMap::HV0FlagsHi, &AMACv2RegMap::HV0FlagsLo,
      // 4 - HV2Flags
      &AMACv2RegMap::HV2FlagsHi, &AMACv2RegMap::HV2FlagsLo,
      // 5 - DCDCflags
      &AMACv2RegMap::DCDCflagsHi, &AMACv2RegMap::DCDCflagsLo,
      // 6 - WRNflags
      &AMACv2RegMap::WRNflagsHi, &AMACv2RegMap::WRNflagsLo,
      // 7 - SynFlags
      &AMACv2RegMap::SynFlagsWRN, &AMACv2RegMap::SynFlagsDCDC, &AMACv2RegMap::SynFlagsHV2, &AMACv2RegMap::SynFlagsHV2, &AMACv2RegMap::SynFlagsHy, &AMACv2RegMap::SynFlagsHx,
      // 10 - Value0
      &AMACv2RegMap::Value0AMen, &AMACv2RegMap::Ch0Value, &AMACv2RegMap::Ch1Value, &AMACv2RegMap::Ch2Value,
      // 11 - Value1
      &AMACv2RegMap::Value1AMen, &AMACv2RegMap::Ch3Value, &AMACv2RegMap::Ch4Value, &AMACv2RegMap::Ch5Value,
      // 12 - Value2
      &AMACv2RegMap::Value2AMen, &AMACv2RegMap::Ch6Value, &AMACv2RegMap::Ch7Value, &AMACv2RegMap::Ch8Value,
      // 13 - Value3
      &AMACv2RegMap::Value3AMen, &AMACv2RegMap::Ch9Value, &AMACv2RegMap::Ch10Value, &AMACv2RegMap::Ch11Value,
      // 14 - Value4
      &AMACv2RegMap::Value4AMen, &AMACv2RegMap::Ch12Value, &AMACv2RegMap::Ch13Value, &AMACv2RegMap::Ch14Value,
      // 15 - Value5
      &AMACv2RegMap::Value5AMen, &AMACv2RegMap::Ch15Value,
      // 31 - SerNum
      &AMACv2RegMap::PadID, &AMACv2RegMap::SerNum,
      // 32 - FlagResets
      &AMACv2RegMap::FlagResetWRN, &AMACv2RegMap::FlagResetDCDC, &AMACv2RegMap::FlagResetHV2, &AMACv2RegMap::FlagResetHV0, &AMACv2RegMap::FlagResetXLDO, &AMACv2RegMap::FlagResetYLDO,
      // 33 - LogicReset
      &AMACv2RegMap::LogicReset,
      // 34 - HardReset
      &AMACv2RegMap::HardReset,
      // 40 - CntSet
      &AMACv2RegMap::CntSetHV3frq, &AMACv2RegMap::CntSetHV3en, &AMACv2RegMap::CntSetHV2frq, &AMACv2RegMap::CntSetHV2en, &AMACv2RegMap::CntSetHV1frq, &AMACv2RegMap::CntSetHV1en, &AMACv2RegMap::CntSetHV0frq, &AMACv2RegMap::CntSetHV0en, &AMACv2RegMap::CntSetHyLDO2en, &AMACv2RegMap::CntSetHyLDO1en, &AMACv2RegMap::CntSetHyLDO0en, &AMACv2RegMap::CntSetHxLDO2en, &AMACv2RegMap::CntSetHxLDO1en, &AMACv2RegMap::CntSetHxLDO0en, &AMACv2RegMap::CntSetWARN,
      // 41 - CntSetC
      &AMACv2RegMap::CntSetCHV3frq, &AMACv2RegMap::CntSetCHV3en, &AMACv2RegMap::CntSetCHV2frq, &AMACv2RegMap::CntSetCHV2en, &AMACv2RegMap::CntSetCHV1frq, &AMACv2RegMap::CntSetCHV1en, &AMACv2RegMap::CntSetCHV0frq, &AMACv2RegMap::CntSetCHV0en, &AMACv2RegMap::CntSetCHyLDO2en, &AMACv2RegMap::CntSetCHyLDO1en, &AMACv2RegMap::CntSetCHyLDO0en, &AMACv2RegMap::CntSetCHxLDO2en, &AMACv2RegMap::CntSetCHxLDO1en, &AMACv2RegMap::CntSetCHxLDO0en, &AMACv2RegMap::CntSetCWARN,
      // 42 - DCDCen
      &AMACv2RegMap::DCDCAdj, &AMACv2RegMap::DCDCen,
      // 43 - DCDCenC
      &AMACv2RegMap::DCDCAdjC, &AMACv2RegMap::DCDCenC,
      // 44 - Ilock
      &AMACv2RegMap::IlockWRN, &AMACv2RegMap::IlockDCDC, &AMACv2RegMap::IlockHV2, &AMACv2RegMap::IlockHV2, &AMACv2RegMap::IlockHy, &AMACv2RegMap::IlockHx,
      // 45 - IlockC
      &AMACv2RegMap::IlockCWRN, &AMACv2RegMap::IlockCDCDC, &AMACv2RegMap::IlockCHV2, &AMACv2RegMap::IlockCHV2, &AMACv2RegMap::IlockCHy, &AMACv2RegMap::IlockCHx,
      // 46 - RstCnt
      &AMACv2RegMap::RstCntHyHCCresetB, &AMACv2RegMap::RstCntHxHCCresetB, &AMACv2RegMap::RstCntOF,
      // 47 - RstCntC
      &AMACv2RegMap::RstCntCHyHCCresetB, &AMACv2RegMap::RstCntCHxHCCresetB, &AMACv2RegMap::RstCntCOF,
      // 48 - AMen
      &AMACv2RegMap::AMzeroCalib, &AMACv2RegMap::AMen,
      // 49 - AMenC
      &AMACv2RegMap::AMzeroCalibC, &AMACv2RegMap::AMenC,
      // 50 - AMpwr
      &AMACv2RegMap::ReqDCDCPGOOD, &AMACv2RegMap::DCDCenToPwrAMAC,
      // 51 - AMpwrC
      &AMACv2RegMap::ReqDCDCPGOODC, &AMACv2RegMap::DCDCenToPwrAMACC,
      // 52 - BgCnt
      &AMACv2RegMap::AMbgen, &AMACv2RegMap::AMbg, &AMACv2RegMap::VDDbgen, &AMACv2RegMap::VDDbg,
      // 53 - AMcnt
      &AMACv2RegMap::AMintCalib, &AMACv2RegMap::Ch13Mux, &AMACv2RegMap::Ch12Mux, &AMACv2RegMap::Ch5Mux, &AMACv2RegMap::Ch4Mux, &AMACv2RegMap::Ch3Mux,
      // 54 - Dacs0
      &AMACv2RegMap::DACShunty, &AMACv2RegMap::DACShuntx, &AMACv2RegMap::DACCaly, &AMACv2RegMap::DACCalx,
      // 55 - DACbias
      &AMACv2RegMap::DACbias,
      // 56 - AMACcnt
      &AMACv2RegMap::HVcurGain, &AMACv2RegMap::DRcomMode, &AMACv2RegMap::DRcurr, &AMACv2RegMap::RingOscFrq,
      // 57 - NTC
      &AMACv2RegMap::CTAToffset, &AMACv2RegMap::NTCpbCal, &AMACv2RegMap::NTCpbSenseRange, &AMACv2RegMap::NTCy0Cal, &AMACv2RegMap::NTCy0SenseRange, &AMACv2RegMap::NTCx0Cal, &AMACv2RegMap::NTCx0SenseRange,
      // 58 - LvCurCal
      &AMACv2RegMap::DCDCoOffset, &AMACv2RegMap::DCDCoZeroReading, &AMACv2RegMap::DCDCoN, &AMACv2RegMap::DCDCoP, &AMACv2RegMap::DCDCiZeroReading, &AMACv2RegMap::DCDCiRangeSW, &AMACv2RegMap::DCDCiOffset, &AMACv2RegMap::DCDCiP, &AMACv2RegMap::DCDCiN,
      // 60 - HxICm_cfg
      &AMACv2RegMap::HxLAM, &AMACv2RegMap::HxFlagsLatch, &AMACv2RegMap::HxFlagsLogic, &AMACv2RegMap::HxFlagsLogic, &AMACv2RegMap::HxFlagsLogic,
      // 61 - HyICm_cfg
      &AMACv2RegMap::HyLAM, &AMACv2RegMap::HyFlagsLatch, &AMACv2RegMap::HyFlagsLogic, &AMACv2RegMap::HyFlagsLogic, &AMACv2RegMap::HyFlagsLogic,
      // 62 - HV0ICm_cfg
      &AMACv2RegMap::HV0LAM, &AMACv2RegMap::HV0FlagsLatch, &AMACv2RegMap::HV0FlagsLogic, &AMACv2RegMap::HV0FlagsLogic, &AMACv2RegMap::HV0FlagsLogic,
      // 63 - HV2ICm_cfg
      &AMACv2RegMap::HV2LAM, &AMACv2RegMap::HV2FlagsLatch, &AMACv2RegMap::HV2FlagsLogic, &AMACv2RegMap::HV2FlagsLogic, &AMACv2RegMap::HV2FlagsLogic,
      // 64 - DCDCICm_cfg
      &AMACv2RegMap::DCDCLAM, &AMACv2RegMap::DCDCFlagsLatch, &AMACv2RegMap::DCDCFlagsLogic, &AMACv2RegMap::DCDCFlagsLogic, &AMACv2RegMap::DCDCFlagsLogic,
      // 65 - WRNICm_cfg
      &AMACv2RegMap::WRNLAM, &AMACv2RegMap::WRNFlagsLatch, &AMACv2RegMap::WRNFlagsLogic, &AMACv2RegMap::WRNFlagsLogic, &AMACv2RegMap::WRNFlagsLogic,
      // 70 - HxTlut
      &AMACv2RegMap::HxTlut,
      // 71 - HxModlut1
      &AMACv2RegMap::HxModlut1,
      // 72 - HxModlut2
      &AMACv2RegMap::HxModlut2,
      // 73 - HyTlut
      &AMACv2RegMap::HyTlut,
      // 74 - HyModlut1
      &AMACv2RegMap::HyModlut1,
      // 75 - HyModlut2
      &AMACv2RegMap::HyModlut2,
      // 76 - HV0Tlut,
      &AMACv2RegMap::HV0Tlut,
      // 77 - HV0Modlut1
      &AMACv2RegMap::HV0Modlut1,
      // 78 - HV0Modlut2
      &AMACv2RegMap::HV0Modlut2,
      // 79 - HV2Tlut
      &AMACv2RegMap::HV2Tlut,
      // 80 - HV2Modlut1
      &AMACv2RegMap::HV2Modlut1,
      // 81 - HV2Modlut2
      &AMACv2RegMap::HV2Modlut2,
      // 82 - DCDCTlut
      &AMACv2RegMap::DCDCTlut,
      // 83 - DCDCModlut1
      &AMACv2RegMap::DCDCModlut1,
      // 84 - DCDCModlut2
      &AMACv2RegMap::DCDCModlut2,
      // 85 - WRNTlut
      &AMACv2RegMap::WRNTlut,
      // 86 - WRNModlut1
      &AMACv2RegMap::WRNModlut1,
      // 87 - WRNModlut2
      &AMACv2RegMap::WRNModlut2,
      // 90 - HxFlagEn
      &AMACv2RegMap::HxFlagsEnHi, &AMACv2RegMap::HxFlagsEnLo,
      // 91 - HyFlagEn
      &AMACv2RegMap::HyFlagsEnHi, &AMACv2RegMap::HyFlagsEnLo,
      // 92 - HV0FlagEn
      &AMACv2RegMap::HV0FlagsEnHi, &AMACv2RegMap::HV0FlagsEnLo,
      // 93 - HV2FlagEn
      &AMACv2RegMap::HxFlagsEnHi, &AMACv2RegMap::HxFlagsEnLo,
      // 94 - DCDCFlagEn
      &AMACv2RegMap::DCDCFlagsEnHi, &AMACv2RegMap::DCDCFlagsEnLo,
      // 94 - WRNFlagEn
      &AMACv2RegMap::WRNFlagsEnHi, &AMACv2RegMap::WRNFlagsEnLo,
      // 95 - SynFlagEn
      &AMACv2RegMap::WRNsynFlagEnHi, &AMACv2RegMap::WRNsynFlagEnLo, &AMACv2RegMap::DCDCsynFlagEnHi, &AMACv2RegMap::DCDCsynFlagEnLo, &AMACv2RegMap::HV2synFlagEnHi, &AMACv2RegMap::HV2synFlagEnLo, &AMACv2RegMap::HV0synFlagEnHi, &AMACv2RegMap::HV0synFlagEnLo, &AMACv2RegMap::HysynFlagEnHi, &AMACv2RegMap::HysynFlagEnLo, &AMACv2RegMap::HxsynFlagEnHi, &AMACv2RegMap::HxsynFlagEnLo,
      // 100 - HxLoTh0
      &AMACv2RegMap::HxLoThCh0, &AMACv2RegMap::HxLoThCh1, &AMACv2RegMap::HxLoThCh2,
      // 101 - HxLoTh1
      &AMACv2RegMap::HxLoThCh3, &AMACv2RegMap::HxLoThCh4, &AMACv2RegMap::HxLoThCh5,
      // 102 - HxLoTh2
      &AMACv2RegMap::HxLoThCh6, &AMACv2RegMap::HxLoThCh7, &AMACv2RegMap::HxLoThCh8,
      // 103 - HxLoTh3
      &AMACv2RegMap::HxLoThCh9, &AMACv2RegMap::HxLoThCh10, &AMACv2RegMap::HxLoThCh11,
      // 104 - HxLoTh4
      &AMACv2RegMap::HxLoThCh12, &AMACv2RegMap::HxLoThCh13, &AMACv2RegMap::HxLoThCh14,
      // 105 - HxLoTh5
      &AMACv2RegMap::HxLoThCh15,
      // 106 - HxHiTh0
      &AMACv2RegMap::HxHiThCh0, &AMACv2RegMap::HxHiThCh1, &AMACv2RegMap::HxHiThCh2,
      // 107 - HxHiTh1
      &AMACv2RegMap::HxHiThCh3, &AMACv2RegMap::HxHiThCh4, &AMACv2RegMap::HxHiThCh5,
      // 108 - HxHiTh2
      &AMACv2RegMap::HxHiThCh6, &AMACv2RegMap::HxHiThCh7, &AMACv2RegMap::HxHiThCh8,
      // 109 - HxHiTh3
      &AMACv2RegMap::HxHiThCh9, &AMACv2RegMap::HxHiThCh10, &AMACv2RegMap::HxHiThCh11,
      // 110 - HxHiTh4
      &AMACv2RegMap::HxHiThCh12, &AMACv2RegMap::HxHiThCh13, &AMACv2RegMap::HxHiThCh14,
      // 111 - HxHiTh5
      &AMACv2RegMap::HxHiThCh15,
      // 112 - HyLoTh0
      &AMACv2RegMap::HyLoThCh0, &AMACv2RegMap::HyLoThCh1, &AMACv2RegMap::HyLoThCh2,
      // 113 - HyLoTh1
      &AMACv2RegMap::HyLoThCh3, &AMACv2RegMap::HyLoThCh4, &AMACv2RegMap::HyLoThCh5,
      // 114 - HyLoTh2
      &AMACv2RegMap::HyLoThCh6, &AMACv2RegMap::HyLoThCh7, &AMACv2RegMap::HyLoThCh8,
      // 115 - HyLoTh3
      &AMACv2RegMap::HyLoThCh9, &AMACv2RegMap::HyLoThCh10, &AMACv2RegMap::HyLoThCh11,
      // 116 - HyLoTh4
      &AMACv2RegMap::HyLoThCh12, &AMACv2RegMap::HyLoThCh13, &AMACv2RegMap::HyLoThCh14,
      // 117 - HyLoTh5
      &AMACv2RegMap::HyLoThCh15,
      // 118 - HyHiTh0
      &AMACv2RegMap::HyHiThCh0, &AMACv2RegMap::HyHiThCh1, &AMACv2RegMap::HyHiThCh2,
      // 119 - HyHiTh1
      &AMACv2RegMap::HyHiThCh3, &AMACv2RegMap::HyHiThCh4, &AMACv2RegMap::HyHiThCh5,
      // 120 - HyHiTh2
      &AMACv2RegMap::HyHiThCh6, &AMACv2RegMap::HyHiThCh7, &AMACv2RegMap::HyHiThCh8,
      // 121 - HyHiTh3
      &AMACv2RegMap::HyHiThCh9, &AMACv2RegMap::HyHiThCh10, &AMACv2RegMap::HyHiThCh11,
      // 122 - HyHiTh4
      &AMACv2RegMap::HyHiThCh12, &AMACv2RegMap::HyHiThCh13, &AMACv2RegMap::HyHiThCh14,
      // 123 - HyHiTh5
      &AMACv2RegMap::HyHiThCh15,
      // 124 - HV0LoTh0
      &AMACv2RegMap::HV0LoThCh0, &AMACv2RegMap::HV0LoThCh1, &AMACv2RegMap::HV0LoThCh2,
      // 125 - HV0LoTh1
      &AMACv2RegMap::HV0LoThCh3, &AMACv2RegMap::HV0LoThCh4, &AMACv2RegMap::HV0LoThCh5,
      // 126 - HV0LoTh2
      &AMACv2RegMap::HV0LoThCh6, &AMACv2RegMap::HV0LoThCh7, &AMACv2RegMap::HV0LoThCh8,
      // 127 - HV0LoTh3
      &AMACv2RegMap::HV0LoThCh9, &AMACv2RegMap::HV0LoThCh10, &AMACv2RegMap::HV0LoThCh11,
      // 128 - HV0LoTh4
      &AMACv2RegMap::HV0LoThCh12, &AMACv2RegMap::HV0LoThCh13, &AMACv2RegMap::HV0LoThCh14,
      // 129 - HV0LoTh5
      &AMACv2RegMap::HV0LoThCh15,
      // 130 - HV0HiTh0
      &AMACv2RegMap::HV0HiThCh0, &AMACv2RegMap::HV0HiThCh1, &AMACv2RegMap::HV0HiThCh2,
      // 131 - HV0HiTh1
      &AMACv2RegMap::HV0HiThCh3, &AMACv2RegMap::HV0HiThCh4, &AMACv2RegMap::HV0HiThCh5,
      // 132 - HV0HiTh2
      &AMACv2RegMap::HV0HiThCh6, &AMACv2RegMap::HV0HiThCh7, &AMACv2RegMap::HV0HiThCh8,
      // 133 - HV0HiTh3
      &AMACv2RegMap::HV0HiThCh9, &AMACv2RegMap::HV0HiThCh10, &AMACv2RegMap::HV0HiThCh11,
      // 134 - HV0HiTh4
      &AMACv2RegMap::HV0HiThCh12, &AMACv2RegMap::HV0HiThCh13, &AMACv2RegMap::HV0HiThCh14,
      // 135 - HV0HiTh5
      &AMACv2RegMap::HV0HiThCh15,
      // 136 - HV2LoTh0
      &AMACv2RegMap::HV2LoThCh0, &AMACv2RegMap::HV2LoThCh1, &AMACv2RegMap::HV2LoThCh2,
      // 137 - HV2LoTh1
      &AMACv2RegMap::HV2LoThCh3, &AMACv2RegMap::HV2LoThCh4, &AMACv2RegMap::HV2LoThCh5,
      // 138 - HV2LoTh2
      &AMACv2RegMap::HV2LoThCh6, &AMACv2RegMap::HV2LoThCh7, &AMACv2RegMap::HV2LoThCh8,
      // 139 - HV2LoTh3
      &AMACv2RegMap::HV2LoThCh9, &AMACv2RegMap::HV2LoThCh10, &AMACv2RegMap::HV2LoThCh11,
      // 140 - HV2LoTh4
      &AMACv2RegMap::HV2LoThCh12, &AMACv2RegMap::HV2LoThCh13, &AMACv2RegMap::HV2LoThCh14,
      // 141 - HV2LoTh5
      &AMACv2RegMap::HV2LoThCh15,
      // 142 - HV2HiTh0
      &AMACv2RegMap::HV2HiThCh0, &AMACv2RegMap::HV2HiThCh1, &AMACv2RegMap::HV2HiThCh2,
      // 143 - HV2HiTh1
      &AMACv2RegMap::HV2HiThCh3, &AMACv2RegMap::HV2HiThCh4, &AMACv2RegMap::HV2HiThCh5,
      // 144 - HV2HiTh2
      &AMACv2RegMap::HV2HiThCh6, &AMACv2RegMap::HV2HiThCh7, &AMACv2RegMap::HV2HiThCh8,
      // 145 - HV2HiTh3
      &AMACv2RegMap::HV2HiThCh9, &AMACv2RegMap::HV2HiThCh10, &AMACv2RegMap::HV2HiThCh11,
      // 146 - HV2HiTh4
      &AMACv2RegMap::HV2HiThCh12, &AMACv2RegMap::HV2HiThCh13, &AMACv2RegMap::HV2HiThCh14,
      // 147 - HV2HiTh5
      &AMACv2RegMap::HV2HiThCh15,
      // 148 - DCDCLoTh0
      &AMACv2RegMap::DCDCLoThCh0, &AMACv2RegMap::DCDCLoThCh1, &AMACv2RegMap::DCDCLoThCh2,
      // 149 - DCDCLoTh1
      &AMACv2RegMap::DCDCLoThCh3, &AMACv2RegMap::DCDCLoThCh4, &AMACv2RegMap::DCDCLoThCh5,
      // 150 - DCDCLoTh2
      &AMACv2RegMap::DCDCLoThCh6, &AMACv2RegMap::DCDCLoThCh7, &AMACv2RegMap::DCDCLoThCh8,
      // 151 - DCDCLoTh3
      &AMACv2RegMap::DCDCLoThCh9, &AMACv2RegMap::DCDCLoThCh10, &AMACv2RegMap::DCDCLoThCh11,
      // 152 - DCDCLoTh4
      &AMACv2RegMap::DCDCLoThCh12, &AMACv2RegMap::DCDCLoThCh13, &AMACv2RegMap::DCDCLoThCh14,
      // 153 - DCDCLoTh5
      &AMACv2RegMap::DCDCLoThCh15,
      // 154 - DCDCHiTh0
      &AMACv2RegMap::DCDCHiThCh0, &AMACv2RegMap::DCDCHiThCh1, &AMACv2RegMap::DCDCHiThCh2,
      // 155 - DCDCHiTh1
      &AMACv2RegMap::DCDCHiThCh3, &AMACv2RegMap::DCDCHiThCh4, &AMACv2RegMap::DCDCHiThCh5,
      // 156 - DCDCHiTh2
      &AMACv2RegMap::DCDCHiThCh6, &AMACv2RegMap::DCDCHiThCh7, &AMACv2RegMap::DCDCHiThCh8,
      // 157 - DCDCHiTh3
      &AMACv2RegMap::DCDCHiThCh9, &AMACv2RegMap::DCDCHiThCh10, &AMACv2RegMap::DCDCHiThCh11,
      // 158 - DCDCHiTh4
      &AMACv2RegMap::DCDCHiThCh12, &AMACv2RegMap::DCDCHiThCh13, &AMACv2RegMap::DCDCHiThCh14,
      // 159 - DCDCHiTh5
      &AMACv2RegMap::DCDCHiThCh15,
      // 160 - WRNLoTh0
      &AMACv2RegMap::WRNLoThCh0, &AMACv2RegMap::WRNLoThCh1, &AMACv2RegMap::WRNLoThCh2,
      // 161 - WRNLoTh1
      &AMACv2RegMap::WRNLoThCh3, &AMACv2RegMap::WRNLoThCh4, &AMACv2RegMap::WRNLoThCh5,
      // 162 - WRNLoTh2
      &AMACv2RegMap::WRNLoThCh6, &AMACv2RegMap::WRNLoThCh7, &AMACv2RegMap::WRNLoThCh8,
      // 163 - WRNLoTh3
      &AMACv2RegMap::WRNLoThCh9, &AMACv2RegMap::WRNLoThCh10, &AMACv2RegMap::WRNLoThCh11,
      // 164 - WRNLoTh4
      &AMACv2RegMap::WRNLoThCh12, &AMACv2RegMap::WRNLoThCh13, &AMACv2RegMap::WRNLoThCh14,
      // 165 - WRNLoTh5
      &AMACv2RegMap::WRNLoThCh15,
      // 166 - WRNHiTh0
      &AMACv2RegMap::WRNHiThCh0, &AMACv2RegMap::WRNHiThCh1, &AMACv2RegMap::WRNHiThCh2,
      // 167 - WRNHiTh1
      &AMACv2RegMap::WRNHiThCh3, &AMACv2RegMap::WRNHiThCh4, &AMACv2RegMap::WRNHiThCh5,
      // 168 - WRNHiTh2
      &AMACv2RegMap::WRNHiThCh6, &AMACv2RegMap::WRNHiThCh7, &AMACv2RegMap::WRNHiThCh8,
      // 169 - WRNHiTh3
      &AMACv2RegMap::WRNHiThCh9, &AMACv2RegMap::WRNHiThCh10, &AMACv2RegMap::WRNHiThCh11,
      // 170 - WRNHiTh4
      &AMACv2RegMap::WRNHiThCh12, &AMACv2RegMap::WRNHiThCh13, &AMACv2RegMap::WRNHiThCh14,
      // 171 - WRNHiTh5
      &AMACv2RegMap::WRNHiThCh15
    };

    for(AMACv2Field AMACv2RegMap::* field : m_fields)
      m_fieldMap[(this->*field).getName()]=field;
}

std::vector<const AMACv2Register*> AMACv2RegMap::getRegisters() const
{
  std::vector<const AMACv2Register*> registers(m_registers.size(), nullptr);
  std::transform(m_registers.cbegin(), m_registers.cend(),
		 registers.begin(),
		 [this](AMACv2Register AMACv2RegMap::* reg)
		 { return (&(this->*reg)); });
  return registers;
}

std::vector<const AMACv2Field*> AMACv2RegMap::getFields() const
{
  std::vector<const AMACv2Field*> fields;
  fields.reserve(m_fieldMap.size());
  for(AMACv2Field AMACv2RegMap::* field : m_fields) fields.push_back(&(this->*field));
  return fields;
}

AMACv2Field* AMACv2RegMap::findField(AMACv2Field AMACv2RegMap::* ref)
{
  return &(this->*ref);
}

AMACv2Field* AMACv2RegMap::findField(const std::string& fieldName)
{
  if(m_fieldMap.find(fieldName) != m_fieldMap.end())
    return &(this->*m_fieldMap[fieldName]);

  return nullptr;
}

uint32_t AMACv2RegMap::getField(AMACv2Field AMACv2RegMap::* ref)
{
  return (this->*ref).read();
}

uint32_t AMACv2RegMap::getField(const std::string& fieldName)
{
  if(m_fieldMap.find(fieldName) != m_fieldMap.end())
    return(this->*m_fieldMap[fieldName]).read();
  else
    std::cerr << " --> Error: Could not find register \""<< fieldName << "\"" << std::endl;
  return 0;
}

uint32_t AMACv2RegMap::getReg(uint32_t reg)
{
  return (this->*m_regAddrMap[reg]).getValue();
}

uint32_t AMACv2RegMap::getReg(AMACv2Register AMACv2RegMap::* ref)
{
  return (this->*ref).getValue();
}

void AMACv2RegMap::setField(AMACv2Field AMACv2RegMap::* ref, uint32_t value)
{
  (this->*ref).write(value);
}

void AMACv2RegMap::setField(const std::string& fieldName, uint32_t value)
{
  if(m_fieldMap.find(fieldName) != m_fieldMap.end())
    return(this->*m_fieldMap[fieldName]).write(value);
  else
    std::cerr << " --> Error: Could not find register \""<< fieldName << "\"" << std::endl;
  return;
}

void AMACv2RegMap::setReg(uint32_t reg, uint32_t value)
{
  (this->*(m_regAddrMap[reg])).setValue(value);
}

void AMACv2RegMap::setReg(AMACv2Register AMACv2RegMap::* ref, uint32_t value)
{
  (this->*ref).setValue(value);
}

uint8_t AMACv2RegMap::getAddr(AMACv2Field AMACv2RegMap::* ref)
{
  return (this->*ref).getRegister()->getAddress();
}

uint8_t AMACv2RegMap::getAddr(const std::string& fieldName)
{
  if(m_fieldMap.find(fieldName) != m_fieldMap.end())
    return (this->*m_fieldMap[fieldName]).getRegister()->getAddress();
  else
    std::cerr << " --> Error: Could not find register \""<< fieldName << "\"" << std::endl;
  return 0;
}


bool AMACv2RegMap::canBeWritten(AMACv2Field AMACv2RegMap::* ref)
{
  return (this->*ref).canBeWrittenField();
}





