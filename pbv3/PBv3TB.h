#ifndef PBV3TB_H
#define PBV3TB_H

#include <memory>

#include <DACDevice.h>

#include "AMACv2.h"

#include "Logger.h"
#include "EquipConf.h"

//! An abstract description of a Powerboard testbench. 
/**
 * A testbench consists of
 * - low voltage power supply
 * - high voltage power supply
 * - N loads connected to powerboards
 * - N powerboards
 */
class PBv3TB
{
public:
  //! \brief Possible outputs of the carrier card on the common analogue output value bus
  enum CARRIER_OUTPUT {
    LINPOL1V4=0,
    LINPOL3V3=1,
    VOUT    =11,
    OFout    =8,
    LDx0EN   =9,
    LDx1EN  =12,
    LDx2EN  =13,
    HRSTBx  =10,
    SHUNTx  =14,
    CALx    =15,
    LDy0EN   =7,
    LDy1EN   =5,
    LDy2EN   =4,
    HRSTBy   =6,
    CALy     =2,
    SHUNTy   =3
  };

  /*
   * Constructor
   */
  PBv3TB() =default;
  
  /*
   * Destructor
   */
  virtual ~PBv3TB() =default;

  /** \name Initialization functions
   * @{ */
  
  /** \brief Store JSON hardware configuration 
   *
   * \param config JSON configuration for the given testbench
   */
  virtual void setConfiguration(const nlohmann::json& config);

  //! Initialize testbench components
  /**
   * Initialize communication and control of the testbench.
   *
   * This function should only perform initialization necessary to communicate
   * with the testbench. The state should not be reset to allow reading from an
   * already active testbench.
   */
  virtual void init() =0;

  //! Power on the testbench
  /**
   * Initialize the state of any important components on the testbench
   * necessary for running a test.
   *
   * Default implementation does not do anything.
   */
  virtual void powerTBOn();

  //! Power off the testbench
  /**
   * Cleanup the testbench components after finishing running of a
   * test.
   *
   * Default implementation does not do anything.
   */
  virtual void powerTBOff();

  /** @} */

  /** \name Control functions
   * Control and measurements of the lower voltage power input
   * @{ 
   */

  //! \brief Return communication object for powerboard pbNum
  virtual std::shared_ptr<AMACv2> getPB(uint8_t pbNum) =0;

  //! \brief Return the DAC object driving the CAL line
  std::shared_ptr<DACDevice> getCalDAC();

  //! \brief Set the OFin for a board
  virtual void setOFin(uint8_t pbNum, bool value) =0;

  //! \brief Get multiplexer values
  virtual double readCarrierOutput(uint32_t pbNum, PBv3TB::CARRIER_OUTPUT value) =0;  
  
  /** @} */

  /** \name Low Voltage
   * Control and measurements of the lower voltage power input
   * @{ 
   */

  //! \brief Set the the low voltage power supply object
  void setLVPS(std::shared_ptr<PowerSupplyChannel> ps);

  //! \brief Get the low voltage power supply object
  std::shared_ptr<PowerSupplyChannel> getLVPS();
  
  //! \brief Get voltage on the low voltage (V) power supply
  virtual double getVin() =0;

  /** \brief Enable LV power
   *
   * Output set is set to 11V with 2A current protect.
   */
  void powerLVOn();

  //! \brief Disable LV power
  void powerLVOff();

  //! \brief Get current (A) supplied by the low voltage power supply
  double getVinCurrent();

  //! \brief Set voltage on the low voltage (V) power supply
  void setVin(double Vin);

  /** @} */

  /** \name Variable Load
   * Control and measurement of the DC/DC load
   * @{
   */

  //! \brief Enable load for PB pbNum
  virtual void loadOn(uint8_t pbNum) =0;

  //! \brief Disable load for PB pbNum
  virtual void loadOff(uint8_t pbNum) =0;
  
  //! \brief Set load on PB pbNum to load Amps
  virtual double setLoad(uint8_t pbNum, double load) =0;

  //! \brief Get load on PB pbNum
  virtual double getLoad(uint8_t pbNum) =0;

  //! \brief Get PB pbNum DC/DC converter output value
  virtual double getVout(uint8_t pbNum) =0;

  //! \brief Get loaded current
  virtual double getIload(uint8_t pbNum) =0;

  /** @} */
  
  /** \name High Voltage
   * Control and measurements of the high voltage
   * @{
   */
  
  //! \brief Set the high voltage power supply object  
  void setHVPS(std::shared_ptr<PowerSupplyChannel> ps);

  //! \brief Get the high voltage power supply object  
  std::shared_ptr<PowerSupplyChannel> getHVPS();
  
  /** \brief Enable HV power
   *
   * Output set is set to 1mA, with 500V voltage protect
   */
  virtual void powerHVOn();

  //! \brief Disable HV power
  virtual void powerHVOff();
  
  //! \brief Get PB pbNum HV output current
  virtual double getHVoutCurrent(uint8_t pbNum) =0;

  /** @} */

protected:
  //! \brief Set the CAL DAC device
  void setCalDAC(std::shared_ptr<DACDevice> CalDAC);

private:
  std::shared_ptr<DACDevice> m_CalDAC;
  
  std::shared_ptr<PowerSupplyChannel> m_lv =nullptr;
  std::shared_ptr<PowerSupplyChannel> m_hv =nullptr;
};

#endif // PBV3TB_H
