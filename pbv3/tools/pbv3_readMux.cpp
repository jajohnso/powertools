#include <unistd.h>
#include <string.h>
#include <getopt.h>

#include <iomanip>
#include <iostream>

#include <Logger.h>

#include "PBv3TBConf.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

//------ Output Map
std::map<std::string,PBv3TB::CARRIER_OUTPUT> outputmap={
  {"LINPOL1V4", PBv3TB::CARRIER_OUTPUT::LINPOL1V4},
  {"LINPOL3V3", PBv3TB::CARRIER_OUTPUT::LINPOL3V3},
  {"VOUT"     , PBv3TB::CARRIER_OUTPUT::VOUT     },
  {"OFout"    , PBv3TB::CARRIER_OUTPUT::OFout    },
  {"LDx0EN"   , PBv3TB::CARRIER_OUTPUT::LDx0EN   },
  {"LDx1EN"   , PBv3TB::CARRIER_OUTPUT::LDx1EN   },
  {"LDx2EN"   , PBv3TB::CARRIER_OUTPUT::LDx2EN   },
  {"HRSTBx"   , PBv3TB::CARRIER_OUTPUT::HRSTBx   },
  {"SHUNTx"   , PBv3TB::CARRIER_OUTPUT::SHUNTx   },
  {"CALx"     , PBv3TB::CARRIER_OUTPUT::CALx     },
  {"LDy0EN"   , PBv3TB::CARRIER_OUTPUT::LDy0EN   },
  {"LDy1EN"   , PBv3TB::CARRIER_OUTPUT::LDy1EN   },
  {"LDy2EN"   , PBv3TB::CARRIER_OUTPUT::LDy2EN   },
  {"HRSTBy"   , PBv3TB::CARRIER_OUTPUT::HRSTBy   },
  {"CALy"     , PBv3TB::CARRIER_OUTPUT::CALy     },
  {"SHUNTy"   , PBv3TB::CARRIER_OUTPUT::SHUNTy   }
};

//---------------

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] output" << std::endl;
  std::cerr << "Output can be either number or one of the following. a name.:" << std::endl;
  for(const std::pair<std::string,PBv3TB::CARRIER_OUTPUT>& kv : outputmap)
    std::cerr << "  " << kv.first << std::endl;
  std::cerr << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -b, --board             Powerboard number (default: " << (uint32_t)pbNum << ")" << std::endl;
  std::cerr << " -e, --equip config.json Equipment configuration file (default: " << equipConfigFile << ")" << std::endl;
  std::cerr << " -d, --debug             Enable more verbose printout"  << std::endl;
  std::cerr << std::endl;
  std::cerr << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
	{
	  {"board"    , required_argument, 0,  'b' },
	  {"equip"    , required_argument, 0,  'e' },
	  {"debug"    , no_argument      , 0,  'd' },
	  {0          , 0                , 0,  0 }
	};

      c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 'b':
	  pbNum = atoi(optarg);
	  break;
	case 'e':
	  equipConfigFile = optarg;
	  break;
	case 'd':
	  logIt::incrDebug();
	  break;
	default:
	  std::cerr << "Invalid option supplied. Aborting." << std::endl;
	  std::cerr << std::endl;
	  usage(argv);
	}
    }

  std::string outStr;
  if (optind == argc-1)
    {
      outStr=argv[optind];
    }
  else
    {
      std::cerr << "Required output argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  logger(logDEBUG) << "Settings";
  logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

  // Create and initialize the testbench
  PBv3TBConf factory_pbv3tb(equipConfigFile);
  std::shared_ptr<PBv3TB> tb=factory_pbv3tb.getPBv3TB("default");
  if(tb==nullptr)
    return 1;


  // Check the output name
  if(outputmap.find(outStr)==outputmap.end())
    {
      std::cerr << "Invalid output name." << std::endl;
      usage(argv);
      return 1;
    }
  PBv3TB::CARRIER_OUTPUT out=outputmap[outStr];

  std::cout << tb->readCarrierOutput(pbNum, out) << " V" << std::endl;

  return 0;
}
