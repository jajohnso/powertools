#include <unistd.h>
#include <string.h>
#include <getopt.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

#include <nlohmann/json.hpp>

#include <Logger.h>

#include "AMACv2.h"

#include "PBv3TBConf.h"
#include "PBv3ConfigTools.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

//------ Channel Map
std::map<std::string,AMACv2Field AMACv2RegMap::*> channelmap={
  {"VDCDC"       ,&AMACv2::Ch0Value},
  {"VDDLR"       ,&AMACv2::Ch1Value},
  {"DCDCin"      ,&AMACv2::Ch2Value},
  {"VDDREG"      ,&AMACv2::Ch3Value},
  {"sysBG"       ,&AMACv2::Ch3Value},
  {"AM900BG"     ,&AMACv2::Ch3Value},
  {"AM600BG"     ,&AMACv2::Ch4Value},
  {"CAL"         ,&AMACv2::Ch4Value},
  {"AMref"       ,&AMACv2::Ch4Value},
  {"CALx"        ,&AMACv2::Ch5Value},
  {"CALy"        ,&AMACv2::Ch5Value},
  {"Shuntx"      ,&AMACv2::Ch5Value},
  {"Shunty"      ,&AMACv2::Ch5Value},
  {"HGND"        ,&AMACv2::Ch5Value},
  {"DIETEMP"     ,&AMACv2::Ch6Value},
  {"NTCx"        ,&AMACv2::Ch7Value},
  {"NTCy"        ,&AMACv2::Ch8Value},
  {"NTCpb"       ,&AMACv2::Ch9Value},
  {"Hrefx"       ,&AMACv2::Ch10Value},
  {"Hrefy"       ,&AMACv2::Ch11Value},
  {"Cur10V"      ,&AMACv2::Ch12Value},
  {"Cur10VTPlow" ,&AMACv2::Ch12Value},
  {"Cur10VTPhigh",&AMACv2::Ch12Value},
  {"Cur1V"       ,&AMACv2::Ch13Value},
  {"Cur1VTPlow"  ,&AMACv2::Ch13Value},
  {"Cur1VTPhigh" ,&AMACv2::Ch13Value},
  {"HVret"       ,&AMACv2::Ch14Value},
  {"PTAT"        ,&AMACv2::Ch15Value},
};

std::map<std::string,std::pair<AMACv2Field AMACv2RegMap::*, uint32_t> > muxmap={
  {"VDDREG"      ,{&AMACv2::Ch3Mux ,0}},
  {"sysBG"       ,{&AMACv2::Ch3Mux ,1}},
  {"AM900BG"     ,{&AMACv2::Ch3Mux ,2}},
  {"AM600BG"     ,{&AMACv2::Ch4Mux ,0}},
  {"CAL"         ,{&AMACv2::Ch4Mux ,1}},
  {"AMref"       ,{&AMACv2::Ch4Mux ,2}},
  {"CALx"        ,{&AMACv2::Ch5Mux ,0}},
  {"CALy"        ,{&AMACv2::Ch5Mux ,1}},
  {"Shuntx"      ,{&AMACv2::Ch5Mux ,2}},
  {"Shunty"      ,{&AMACv2::Ch5Mux ,3}},
  {"HGND"        ,{&AMACv2::Ch5Mux ,4}},
  {"Cur10V"      ,{&AMACv2::Ch12Mux,0}},
  {"Cur10VTPlow" ,{&AMACv2::Ch12Mux,1}},
  {"Cur10VTPhigh",{&AMACv2::Ch12Mux,2}},
  {"Cur1V"       ,{&AMACv2::Ch13Mux,0}},
  {"Cur1VTPlow"  ,{&AMACv2::Ch13Mux,1}},
  {"Cur1VTPhigh" ,{&AMACv2::Ch13Mux,2}}
};
//---------------

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] channel" << std::endl;
  std::cerr << "Channel can be either number or one of the following. a name.:" << std::endl;
  for(const std::pair<std::string,AMACv2Field AMACv2RegMap::*>& kv : channelmap)
    std::cerr << "  " << kv.first << std::endl;
  std::cerr << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -b, --board             Powerboard number (default: " << (uint32_t)pbNum << ")" << std::endl;
  std::cerr << " -e, --equip config.json Equipment configuration file (default: " << equipConfigFile << ")" << std::endl;
  std::cerr << " -d, --debug             Enable more verbose printout"  << std::endl;
  std::cerr << std::endl;
  std::cerr << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 2)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
	{
	  {"board"    , required_argument, 0,  'b' },
	  {"equip"    , required_argument, 0,  'e' },
	  {"debug"    , no_argument      , 0,  'd' },
	  {0          , 0                , 0,  0 }
	};

      c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 'b':
	  pbNum = atoi(optarg);
	  break;
	case 'e':
	  equipConfigFile = optarg;
	  break;
	case 'd':
	  logIt::incrDebug();
	  break;
	default:
	  std::cerr << "Invalid option supplied. Aborting." << std::endl;
	  std::cerr << std::endl;
	  usage(argv);
	}
    }

  std::string chStr;
  if (optind == argc-1)
    {
      chStr=argv[optind];
    }
  else
    {
      std::cerr << "Required channel argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  logger(logDEBUG) << "Settings";
  logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;


  // Create and initialize the testbench
  PBv3TBConf factory_pbv3tb(equipConfigFile);
  std::shared_ptr<PBv3TB> tb=factory_pbv3tb.getPBv3TB("default");
  if(tb==nullptr)
    return 1;

  tb->getPB(pbNum)->init();

  //
  // Perform the read of the ADC
  AMACv2Field AMACv2RegMap::* ch;

  // try channel number
  try
    {
      int32_t chNum=std::stoi(chStr);
      switch(chNum)
	{
	case 0:
	  ch=&AMACv2::Ch0Value;
	  break;
	case 1:
	  ch=&AMACv2::Ch1Value;
	  break;
	case 2:
	  ch=&AMACv2::Ch2Value;
	  break;
	case 3:
	  ch=&AMACv2::Ch3Value;
	  break;
	case 4:
	  ch=&AMACv2::Ch4Value;
	  break;
	case 5:
	  ch=&AMACv2::Ch5Value;
	  break;
	case 6:
	  ch=&AMACv2::Ch6Value;
	  break;
	case 7:
	  ch=&AMACv2::Ch7Value;
	  break;
	case 8:
	  ch=&AMACv2::Ch8Value;
	  break;
	case 9:
	  ch=&AMACv2::Ch9Value;
	  break;
	case 10:
	  ch=&AMACv2::Ch10Value;
	  break;
	case 11:
	  ch=&AMACv2::Ch11Value;
	  break;
	case 12:
	  ch=&AMACv2::Ch12Value;
	  break;
	case 13:
	  ch=&AMACv2::Ch13Value;
	  break;
	case 14:
	  ch=&AMACv2::Ch14Value;
	  break;
	case 15:
	  ch=&AMACv2::Ch15Value;
	  break;
	default:
	  std::cerr << "Channel must be in range 0-15." << std::endl;
	  usage(argv);
	  return 1;
	}
    }
  catch(const std::invalid_argument& e)
    { // Could be a channel name
      if(channelmap.find(chStr)==channelmap.end())
	{
	  std::cerr << "Invalid channel name." << std::endl;
	  usage(argv);
	  return 1;
	}
      ch=channelmap[chStr];

      // Set multiplexer
      if(muxmap.find(chStr)!=muxmap.end())
	tb->getPB(pbNum)->wrField(muxmap[chStr].first,muxmap[chStr].second);
    }

  std::cout << tb->getPB(pbNum)->rdField(ch) << std::endl;

  return 0;
}
