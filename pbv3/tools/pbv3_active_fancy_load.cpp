#include <unistd.h>
#include <string.h>
#include <getopt.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

#include <nlohmann/json.hpp>

#include "Logger.h"
#include "PowerSupplyChannel.h"

#include "AMACv2.h"
#include "EndeavourComException.h"

#include "PBv3TBConf.h"
#include "PBv3TestTools.h"
#include "PBv3ConfigTools.h"
#include "PBv3Utils.h"
#include "PBv3TBMassive.h"

//------ SETTINGS
std::string configfile;
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

// Range is iout in A
json measureEfficiency(std::shared_ptr<AMACv2> amac, std::shared_ptr<PowerSupplyChannel> lv, std::shared_ptr<Bk85xx> load, double step, double min, double max, double VinSet=11.0)
{
    logger(logINFO) << "## Measuring DCDC efficiency ## " << PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "DCDCEFFICIENCY";
    testSum["results"]["TIMESTART"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    // Store current state
    uint32_t DCDCen_curr =amac->rdField(&AMACv2RegMap::DCDCen );
    uint32_t DCDCenC_curr=amac->rdField(&AMACv2RegMap::DCDCenC);
    uint32_t Ch12Mux_curr=amac->rdField(&AMACv2RegMap::Ch12Mux);
    uint32_t Ch13Mux_curr=amac->rdField(&AMACv2RegMap::Ch13Mux);

    //
    // Initialize

    // Set load to 0
    try
      {
	load->setCurrent(0);
	load->turnOn();
      }
    catch(const std::string &s)
      {
	logger(logERROR) << s;
	testSum["error"] = s;
	return testSum;
      }
    
    logger(logINFO) << " --> Vin = " << VinSet << "V";
    lv->setVoltageLevel(VinSet);

    logger(logINFO) << " --> Turn off DCDC ..";
    amac->wrField(&AMACv2::DCDCen, 0);
    amac->wrField(&AMACv2::DCDCenC, 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
    double Iin_offset = lv->measureCurrent();
    logger(logINFO) << " --> Get baseline current: (" << Iin_offset << ")A";
    testSum["results"]["IINOFFSET"] = Iin_offset;

    logger(logINFO) << " --> Turn on DCDC ...";
    amac->wrField(&AMACv2::DCDCen, 1);
    amac->wrField(&AMACv2::DCDCenC, 1);

    //unsigned dwell_time = .1; //s
    unsigned dwell_time_s = .005; //s
    unsigned dwell_time = 5; //s
    testSum["properties"]["DWELLTIMELONG" ] = dwell_time;
    testSum["properties"]["DWELLTIMESHORT"] = dwell_time_s;

    logger(logINFO) << " --> Starting measurement ...";
    std::cout << "Vin" << "\t" << "Iin" << "\t" << "Vout" << "\t" << "Iout" << "\t" << "IoutSense" << "\t" << "Vdcdc"
	      << "\t" << "VddLr" << "\t" << "DCDCin" << "\t" << "NTC" << "\t"
	      << "Cur10V" << "\t" << "Cur1V" << "\t" << "PTAT" << "\t" << "Efficiency" << "\t" << "EfficiencySense" << std::endl;
    // Set sub-channel
    amac->wrField(&AMACv2::Ch12Mux, 0); //a
    amac->wrField(&AMACv2::Ch13Mux, 0); //a

    // Loop over currents
    int index = 0;

    for (double iout=min;iout<=max;iout+=step)
      { 
	//allowing system to reach thermal equilibrium
	std::this_thread::sleep_for(std::chrono::seconds(dwell_time));
	logger(logDEBUG) << " --> Setting " << iout << "A load!";
	// Set Current
        load->setCurrent(iout*1e3);
	// Wait for temp and everything to settle
	std::this_thread::sleep_for(std::chrono::seconds(dwell_time_s));
	// Read AMAC values
	int Vdcdc, VddLr, DCDCin, NTC, Cur10V, Cur1V, PTAT;

	uint32_t trails=0;
	for(uint32_t trail=0; trail<trails; trail++)
	  {
	    Vdcdc  = amac->rdField(&AMACv2::Ch0Value);
	    VddLr  = amac->rdField(&AMACv2::Ch1Value);
	    DCDCin = amac->rdField(&AMACv2::Ch2Value);
	    NTC    = amac->rdField(&AMACv2::Ch9Value);
	    Cur10V = amac->rdField(&AMACv2::Ch12Value);
	    Cur1V  = amac->rdField(&AMACv2::Ch13Value);
	    PTAT   = amac->rdField(&AMACv2::Ch15Value);

	    double Vin = lv->measureVoltage();
            double Iin = lv->measureCurrent();
            double IoutSense = load->getValues().cur*1e-3;

            double Vout = load->getValues().vol*1e-3;

	    double efficiency = (Vout*iout)/(Vin*(Iin-Iin_offset));
	    double efficiencySense = (Vout*IoutSense)/(Vin*(Iin-Iin_offset));
	    std::cout << Vin << "\t" << Iin << "\t" << Vout << "\t" << iout << "\t" << IoutSense << "\t" << Vdcdc
		      << "\t" << VddLr << "\t" << DCDCin << "\t" << NTC << "\t"
		      << Cur10V << "\t" << Cur1V << "\t" << PTAT << "\t" << efficiency << "\t" << efficiencySense << std::endl;
	    testSum["results"]["VIN"            ][index] = Vin;
	    testSum["results"]["IIN"            ][index] = Iin;
	    testSum["results"]["VOUT"           ][index] = Vout;
	    testSum["results"]["IOUT"           ][index] = IoutSense;
	    testSum["results"]["AMACVDCDC"      ][index] = Vdcdc;
	    testSum["results"]["AMACVDDLR"      ][index] = VddLr;
	    testSum["results"]["AMACDCDCIN"     ][index] = DCDCin;
	    testSum["results"]["AMACNTCPB"      ][index] = NTC;
	    testSum["results"]["AMACCUR10V"     ][index] = Cur10V;
	    testSum["results"]["AMACCUR1V"      ][index] = Cur1V;
	    testSum["results"]["AMACPTAT"       ][index] = PTAT;
	    testSum["results"]["EFFICIENCY"     ][index] = efficiency;
	    testSum["results"]["EFFICIENCYSENSE"][index] = efficiencySense;
	    index++;
	  }
      }

    logger(logINFO) << " --> Done!! Turng off load!";

    // Disable load
    load->turnOff();

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCen , DCDCen_curr );
    amac->wrField(&AMACv2RegMap::DCDCenC, DCDCenC_curr);
    amac->wrField(&AMACv2RegMap::Ch12Mux, Ch12Mux_curr);
    amac->wrField(&AMACv2RegMap::Ch13Mux, Ch13Mux_curr);

    testSum["results"]["TIMEEND"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
  }


void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] datadir BK85XX" << std::endl;
  std::cerr << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -c, --config            Config for initializing the AMAC. (default: " << configfile << ")" << std::endl;
  std::cerr << " -b, --board             Powerboard number (default: " << (uint32_t)pbNum << ")" << std::endl;
  std::cerr << " -e, --equip config.json Equipment configuration file (default: " << equipConfigFile << ")" << std::endl;
  std::cerr << " -d, --debug             Enable more verbose printout"  << std::endl;
  std::cerr << std::endl;
  std::cerr << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 3)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
	{
	  {"config"   , required_argument, 0,  'c' },
	  {"board"    , required_argument, 0,  'b' },
	  {"equip"    , required_argument, 0,  'e' },
	  {"debug"    , no_argument      , 0,  'd' },
	  {0          , 0                , 0,  0 }
	};

      c = getopt_long(argc, argv, "c:b:e:d", long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 'c':
	  configfile = optarg;
	  break;
	case 'b':
	  pbNum = atoi(optarg);
	  break;
	case 'e':
	  equipConfigFile = optarg;
	  break;
	case 'd':
	  logIt::incrDebug();
	  break;
	default:
	  std::cerr << "Invalid option supplied. Aborting." << std::endl;
	  std::cerr << std::endl;
	  usage(argv);
	}
    }

  if (argc-optind < 2)
    {
      std::cerr << "Required paths missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  std::string outDir = argv[optind++];
  std::string bkDev  = argv[optind++];

  logger(logDEBUG) << "Settings";
  logger(logDEBUG) << " outDir: " << outDir;
  logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;
  logger(logDEBUG) << " bkDev: "  << bkDev;

  // Output file
  std::string fileName = outDir + "/" + PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) + "_pbv3-test.json";
  logger(logINFO) << "Results stored in " << fileName;
  std::fstream outfile(fileName, std::ios::out);
  if(!outfile.is_open())
    {
      logger(logERROR) << "Unable to create results file " << fileName;
      return 2;
    }

  // Create and initialize the testbench
  PBv3TBConf factory_pbv3tb(equipConfigFile);
  std::shared_ptr<PBv3TB> tb=factory_pbv3tb.getPBv3TB("default");
  if(tb==nullptr)
    return 1;
  if(std::dynamic_pointer_cast<PBv3TBMassive>(tb)==nullptr)
    {
      logger(logERROR) << argv[0] << " only works with PBv3TBMassive!";
      return 1;
    }

  // Turn on power
  logger(logINFO) << "Turn on PS fully";
  tb->powerLVOn();
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  // Init Load
  logger(logINFO) << "Init BK DCDC Load";
  std::shared_ptr<Bk85xx> load=std::make_shared<Bk85xx>(bkDev);
  load->setRemote();
  load->setRemoteSense(false);
  load->setModeCC();
  load->setCurrent(0);

  // Init com
  json config;
  if(!configfile.empty())
    {
      std::ifstream fh_in(configfile);
      if(fh_in.is_open())
	fh_in >> config;
    }


  logger(logINFO) << "Init AMAC";
  std::shared_ptr<AMACv2> amac=tb->getPB(pbNum);
  amac->init();
  PBv3ConfigTools::configAMAC(amac, config, false);
  amac->initRegisters();
  PBv3ConfigTools::saveConfigAMAC(amac, config);

  //
  // Start testing

  std::shared_ptr<PowerSupplyChannel> lv=tb->getLVPS();

  json testSum;
  testSum["program"] = argv[0];
  testSum["config"] = config;
  testSum["time"]["start"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 

  uint32_t test=0;
  testSum["tests"][test++] = measureEfficiency(amac, lv, load, 0.1, 0.0, 3.6);

  testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 

  outfile << std::setw(4) << testSum << std::endl;

  outfile.close();

  logger(logINFO) << "Power off";

  tb->powerLVOff();
  load->turnOff();

  return 0;
}
