#ifndef ITSDAQOPCODE_H_
#define ITSDAQOPCODE_H_

#include <cstdint>
#include <vector>

#define ITSDAQ_OPCODE_ECHO    0x0003
#define ITSDAQ_OPCODE_TWOWIRE 0x0080
#define ITSDAQ_OPCODE_EMORSE  0x008E

class ITSDAQOpCode
{
public:
  ITSDAQOpCode(uint16_t seqnum, uint16_t opcode, const std::vector<uint16_t>& payload);

  uint16_t opcode() const;
  std::vector<uint16_t> payload() const;

  std::vector<uint16_t> data() const;

private:
  uint16_t m_seqNum;
  uint16_t m_opcode;
  std::vector<uint16_t> m_payload;
};

#endif // ITSDAQOPCODE
