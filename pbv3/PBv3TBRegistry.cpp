#include "PBv3TBRegistry.h"

#include <iostream>

#include <ClassRegistry.h>

namespace PBv3TBRegistry
{

  typedef EquipRegistry::ClassRegistry<PBv3TB> RegistryPBv3TB;

  static RegistryPBv3TB &registry()
  {
    static RegistryPBv3TB instance;
    return instance;
  }

  bool registerPBv3TB(const std::string& type, std::function<std::shared_ptr<PBv3TB>()> f)
  {
    return registry().registerClass(type, f);
  }
  
  std::shared_ptr<PBv3TB> createPBv3TB(const std::string& type)
  {
    std::shared_ptr<PBv3TB> result = registry().makeClass(type);
    if(result == nullptr)
      throw "No PBv3 testbench (PBv3TB) of type " +type+ " found\n";

    return result;
  }

  std::vector<std::string> listPBv3TB()
  {
    return registry().listClasses();
  }
}

