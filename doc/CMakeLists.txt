option(BUILD_DOC "Build documentation" ON)

# Check if doxygen installed
find_package(Doxygen)
if (DOXYGEN_FOUND AND BUILD_DOC)

  # Doxygen configuration
  set(DOXYGEN_GENERATE_HTML YES)
  set(DOXYGEN_USE_MDFILE_AS_MAINPAGE ../README.md)
  set(DOXYGEN_OPTIMIZE_OUTPUT_FOR_C NO)
  set(DOXYGEN_OPTIMIZE_OUTPUT_JAVA NO)
  set(DOXYGEN_OPTIMIZE_FOR_FORTRAN NO)
  set(DOXYGEN_OPTIMIZE_OUTPUT_VHDL NO)

  # Create target
  doxygen_add_docs(
    docs
    ../README.md
    ../pbv3
    COMMENT "Generate automatic documentation")
endif()
